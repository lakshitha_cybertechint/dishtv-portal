-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2018 at 12:36 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dish-tv`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'ojiLiaMKi2NpL8chlTyJBcoOoaCR2ij9', 1, '2018-11-02 16:07:29', '2018-11-02 16:07:29', '2018-11-02 16:07:29'),
(2, 2, '5m7TnAxhEntAAvOBvLPWuLccXAjB0k2w', 1, '2018-11-02 16:07:30', '2018-11-02 16:07:30', '2018-11-02 16:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `fonts-list`
--

CREATE TABLE `fonts-list` (
  `id` int(15) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `unicode` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fonts-list`
--

INSERT INTO `fonts-list` (`id`, `type`, `icon`, `unicode`) VALUES
(1, 'fa', 'fa-adjust', '&#xf042;'),
(2, 'fa', 'fa-adn', '&#xf170;'),
(3, 'fa', 'fa-align-center', '&#xf037;'),
(4, 'fa', 'fa-align-justify', '&#xf039;'),
(5, 'fa', 'fa-align-left', '&#xf036;'),
(6, 'fa', 'fa-align-right', '&#xf038;'),
(7, 'fa', 'fa-ambulance', '&#xf0f9;'),
(8, 'fa', 'fa-anchor', '&#xf13d;'),
(9, 'fa', 'fa-android', '&#xf17b;'),
(10, 'fa', 'fa-angellist', '&#xf209;'),
(11, 'fa', 'fa-angle-double-down', '&#xf103;'),
(12, 'fa', 'fa-angle-double-left', '&#xf100;'),
(13, 'fa', 'fa-angle-double-right', '&#xf101;'),
(14, 'fa', 'fa-angle-double-up', '&#xf102;'),
(15, 'fa', 'fa-angle-down', '&#xf107;'),
(16, 'fa', 'fa-angle-left', '&#xf104;'),
(17, 'fa', 'fa-angle-right', '&#xf105;'),
(18, 'fa', 'fa-angle-up', '&#xf106;'),
(19, 'fa', 'fa-apple', '&#xf179;'),
(20, 'fa', 'fa-archive', '&#xf187;'),
(21, 'fa', 'fa-area-chart', '&#xf1fe;'),
(22, 'fa', 'fa-arrow-circle-down', '&#xf0ab;'),
(23, 'fa', 'fa-arrow-circle-left', '&#xf0a8;'),
(24, 'fa', 'fa-arrow-circle-o-down', '&#xf01a;'),
(25, 'fa', 'fa-arrow-circle-o-left', '&#xf190;'),
(26, 'fa', 'fa-arrow-circle-o-right', '&#xf18e;'),
(27, 'fa', 'fa-arrow-circle-o-up', '&#xf01b;'),
(28, 'fa', 'fa-arrow-circle-right', '&#xf0a9;'),
(29, 'fa', 'fa-arrow-circle-up', '&#xf0aa;'),
(30, 'fa', 'fa-arrow-down', '&#xf063;'),
(31, 'fa', 'fa-arrow-left', '&#xf060;'),
(32, 'fa', 'fa-arrow-right', '&#xf061;'),
(33, 'fa', 'fa-arrow-up', '&#xf062;'),
(34, 'fa', 'fa-arrows', '&#xf047;'),
(35, 'fa', 'fa-arrows-alt', '&#xf0b2;'),
(36, 'fa', 'fa-arrows-h', '&#xf07e;'),
(37, 'fa', 'fa-arrows-v', '&#xf07d;'),
(38, 'fa', 'fa-asterisk', '&#xf069;'),
(39, 'fa', 'fa-at', '&#xf1fa;'),
(40, 'fa', 'fa-automobile(alias)', '&#xf1b9;'),
(41, 'fa', 'fa-backward', '&#xf04a;'),
(42, 'fa', 'fa-ban', '&#xf05e;'),
(43, 'fa', 'fa-bank(alias)', '&#xf19c;'),
(44, 'fa', 'fa-bar-chart', '&#xf080;'),
(45, 'fa', 'fa-bar-chart-o(alias)', '&#xf080;'),
(46, 'fa', 'fa-barcode', '&#xf02a;'),
(47, 'fa', 'fa-bars', '&#xf0c9;'),
(48, 'fa', 'fa-bed', '&#xf236;'),
(49, 'fa', 'fa-beer', '&#xf0fc;'),
(50, 'fa', 'fa-behance', '&#xf1b4;'),
(51, 'fa', 'fa-behance-square', '&#xf1b5;'),
(52, 'fa', 'fa-bell', '&#xf0f3;'),
(53, 'fa', 'fa-bell-o', '&#xf0a2;'),
(54, 'fa', 'fa-bell-slash', '&#xf1f6;'),
(55, 'fa', 'fa-bell-slash-o', '&#xf1f7;'),
(56, 'fa', 'fa-bicycle', '&#xf206;'),
(57, 'fa', 'fa-binoculars', '&#xf1e5;'),
(58, 'fa', 'fa-birthday-cake', '&#xf1fd;'),
(59, 'fa', 'fa-bitbucket', '&#xf171;'),
(60, 'fa', 'fa-bitbucket-square', '&#xf172;'),
(61, 'fa', 'fa-bitcoin(alias)', '&#xf15a;'),
(62, 'fa', 'fa-bold', '&#xf032;'),
(63, 'fa', 'fa-bolt', '&#xf0e7;'),
(64, 'fa', 'fa-bomb', '&#xf1e2;'),
(65, 'fa', 'fa-book', '&#xf02d;'),
(66, 'fa', 'fa-bookmark', '&#xf02e;'),
(67, 'fa', 'fa-bookmark-o', '&#xf097;'),
(68, 'fa', 'fa-briefcase', '&#xf0b1;'),
(69, 'fa', 'fa-btc', '&#xf15a;'),
(70, 'fa', 'fa-bug', '&#xf188;'),
(71, 'fa', 'fa-building', '&#xf1ad;'),
(72, 'fa', 'fa-building-o', '&#xf0f7;'),
(73, 'fa', 'fa-bullhorn', '&#xf0a1;'),
(74, 'fa', 'fa-bullseye', '&#xf140;'),
(75, 'fa', 'fa-bus', '&#xf207;'),
(76, 'fa', 'fa-buysellads', NULL),
(77, 'fa', 'fa-cab(alias)', NULL),
(78, 'fa', 'fa-calculator', NULL),
(79, 'fa', 'fa-calendar', NULL),
(80, 'fa', 'fa-calendar-o', NULL),
(81, 'fa', 'fa-camera', NULL),
(82, 'fa', 'fa-camera-retro', NULL),
(83, 'fa', 'fa-car', NULL),
(84, 'fa', 'fa-caret-down', NULL),
(85, 'fa', 'fa-caret-left', NULL),
(86, 'fa', 'fa-caret-right', NULL),
(87, 'fa', 'fa-caret-square-o-down', NULL),
(88, 'fa', 'fa-caret-square-o-left', NULL),
(89, 'fa', 'fa-caret-square-o-right', NULL),
(90, 'fa', 'fa-caret-square-o-up', NULL),
(91, 'fa', 'fa-caret-up', NULL),
(92, 'fa', 'fa-cart-arrow-down', NULL),
(93, 'fa', 'fa-cart-plus', NULL),
(94, 'fa', 'fa-cc', NULL),
(95, 'fa', 'fa-cc-amex', NULL),
(96, 'fa', 'fa-cc-discover', NULL),
(97, 'fa', 'fa-cc-mastercard', NULL),
(98, 'fa', 'fa-cc-paypal', NULL),
(99, 'fa', 'fa-cc-stripe', NULL),
(100, 'fa', 'fa-cc-visa', NULL),
(101, 'fa', 'fa-certificate', NULL),
(102, 'fa', 'fa-chain(alias)', NULL),
(103, 'fa', 'fa-chain-broken', NULL),
(104, 'fa', 'fa-check', NULL),
(105, 'fa', 'fa-check-circle', NULL),
(106, 'fa', 'fa-check-circle-o', NULL),
(107, 'fa', 'fa-check-square', NULL),
(108, 'fa', 'fa-check-square-o', NULL),
(109, 'fa', 'fa-chevron-circle-down', NULL),
(110, 'fa', 'fa-chevron-circle-left', NULL),
(111, 'fa', 'fa-chevron-circle-right', NULL),
(112, 'fa', 'fa-chevron-circle-up', NULL),
(113, 'fa', 'fa-chevron-down', NULL),
(114, 'fa', 'fa-chevron-left', NULL),
(115, 'fa', 'fa-chevron-right', NULL),
(116, 'fa', 'fa-chevron-up', NULL),
(117, 'fa', 'fa-child', NULL),
(118, 'fa', 'fa-circle', NULL),
(119, 'fa', 'fa-circle-o', NULL),
(120, 'fa', 'fa-circle-o-notch', NULL),
(121, 'fa', 'fa-circle-thin', NULL),
(122, 'fa', 'fa-clipboard', NULL),
(123, 'fa', 'fa-clock-o', NULL),
(124, 'fa', 'fa-close(alias)', NULL),
(125, 'fa', 'fa-cloud', NULL),
(126, 'fa', 'fa-cloud-download', NULL),
(127, 'fa', 'fa-cloud-upload', NULL),
(128, 'fa', 'fa-cny(alias)', NULL),
(129, 'fa', 'fa-code', NULL),
(130, 'fa', 'fa-code-fork', NULL),
(131, 'fa', 'fa-codepen', NULL),
(132, 'fa', 'fa-coffee', NULL),
(133, 'fa', 'fa-cog', NULL),
(134, 'fa', 'fa-cogs', NULL),
(135, 'fa', 'fa-columns', NULL),
(136, 'fa', 'fa-comment', NULL),
(137, 'fa', 'fa-comment-o', NULL),
(138, 'fa', 'fa-comments', NULL),
(139, 'fa', 'fa-comments-o', NULL),
(140, 'fa', 'fa-compass', NULL),
(141, 'fa', 'fa-compress', NULL),
(142, 'fa', 'fa-connectdevelop', NULL),
(143, 'fa', 'fa-copy(alias)', NULL),
(144, 'fa', 'fa-copyright', NULL),
(145, 'fa', 'fa-credit-card', NULL),
(146, 'fa', 'fa-crop', NULL),
(147, 'fa', 'fa-crosshairs', NULL),
(148, 'fa', 'fa-css3', NULL),
(149, 'fa', 'fa-cube', NULL),
(150, 'fa', 'fa-cubes', NULL),
(151, 'fa', 'fa-cut(alias)', NULL),
(152, 'fa', 'fa-cutlery', NULL),
(153, 'fa', 'fa-dashboard(alias)', NULL),
(154, 'fa', 'fa-dashcube', NULL),
(155, 'fa', 'fa-database', NULL),
(156, 'fa', 'fa-dedent(alias)', NULL),
(157, 'fa', 'fa-delicious', NULL),
(158, 'fa', 'fa-desktop', NULL),
(159, 'fa', 'fa-deviantart', NULL),
(160, 'fa', 'fa-diamond', NULL),
(161, 'fa', 'fa-digg', NULL),
(162, 'fa', 'fa-dollar(alias)', NULL),
(163, 'fa', 'fa-dot-circle-o', NULL),
(164, 'fa', 'fa-download', NULL),
(165, 'fa', 'fa-dribbble', NULL),
(166, 'fa', 'fa-dropbox', NULL),
(167, 'fa', 'fa-drupal', NULL),
(168, 'fa', 'fa-edit(alias)', NULL),
(169, 'fa', 'fa-eject', NULL),
(170, 'fa', 'fa-ellipsis-h', NULL),
(171, 'fa', 'fa-ellipsis-v', NULL),
(172, 'fa', 'fa-empire', NULL),
(173, 'fa', 'fa-envelope', NULL),
(174, 'fa', 'fa-envelope-o', NULL),
(175, 'fa', 'fa-envelope-square', NULL),
(176, 'fa', 'fa-eraser', NULL),
(177, 'fa', 'fa-eur', NULL),
(178, 'fa', 'fa-euro(alias)', NULL),
(179, 'fa', 'fa-exchange', NULL),
(180, 'fa', 'fa-exclamation', NULL),
(181, 'fa', 'fa-exclamation-circle', NULL),
(182, 'fa', 'fa-exclamation-triangle', NULL),
(183, 'fa', 'fa-expand', NULL),
(184, 'fa', 'fa-external-link', NULL),
(185, 'fa', 'fa-external-link-square', NULL),
(186, 'fa', 'fa-eye', NULL),
(187, 'fa', 'fa-eye-slash', NULL),
(188, 'fa', 'fa-eyedropper', NULL),
(189, 'fa', 'fa-facebook', NULL),
(190, 'fa', 'fa-facebook-f(alias)', NULL),
(191, 'fa', 'fa-facebook-official', NULL),
(192, 'fa', 'fa-facebook-square', NULL),
(193, 'fa', 'fa-fast-backward', NULL),
(194, 'fa', 'fa-fast-forward', NULL),
(195, 'fa', 'fa-fax', NULL),
(196, 'fa', 'fa-female', NULL),
(197, 'fa', 'fa-fighter-jet', NULL),
(198, 'fa', 'fa-file', NULL),
(199, 'fa', 'fa-file-archive-o', NULL),
(200, 'fa', 'fa-file-audio-o', NULL),
(201, 'fa', 'fa-file-code-o', NULL),
(202, 'fa', 'fa-file-excel-o', NULL),
(203, 'fa', 'fa-file-image-o', NULL),
(204, 'fa', 'fa-file-movie-o(alias)', NULL),
(205, 'fa', 'fa-file-o', NULL),
(206, 'fa', 'fa-file-pdf-o', NULL),
(207, 'fa', 'fa-file-photo-o(alias)', NULL),
(208, 'fa', 'fa-file-picture-o(alias)', NULL),
(209, 'fa', 'fa-file-powerpoint-o', NULL),
(210, 'fa', 'fa-file-sound-o(alias)', NULL),
(211, 'fa', 'fa-file-text', NULL),
(212, 'fa', 'fa-file-text-o', NULL),
(213, 'fa', 'fa-file-video-o', NULL),
(214, 'fa', 'fa-file-word-o', NULL),
(215, 'fa', 'fa-file-zip-o(alias)', NULL),
(216, 'fa', 'fa-files-o', NULL),
(217, 'fa', 'fa-film', NULL),
(218, 'fa', 'fa-filter', NULL),
(219, 'fa', 'fa-fire', NULL),
(220, 'fa', 'fa-fire-extinguisher', NULL),
(221, 'fa', 'fa-flag', NULL),
(222, 'fa', 'fa-flag-checkered', NULL),
(223, 'fa', 'fa-flag-o', NULL),
(224, 'fa', 'fa-flash(alias)', NULL),
(225, 'fa', 'fa-flask', NULL),
(226, 'fa', 'fa-flickr', NULL),
(227, 'fa', 'fa-floppy-o', NULL),
(228, 'fa', 'fa-folder', NULL),
(229, 'fa', 'fa-folder-o', NULL),
(230, 'fa', 'fa-folder-open', NULL),
(231, 'fa', 'fa-folder-open-o', NULL),
(232, 'fa', 'fa-font', NULL),
(233, 'fa', 'fa-forumbee', NULL),
(234, 'fa', 'fa-forward', NULL),
(235, 'fa', 'fa-foursquare', NULL),
(236, 'fa', 'fa-frown-o', NULL),
(237, 'fa', 'fa-futbol-o', NULL),
(238, 'fa', 'fa-gamepad', NULL),
(239, 'fa', 'fa-gavel', NULL),
(240, 'fa', 'fa-gbp', NULL),
(241, 'fa', 'fa-ge(alias)', NULL),
(242, 'fa', 'fa-gear(alias)', NULL),
(243, 'fa', 'fa-gears(alias)', NULL),
(244, 'fa', 'fa-genderless(alias)', NULL),
(245, 'fa', 'fa-gift', NULL),
(246, 'fa', 'fa-git', NULL),
(247, 'fa', 'fa-git-square', NULL),
(248, 'fa', 'fa-github', NULL),
(249, 'fa', 'fa-github-alt', NULL),
(250, 'fa', 'fa-github-square', NULL),
(251, 'fa', 'fa-gittip(alias)', NULL),
(252, 'fa', 'fa-glass', NULL),
(253, 'fa', 'fa-globe', NULL),
(254, 'fa', 'fa-google', NULL),
(255, 'fa', 'fa-google-plus', NULL),
(256, 'fa', 'fa-google-plus-square', NULL),
(257, 'fa', 'fa-google-wallet', NULL),
(258, 'fa', 'fa-graduation-cap', NULL),
(259, 'fa', 'fa-gratipay', NULL),
(260, 'fa', 'fa-group(alias)', NULL),
(261, 'fa', 'fa-h-square', NULL),
(262, 'fa', 'fa-hacker-news', NULL),
(263, 'fa', 'fa-hand-o-down', NULL),
(264, 'fa', 'fa-hand-o-left', NULL),
(265, 'fa', 'fa-hand-o-right', NULL),
(266, 'fa', 'fa-hand-o-up', NULL),
(267, 'fa', 'fa-hdd-o', NULL),
(268, 'fa', 'fa-header', NULL),
(269, 'fa', 'fa-headphones', NULL),
(270, 'fa', 'fa-heart', NULL),
(271, 'fa', 'fa-heart-o', NULL),
(272, 'fa', 'fa-heartbeat', NULL),
(273, 'fa', 'fa-history', NULL),
(274, 'fa', 'fa-home', NULL),
(275, 'fa', 'fa-hospital-o', NULL),
(276, 'fa', 'fa-hotel(alias)', NULL),
(277, 'fa', 'fa-html5', NULL),
(278, 'fa', 'fa-ils', NULL),
(279, 'fa', 'fa-image(alias)', NULL),
(280, 'fa', 'fa-inbox', NULL),
(281, 'fa', 'fa-indent', NULL),
(282, 'fa', 'fa-info', NULL),
(283, 'fa', 'fa-info-circle', NULL),
(284, 'fa', 'fa-inr', NULL),
(285, 'fa', 'fa-instagram', NULL),
(286, 'fa', 'fa-institution(alias)', NULL),
(287, 'fa', 'fa-ioxhost', NULL),
(288, 'fa', 'fa-italic', NULL),
(289, 'fa', 'fa-joomla', NULL),
(290, 'fa', 'fa-jpy', NULL),
(291, 'fa', 'fa-jsfiddle', NULL),
(292, 'fa', 'fa-key', NULL),
(293, 'fa', 'fa-keyboard-o', NULL),
(294, 'fa', 'fa-krw', NULL),
(295, 'fa', 'fa-language', NULL),
(296, 'fa', 'fa-laptop', NULL),
(297, 'fa', 'fa-lastfm', NULL),
(298, 'fa', 'fa-lastfm-square', NULL),
(299, 'fa', 'fa-leaf', NULL),
(300, 'fa', 'fa-leanpub', NULL),
(301, 'fa', 'fa-legal(alias)', NULL),
(302, 'fa', 'fa-lemon-o', NULL),
(303, 'fa', 'fa-level-down', NULL),
(304, 'fa', 'fa-level-up', NULL),
(305, 'fa', 'fa-life-bouy(alias)', NULL),
(306, 'fa', 'fa-life-buoy(alias)', NULL),
(307, 'fa', 'fa-life-ring', NULL),
(308, 'fa', 'fa-life-saver(alias)', NULL),
(309, 'fa', 'fa-lightbulb-o', NULL),
(310, 'fa', 'fa-line-chart', NULL),
(311, 'fa', 'fa-link', NULL),
(312, 'fa', 'fa-linkedin', NULL),
(313, 'fa', 'fa-linkedin-square', NULL),
(314, 'fa', 'fa-linux', NULL),
(315, 'fa', 'fa-list', NULL),
(316, 'fa', 'fa-list-alt', NULL),
(317, 'fa', 'fa-list-ol', NULL),
(318, 'fa', 'fa-list-ul', NULL),
(319, 'fa', 'fa-location-arrow', NULL),
(320, 'fa', 'fa-lock', NULL),
(321, 'fa', 'fa-long-arrow-down', NULL),
(322, 'fa', 'fa-long-arrow-left', NULL),
(323, 'fa', 'fa-long-arrow-right', NULL),
(324, 'fa', 'fa-long-arrow-up', NULL),
(325, 'fa', 'fa-magic', NULL),
(326, 'fa', 'fa-magnet', NULL),
(327, 'fa', 'fa-mail-forward(alias)', NULL),
(328, 'fa', 'fa-mail-reply(alias)', NULL),
(329, 'fa', 'fa-mail-reply-all(alias)', NULL),
(330, 'fa', 'fa-male', NULL),
(331, 'fa', 'fa-map-marker', NULL),
(332, 'fa', 'fa-mars', NULL),
(333, 'fa', 'fa-mars-double', NULL),
(334, 'fa', 'fa-mars-stroke', NULL),
(335, 'fa', 'fa-mars-stroke-h', NULL),
(336, 'fa', 'fa-mars-stroke-v', NULL),
(337, 'fa', 'fa-maxcdn', NULL),
(338, 'fa', 'fa-meanpath', NULL),
(339, 'fa', 'fa-medium', NULL),
(340, 'fa', 'fa-medkit', NULL),
(341, 'fa', 'fa-meh-o', NULL),
(342, 'fa', 'fa-mercury', NULL),
(343, 'fa', 'fa-microphone', NULL),
(344, 'fa', 'fa-microphone-slash', NULL),
(345, 'fa', 'fa-minus', NULL),
(346, 'fa', 'fa-minus-circle', NULL),
(347, 'fa', 'fa-minus-square', NULL),
(348, 'fa', 'fa-minus-square-o', NULL),
(349, 'fa', 'fa-mobile', NULL),
(350, 'fa', 'fa-mobile-phone(alias)', NULL),
(351, 'fa', 'fa-money', NULL),
(352, 'fa', 'fa-moon-o', NULL),
(353, 'fa', 'fa-mortar-board(alias)', NULL),
(354, 'fa', 'fa-motorcycle', NULL),
(355, 'fa', 'fa-music', NULL),
(356, 'fa', 'fa-navicon(alias)', NULL),
(357, 'fa', 'fa-neuter', NULL),
(358, 'fa', 'fa-newspaper-o', NULL),
(359, 'fa', 'fa-openid', NULL),
(360, 'fa', 'fa-outdent', NULL),
(361, 'fa', 'fa-pagelines', NULL),
(362, 'fa', 'fa-paint-brush', NULL),
(363, 'fa', 'fa-paper-plane', NULL),
(364, 'fa', 'fa-paper-plane-o', NULL),
(365, 'fa', 'fa-paperclip', NULL),
(366, 'fa', 'fa-paragraph', NULL),
(367, 'fa', 'fa-paste(alias)', NULL),
(368, 'fa', 'fa-pause', NULL),
(369, 'fa', 'fa-paw', NULL),
(370, 'fa', 'fa-paypal', NULL),
(371, 'fa', 'fa-pencil', NULL),
(372, 'fa', 'fa-pencil-square', NULL),
(373, 'fa', 'fa-pencil-square-o', NULL),
(374, 'fa', 'fa-phone', NULL),
(375, 'fa', 'fa-phone-square', NULL),
(376, 'fa', 'fa-photo(alias)', NULL),
(377, 'fa', 'fa-picture-o', NULL),
(378, 'fa', 'fa-pie-chart', NULL),
(379, 'fa', 'fa-pied-piper', NULL),
(380, 'fa', 'fa-pied-piper-alt', NULL),
(381, 'fa', 'fa-pinterest', NULL),
(382, 'fa', 'fa-pinterest-p', NULL),
(383, 'fa', 'fa-pinterest-square', NULL),
(384, 'fa', 'fa-plane', NULL),
(385, 'fa', 'fa-play', NULL),
(386, 'fa', 'fa-play-circle', NULL),
(387, 'fa', 'fa-play-circle-o', NULL),
(388, 'fa', 'fa-plug', NULL),
(389, 'fa', 'fa-plus', NULL),
(390, 'fa', 'fa-plus-circle', NULL),
(391, 'fa', 'fa-plus-square', NULL),
(392, 'fa', 'fa-plus-square-o', NULL),
(393, 'fa', 'fa-power-off', NULL),
(394, 'fa', 'fa-print', NULL),
(395, 'fa', 'fa-puzzle-piece', NULL),
(396, 'fa', 'fa-qq', NULL),
(397, 'fa', 'fa-qrcode', NULL),
(398, 'fa', 'fa-question', NULL),
(399, 'fa', 'fa-question-circle', NULL),
(400, 'fa', 'fa-quote-left', NULL),
(401, 'fa', 'fa-quote-right', NULL),
(402, 'fa', 'fa-ra(alias)', NULL),
(403, 'fa', 'fa-random', NULL),
(404, 'fa', 'fa-rebel', NULL),
(405, 'fa', 'fa-recycle', NULL),
(406, 'fa', 'fa-reddit', NULL),
(407, 'fa', 'fa-reddit-square', NULL),
(408, 'fa', 'fa-refresh', NULL),
(409, 'fa', 'fa-remove(alias)', NULL),
(410, 'fa', 'fa-renren', NULL),
(411, 'fa', 'fa-reorder(alias)', NULL),
(412, 'fa', 'fa-repeat', NULL),
(413, 'fa', 'fa-reply', NULL),
(414, 'fa', 'fa-reply-all', NULL),
(415, 'fa', 'fa-retweet', NULL),
(416, 'fa', 'fa-rmb(alias)', NULL),
(417, 'fa', 'fa-road', NULL),
(418, 'fa', 'fa-rocket', NULL),
(419, 'fa', 'fa-rotate-left(alias)', NULL),
(420, 'fa', 'fa-rotate-right(alias)', NULL),
(421, 'fa', 'fa-rouble(alias)', NULL),
(422, 'fa', 'fa-rss', NULL),
(423, 'fa', 'fa-rss-square', NULL),
(424, 'fa', 'fa-rub', NULL),
(425, 'fa', 'fa-ruble(alias)', NULL),
(426, 'fa', 'fa-rupee(alias)', NULL),
(427, 'fa', 'fa-save(alias)', NULL),
(428, 'fa', 'fa-scissors', NULL),
(429, 'fa', 'fa-search', NULL),
(430, 'fa', 'fa-search-minus', NULL),
(431, 'fa', 'fa-search-plus', NULL),
(432, 'fa', 'fa-sellsy', NULL),
(433, 'fa', 'fa-send(alias)', NULL),
(434, 'fa', 'fa-send-o(alias)', NULL),
(435, 'fa', 'fa-server', NULL),
(436, 'fa', 'fa-share', NULL),
(437, 'fa', 'fa-share-alt', NULL),
(438, 'fa', 'fa-share-alt-square', NULL),
(439, 'fa', 'fa-share-square', NULL),
(440, 'fa', 'fa-share-square-o', NULL),
(441, 'fa', 'fa-shekel(alias)', NULL),
(442, 'fa', 'fa-sheqel(alias)', NULL),
(443, 'fa', 'fa-shield', NULL),
(444, 'fa', 'fa-ship', NULL),
(445, 'fa', 'fa-shirtsinbulk', NULL),
(446, 'fa', 'fa-shopping-cart', NULL),
(447, 'fa', 'fa-sign-in', NULL),
(448, 'fa', 'fa-sign-out', NULL),
(449, 'fa', 'fa-signal', NULL),
(450, 'fa', 'fa-simplybuilt', NULL),
(451, 'fa', 'fa-sitemap', NULL),
(452, 'fa', 'fa-skyatlas', NULL),
(453, 'fa', 'fa-skype', NULL),
(454, 'fa', 'fa-slack', NULL),
(455, 'fa', 'fa-sliders', NULL),
(456, 'fa', 'fa-slideshare', NULL),
(457, 'fa', 'fa-smile-o', NULL),
(458, 'fa', 'fa-soccer-ball-o(alias)', NULL),
(459, 'fa', 'fa-sort', NULL),
(460, 'fa', 'fa-sort-alpha-asc', NULL),
(461, 'fa', 'fa-sort-alpha-desc', NULL),
(462, 'fa', 'fa-sort-amount-asc', NULL),
(463, 'fa', 'fa-sort-amount-desc', NULL),
(464, 'fa', 'fa-sort-asc', NULL),
(465, 'fa', 'fa-sort-desc', NULL),
(466, 'fa', 'fa-sort-down(alias)', NULL),
(467, 'fa', 'fa-sort-numeric-asc', NULL),
(468, 'fa', 'fa-sort-numeric-desc', NULL),
(469, 'fa', 'fa-sort-up(alias)', NULL),
(470, 'fa', 'fa-soundcloud', NULL),
(471, 'fa', 'fa-space-shuttle', NULL),
(472, 'fa', 'fa-spinner', NULL),
(473, 'fa', 'fa-spoon', NULL),
(474, 'fa', 'fa-spotify', NULL),
(475, 'fa', 'fa-square', NULL),
(476, 'fa', 'fa-square-o', NULL),
(477, 'fa', 'fa-stack-exchange', NULL),
(478, 'fa', 'fa-stack-overflow', NULL),
(479, 'fa', 'fa-star', NULL),
(480, 'fa', 'fa-star-half', NULL),
(481, 'fa', 'fa-star-half-empty(alias)', NULL),
(482, 'fa', 'fa-star-half-full(alias)', NULL),
(483, 'fa', 'fa-star-half-o', NULL),
(484, 'fa', 'fa-star-o', NULL),
(485, 'fa', 'fa-steam', NULL),
(486, 'fa', 'fa-steam-square', NULL),
(487, 'fa', 'fa-step-backward', NULL),
(488, 'fa', 'fa-step-forward', NULL),
(489, 'fa', 'fa-stethoscope', NULL),
(490, 'fa', 'fa-stop', NULL),
(491, 'fa', 'fa-street-view', NULL),
(492, 'fa', 'fa-strikethrough', NULL),
(493, 'fa', 'fa-stumbleupon', NULL),
(494, 'fa', 'fa-stumbleupon-circle', NULL),
(495, 'fa', 'fa-subscript', NULL),
(496, 'fa', 'fa-subway', NULL),
(497, 'fa', 'fa-suitcase', NULL),
(498, 'fa', 'fa-sun-o', NULL),
(499, 'fa', 'fa-superscript', NULL),
(500, 'fa', 'fa-support(alias)', NULL),
(501, 'fa', 'fa-table', NULL),
(502, 'fa', 'fa-tablet', NULL),
(503, 'fa', 'fa-tachometer', NULL),
(504, 'fa', 'fa-tag', NULL),
(505, 'fa', 'fa-tags', NULL),
(506, 'fa', 'fa-tasks', NULL),
(507, 'fa', 'fa-taxi', NULL),
(508, 'fa', 'fa-tencent-weibo', NULL),
(509, 'fa', 'fa-terminal', NULL),
(510, 'fa', 'fa-text-height', NULL),
(511, 'fa', 'fa-text-width', NULL),
(512, 'fa', 'fa-th', NULL),
(513, 'fa', 'fa-th-large', NULL),
(514, 'fa', 'fa-th-list', NULL),
(515, 'fa', 'fa-thumb-tack', NULL),
(516, 'fa', 'fa-thumbs-down', NULL),
(517, 'fa', 'fa-thumbs-o-down', NULL),
(518, 'fa', 'fa-thumbs-o-up', NULL),
(519, 'fa', 'fa-thumbs-up', NULL),
(520, 'fa', 'fa-ticket', NULL),
(521, 'fa', 'fa-times', NULL),
(522, 'fa', 'fa-times-circle', NULL),
(523, 'fa', 'fa-times-circle-o', NULL),
(524, 'fa', 'fa-tint', NULL),
(525, 'fa', 'fa-toggle-down(alias)', NULL),
(526, 'fa', 'fa-toggle-left(alias)', NULL),
(527, 'fa', 'fa-toggle-off', NULL),
(528, 'fa', 'fa-toggle-on', NULL),
(529, 'fa', 'fa-toggle-right(alias)', NULL),
(530, 'fa', 'fa-toggle-up(alias)', NULL),
(531, 'fa', 'fa-train', NULL),
(532, 'fa', 'fa-transgender', NULL),
(533, 'fa', 'fa-transgender-alt', NULL),
(534, 'fa', 'fa-trash', NULL),
(535, 'fa', 'fa-trash-o', NULL),
(536, 'fa', 'fa-tree', NULL),
(537, 'fa', 'fa-trello', NULL),
(538, 'fa', 'fa-trophy', NULL),
(539, 'fa', 'fa-truck', NULL),
(540, 'fa', 'fa-try', NULL),
(541, 'fa', 'fa-tty', NULL),
(542, 'fa', 'fa-tumblr', NULL),
(543, 'fa', 'fa-tumblr-square', NULL),
(544, 'fa', 'fa-turkish-lira(alias)', NULL),
(545, 'fa', 'fa-twitch', NULL),
(546, 'fa', 'fa-twitter', NULL),
(547, 'fa', 'fa-twitter-square', NULL),
(548, 'fa', 'fa-umbrella', NULL),
(549, 'fa', 'fa-underline', NULL),
(550, 'fa', 'fa-undo', NULL),
(551, 'fa', 'fa-university', NULL),
(552, 'fa', 'fa-unlink(alias)', NULL),
(553, 'fa', 'fa-unlock', NULL),
(554, 'fa', 'fa-unlock-alt', NULL),
(555, 'fa', 'fa-unsorted(alias)', NULL),
(556, 'fa', 'fa-upload', NULL),
(557, 'fa', 'fa-usd', NULL),
(558, 'fa', 'fa-user', NULL),
(559, 'fa', 'fa-user-md', NULL),
(560, 'fa', 'fa-user-plus', NULL),
(561, 'fa', 'fa-user-secret', NULL),
(562, 'fa', 'fa-user-times', NULL),
(563, 'fa', 'fa-users', NULL),
(564, 'fa', 'fa-venus', NULL),
(565, 'fa', 'fa-venus-double', NULL),
(566, 'fa', 'fa-venus-mars', NULL),
(567, 'fa', 'fa-viacoin', NULL),
(568, 'fa', 'fa-video-camera', NULL),
(569, 'fa', 'fa-vimeo-square', NULL),
(570, 'fa', 'fa-vine', NULL),
(571, 'fa', 'fa-vk', NULL),
(572, 'fa', 'fa-volume-down', NULL),
(573, 'fa', 'fa-volume-off', NULL),
(574, 'fa', 'fa-volume-up', NULL),
(575, 'fa', 'fa-warning(alias)', NULL),
(576, 'fa', 'fa-wechat(alias)', NULL),
(577, 'fa', 'fa-weibo', NULL),
(578, 'fa', 'fa-weixin', NULL),
(579, 'fa', 'fa-whatsapp', NULL),
(580, 'fa', 'fa-wheelchair', NULL),
(581, 'fa', 'fa-wifi', NULL),
(582, 'fa', 'fa-windows', NULL),
(583, 'fa', 'fa-won(alias)', NULL),
(584, 'fa', 'fa-wordpress', NULL),
(585, 'fa', 'fa-wrench', NULL),
(586, 'fa', 'fa-xing', NULL),
(587, 'fa', 'fa-xing-square', NULL),
(588, 'fa', 'fa-yahoo', NULL),
(589, 'fa', 'fa-yelp', NULL),
(590, 'fa', 'fa-yen(alias)', NULL),
(591, 'fa', 'fa-youtube', NULL),
(592, 'fa', 'fa-youtube-play', NULL),
(593, 'fa', 'fa-youtube-square', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `lft` int(11) NOT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `label`, `link`, `icon`, `parent`, `permissions`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Root Menu', '#', NULL, NULL, NULL, 1, 30, 0, 1, '2016-09-20 18:30:00', '2018-11-04 11:12:31'),
(2, 'MENU MANAGEMENT', 'menu/list', NULL, 1, '[\"menu.add\",\"admin\"]', 2, 3, 1, 1, '2016-09-20 18:30:00', '2017-12-24 11:09:01'),
(3, 'BRANCH MANAGEMENT', 'branch/list', NULL, 1, '[\"branch.list\",\"branch.add\",\"branch.edit\",\"branch.delete\",\"admin\"]', 16, 17, 1, 1, '2016-09-21 05:25:43', '2017-12-24 11:09:01'),
(4, 'USER MANAGEMENT', '#', NULL, 1, '[\"user.list\",\"user.add\",\"user.edit\",\"user.delete\",\"admin\"]', 6, 15, 1, 1, '2016-09-21 05:26:25', '2017-12-24 11:09:01'),
(5, 'PERMISSION', 'permission/list', NULL, 4, '[\"admin\"]', 7, 8, 2, 1, '2016-09-21 05:26:51', '2017-12-24 11:09:01'),
(6, 'ROLE', 'user/role/list', NULL, 4, '[\"admin\"]', 9, 10, 2, 1, '2016-09-21 05:27:15', '2017-12-24 11:09:01'),
(7, 'USER', 'user/list', NULL, 4, '[\"user.list\",\"user.add\",\"user.edit\",\"user.delete\",\"admin\"]', 11, 12, 2, 1, '2016-09-21 05:27:51', '2017-12-24 11:09:01'),
(8, 'DEALER MANAGEMENT', '#', NULL, 1, '[\"admin\"]', 20, 21, 1, 1, '2018-11-02 15:24:04', '2018-11-02 16:14:57'),
(9, 'SERVICE PROVIDER MANAGEMENT', '#', NULL, 1, '[\"admin\"]', 22, 23, 1, 1, '2018-11-02 15:24:37', '2018-11-04 11:15:01'),
(10, 'Customer Management', 'customer/list', NULL, 1, '[\"customer.add\",\"customer.edit\",\"customer.delete\",\"customer.list\",\"admin\"]', 18, 19, 1, 1, '2018-11-02 16:14:57', '2018-11-02 16:14:57'),
(13, 'Channel Management', '#', NULL, 1, '[\"channel.category.add\",\"channel.category.list\",\"channel.category.delete\",\"channel.category.edit\",\"admin\"]', 26, 29, 1, 1, '2018-11-04 11:11:28', '2018-11-04 11:15:02'),
(14, 'CHANNEL CATEGORY', 'channel/category/list', NULL, 13, '[\"channel.category.add\",\"channel.category.list\",\"channel.category.delete\",\"channel.category.edit\",\"admin\"]', 27, 28, 2, 1, '2018-11-04 11:12:30', '2018-11-04 11:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2018_11_04_100613_create_service_provider_table', 1),
('2018_11_04_150302_create_channel_catagory_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'user', 'Normal Registered User', 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:24:39'),
(2, 'menu.add', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-03 02:32:41'),
(3, 'menu.list', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:24:54'),
(4, 'menu.edit', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:24:57'),
(5, 'menu.status', NULL, 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:25:01'),
(6, 'admin', 'Super Admin Permission', 1, 1, '2015-07-24 18:30:00', '2015-07-24 18:30:00'),
(7, 'index', 'Home Page Permission', 1, 1, '2015-07-24 18:30:00', '2015-12-01 23:25:03'),
(8, 'menu.delete', NULL, 1, 1, '2015-09-06 09:00:06', '2015-09-06 09:00:09'),
(9, 'user.add', NULL, 1, 1, '2015-10-15 18:30:00', '2015-10-15 18:30:00'),
(10, 'user.edit', NULL, 1, 1, '2015-10-15 18:30:00', '2015-10-15 18:30:00'),
(11, 'user.delete', NULL, 1, 1, '2015-10-15 18:30:00', '2015-10-15 18:30:00'),
(12, 'user.list', NULL, 1, 1, '2015-10-19 18:30:00', '2015-10-20 14:31:57'),
(13, 'user.role.add', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(14, 'user.role.edit', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(15, 'user.role.list', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(16, 'user.role.delete', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(17, 'permission.add', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(18, 'permission.edit', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(19, 'permission.delete', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(20, 'permission.list', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(21, 'permission.group.add', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(22, 'permission.group.edit', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(23, 'permission.group.list', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(24, 'permission.group.delete', NULL, 1, 1, '2015-10-21 18:30:00', '2015-10-21 18:30:00'),
(25, 'user.status', NULL, 1, 1, '2015-12-18 18:30:47', '2015-12-18 18:30:47'),
(27, 'dashboard', 'DASHBOARD', 1, 1, '2017-01-23 13:18:39', '2017-01-23 13:18:39'),
(53, 'customer.add', 'New Customer', 1, 1, '2018-11-02 16:12:55', '2018-11-02 16:12:55'),
(54, 'customer.edit', 'Update Customer', 1, 1, '2018-11-02 16:13:19', '2018-11-02 16:13:19'),
(55, 'customer.delete', 'Delete Customer', 1, 1, '2018-11-02 16:13:42', '2018-11-02 16:13:42'),
(56, 'customer.list', 'Customer List', 1, 1, '2018-11-02 16:13:57', '2018-11-02 16:13:57'),
(57, 'channel.add', '', 1, 1, '2018-11-04 11:01:22', '2018-11-04 11:01:22'),
(58, 'channel.category.add', '', 1, 1, '2018-11-04 11:03:24', '2018-11-04 11:05:18'),
(59, 'channel.category.list', 'view channel category list', 1, 1, '2018-11-04 11:07:42', '2018-11-04 11:07:42'),
(60, 'channel.category.delete', '', 1, 1, '2018-11-04 11:07:56', '2018-11-04 11:07:56'),
(61, 'channel.category.edit', '', 1, 1, '2018-11-04 11:08:06', '2018-11-04 11:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(7, 1, 'aVYJcuD4yC1ib02pQ6rS5ow81K5u8h4K', '2018-11-02 16:17:57', '2018-11-02 16:17:57'),
(10, 1, 'nhoLcHGc0UJabG5Jm4jo5XNFy7GEOv4K', '2018-11-03 06:10:55', '2018-11-03 06:10:55'),
(11, 1, 'VjVfW8301NSKCC9Yt6QlZC8Bv9ehHQaE', '2018-11-03 09:40:34', '2018-11-03 09:40:34'),
(12, 1, 'MxEWgLs3On72bXzRGMx3XLXIlOu9qT5W', '2018-11-04 03:40:43', '2018-11-04 03:40:43'),
(13, 1, '3K5dGSXC8t9Ch9HMfnO5aMHnjCSfIkyR', '2018-11-04 04:01:54', '2018-11-04 04:01:54'),
(14, 1, 'vKElixav9D5G0iXPQwRK0qnB1O2jTrUX', '2018-11-04 04:26:49', '2018-11-04 04:26:49'),
(15, 1, 'jzz1Q9DAxx9rjoVmBpQQOzUC9TVH8wWZ', '2018-11-04 05:42:34', '2018-11-04 05:42:34'),
(27, 1, 'YS4VWnocHke4YBp8fpn3CQhKiRjM1HVs', '2018-11-04 11:16:39', '2018-11-04 11:16:39');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'developer', 'Developer', '{\"admin\":true}', 1, '2018-11-02 15:54:30', '2018-11-02 15:54:32'),
(2, 'dealer', 'Dealer', '{\"index\":true,\"dashboard\":true,\"customer.add\":true,\"customer.edit\":true,\"customer.delete\":true,\"customer.list\":true,\"channel.category.add\":true,\"channel.category.list\":true,\"channel.category.delete\":true,\"channel.category.edit\":true}', 1, '2018-11-02 15:55:14', '2018-11-04 11:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-11-02 16:07:30', '2018-11-02 16:07:30'),
(2, 2, '2018-11-02 16:07:31', '2018-11-02 16:07:31');

-- --------------------------------------------------------

--
-- Table structure for table `sa_branch`
--

CREATE TABLE `sa_branch` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_branch`
--

INSERT INTO `sa_branch` (`id`, `name`, `code`, `address`, `tel`, `city_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HEADOFFICE', '-', 'NEGAMBO', '756388155', 1, 1, '2016-09-29 16:31:30', '2016-10-13 09:48:48', NULL),
(2, 'KANDY', 'K-001', 'Kandy', '7995', 2, 1, '2017-01-18 19:03:04', '2017-01-18 19:03:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_channel_catagory`
--

CREATE TABLE `sa_channel_catagory` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sa_channel_catagory`
--

INSERT INTO `sa_channel_catagory` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'test tested', 'something ', '2018-11-04 09:46:17', '2018-11-04 11:16:22'),
(4, 'test 2', 'updated', '2018-11-04 10:20:58', '2018-11-04 10:21:10');

-- --------------------------------------------------------

--
-- Table structure for table `sa_city`
--

CREATE TABLE `sa_city` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_city`
--

INSERT INTO `sa_city` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Galle', '2018-03-27 19:37:19', '2018-03-27 19:37:19'),
(2, 'Colombo', '2018-03-27 19:37:19', '2018-03-27 19:37:19');

-- --------------------------------------------------------

--
-- Table structure for table `sa_serviceprovider`
--

CREATE TABLE `sa_serviceprovider` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sa_serviceprovider`
--

INSERT INTO `sa_serviceprovider` (`id`, `name`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'test updated again', 'service-provider-logo-20181104135333.png', '2018-11-04 05:21:55', '2018-11-04 08:23:33'),
(2, 'test 2', 'service-provider-logo-20181104105210.png', '2018-11-04 05:22:10', '2018-11-04 05:22:10'),
(6, 'sample', '', '2018-11-04 06:10:31', '2018-11-04 06:10:31'),
(7, 'sample2', 'service-provider-logo-20181104114047.png', '2018-11-04 06:10:47', '2018-11-04 06:10:47');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2015-07-11 06:05:27', '2015-07-11 06:05:27'),
(2, NULL, 'ip', '127.0.0.1', '2015-07-11 06:05:28', '2015-07-11 06:05:28'),
(3, NULL, 'global', NULL, '2015-07-11 06:06:28', '2015-07-11 06:06:28'),
(4, NULL, 'ip', '127.0.0.1', '2015-07-11 06:06:28', '2015-07-11 06:06:28'),
(5, NULL, 'global', NULL, '2015-07-13 04:44:23', '2015-07-13 04:44:23'),
(6, NULL, 'ip', '127.0.0.1', '2015-07-13 04:44:23', '2015-07-13 04:44:23'),
(7, 1, 'user', NULL, '2015-07-13 04:44:23', '2015-07-13 04:44:23'),
(8, NULL, 'global', NULL, '2015-07-26 10:24:02', '2015-07-26 10:24:02'),
(9, NULL, 'ip', '127.0.0.1', '2015-07-26 10:24:02', '2015-07-26 10:24:02'),
(10, 1, 'user', NULL, '2015-07-26 10:24:02', '2015-07-26 10:24:02'),
(11, NULL, 'global', NULL, '2015-08-13 09:48:09', '2015-08-13 09:48:09'),
(12, NULL, 'ip', '127.0.0.1', '2015-08-13 09:48:09', '2015-08-13 09:48:09'),
(13, 1, 'user', NULL, '2015-08-13 09:48:09', '2015-08-13 09:48:09'),
(14, NULL, 'global', NULL, '2015-08-13 09:50:45', '2015-08-13 09:50:45'),
(15, NULL, 'ip', '127.0.0.1', '2015-08-13 09:50:45', '2015-08-13 09:50:45'),
(16, 1, 'user', NULL, '2015-08-13 09:50:45', '2015-08-13 09:50:45'),
(17, NULL, 'global', NULL, '2015-08-18 03:31:17', '2015-08-18 03:31:17'),
(18, NULL, 'ip', '127.0.0.1', '2015-08-18 03:31:17', '2015-08-18 03:31:17'),
(19, 1, 'user', NULL, '2015-08-18 03:31:17', '2015-08-18 03:31:17'),
(20, NULL, 'global', NULL, '2015-08-20 03:57:44', '2015-08-20 03:57:44'),
(21, NULL, 'ip', '127.0.0.1', '2015-08-20 03:57:44', '2015-08-20 03:57:44'),
(22, 1, 'user', NULL, '2015-08-20 03:57:45', '2015-08-20 03:57:45'),
(23, NULL, 'global', NULL, '2015-08-20 04:52:53', '2015-08-20 04:52:53'),
(24, NULL, 'ip', '127.0.0.1', '2015-08-20 04:52:53', '2015-08-20 04:52:53'),
(25, 1, 'user', NULL, '2015-08-20 04:52:54', '2015-08-20 04:52:54'),
(26, NULL, 'global', NULL, '2015-08-20 04:53:06', '2015-08-20 04:53:06'),
(27, NULL, 'ip', '127.0.0.1', '2015-08-20 04:53:06', '2015-08-20 04:53:06'),
(28, 1, 'user', NULL, '2015-08-20 04:53:06', '2015-08-20 04:53:06'),
(29, NULL, 'global', NULL, '2015-08-20 04:53:09', '2015-08-20 04:53:09'),
(30, NULL, 'ip', '127.0.0.1', '2015-08-20 04:53:09', '2015-08-20 04:53:09'),
(31, 1, 'user', NULL, '2015-08-20 04:53:09', '2015-08-20 04:53:09'),
(32, NULL, 'global', NULL, '2015-08-20 04:55:59', '2015-08-20 04:55:59'),
(33, NULL, 'ip', '127.0.0.1', '2015-08-20 04:55:59', '2015-08-20 04:55:59'),
(34, 1, 'user', NULL, '2015-08-20 04:55:59', '2015-08-20 04:55:59'),
(35, NULL, 'global', NULL, '2015-08-20 04:56:18', '2015-08-20 04:56:18'),
(36, NULL, 'ip', '127.0.0.1', '2015-08-20 04:56:19', '2015-08-20 04:56:19'),
(37, 1, 'user', NULL, '2015-08-20 04:56:19', '2015-08-20 04:56:19'),
(38, NULL, 'global', NULL, '2015-08-20 04:57:25', '2015-08-20 04:57:25'),
(39, NULL, 'ip', '127.0.0.1', '2015-08-20 04:57:25', '2015-08-20 04:57:25'),
(40, 1, 'user', NULL, '2015-08-20 04:57:25', '2015-08-20 04:57:25'),
(41, NULL, 'global', NULL, '2015-08-24 15:06:12', '2015-08-24 15:06:12'),
(42, NULL, 'ip', '127.0.0.1', '2015-08-24 15:06:12', '2015-08-24 15:06:12'),
(43, NULL, 'global', NULL, '2015-08-24 15:08:25', '2015-08-24 15:08:25'),
(44, NULL, 'ip', '127.0.0.1', '2015-08-24 15:08:25', '2015-08-24 15:08:25'),
(45, NULL, 'global', NULL, '2015-08-24 15:09:09', '2015-08-24 15:09:09'),
(46, NULL, 'ip', '127.0.0.1', '2015-08-24 15:09:09', '2015-08-24 15:09:09'),
(47, NULL, 'global', NULL, '2015-08-24 15:09:44', '2015-08-24 15:09:44'),
(48, NULL, 'ip', '127.0.0.1', '2015-08-24 15:09:44', '2015-08-24 15:09:44'),
(49, NULL, 'global', NULL, '2015-08-24 15:09:49', '2015-08-24 15:09:49'),
(50, NULL, 'ip', '127.0.0.1', '2015-08-24 15:09:50', '2015-08-24 15:09:50'),
(51, NULL, 'global', NULL, '2015-08-24 15:11:29', '2015-08-24 15:11:29'),
(52, NULL, 'ip', '127.0.0.1', '2015-08-24 15:11:29', '2015-08-24 15:11:29'),
(53, NULL, 'global', NULL, '2015-08-25 07:26:45', '2015-08-25 07:26:45'),
(54, NULL, 'ip', '127.0.0.1', '2015-08-25 07:26:45', '2015-08-25 07:26:45'),
(55, NULL, 'global', NULL, '2015-08-26 07:48:20', '2015-08-26 07:48:20'),
(56, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:21', '2015-08-26 07:48:21'),
(57, NULL, 'global', NULL, '2015-08-26 07:48:23', '2015-08-26 07:48:23'),
(58, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:23', '2015-08-26 07:48:23'),
(59, NULL, 'global', NULL, '2015-08-26 07:48:27', '2015-08-26 07:48:27'),
(60, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:27', '2015-08-26 07:48:27'),
(61, NULL, 'global', NULL, '2015-08-26 07:48:31', '2015-08-26 07:48:31'),
(62, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:31', '2015-08-26 07:48:31'),
(63, NULL, 'global', NULL, '2015-08-26 07:48:36', '2015-08-26 07:48:36'),
(64, NULL, 'ip', '192.168.1.35', '2015-08-26 07:48:36', '2015-08-26 07:48:36'),
(65, NULL, 'global', NULL, '2015-08-26 07:48:48', '2015-08-26 07:48:48'),
(66, NULL, 'global', NULL, '2015-08-27 03:20:50', '2015-08-27 03:20:50'),
(67, NULL, 'ip', '127.0.0.1', '2015-08-27 03:20:50', '2015-08-27 03:20:50'),
(68, 1, 'user', NULL, '2015-08-27 03:20:50', '2015-08-27 03:20:50'),
(69, NULL, 'global', NULL, '2015-08-30 06:42:57', '2015-08-30 06:42:57'),
(70, NULL, 'ip', '127.0.0.1', '2015-08-30 06:42:57', '2015-08-30 06:42:57'),
(71, NULL, 'global', NULL, '2015-08-30 06:51:13', '2015-08-30 06:51:13'),
(72, NULL, 'ip', '127.0.0.1', '2015-08-30 06:51:14', '2015-08-30 06:51:14'),
(73, NULL, 'global', NULL, '2015-09-06 11:03:36', '2015-09-06 11:03:36'),
(74, NULL, 'ip', '127.0.0.1', '2015-09-06 11:03:36', '2015-09-06 11:03:36'),
(75, NULL, 'global', NULL, '2015-09-18 05:45:18', '2015-09-18 05:45:18'),
(76, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:18', '2015-09-18 05:45:18'),
(77, NULL, 'global', NULL, '2015-09-18 05:45:22', '2015-09-18 05:45:22'),
(78, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:22', '2015-09-18 05:45:22'),
(79, NULL, 'global', NULL, '2015-09-18 05:45:30', '2015-09-18 05:45:30'),
(80, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:30', '2015-09-18 05:45:30'),
(81, NULL, 'global', NULL, '2015-09-18 05:45:34', '2015-09-18 05:45:34'),
(82, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:34', '2015-09-18 05:45:34'),
(83, NULL, 'global', NULL, '2015-09-18 05:45:40', '2015-09-18 05:45:40'),
(84, NULL, 'ip', '192.168.1.15', '2015-09-18 05:45:40', '2015-09-18 05:45:40'),
(85, NULL, 'global', NULL, '2015-10-29 13:37:26', '2015-10-29 13:37:26'),
(86, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:26', '2015-10-29 13:37:26'),
(87, 1, 'user', NULL, '2015-10-29 13:37:26', '2015-10-29 13:37:26'),
(88, NULL, 'global', NULL, '2015-10-29 13:37:30', '2015-10-29 13:37:30'),
(89, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:30', '2015-10-29 13:37:30'),
(90, 1, 'user', NULL, '2015-10-29 13:37:30', '2015-10-29 13:37:30'),
(91, NULL, 'global', NULL, '2015-10-29 13:37:34', '2015-10-29 13:37:34'),
(92, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:34', '2015-10-29 13:37:34'),
(93, 1, 'user', NULL, '2015-10-29 13:37:34', '2015-10-29 13:37:34'),
(94, NULL, 'global', NULL, '2015-10-29 13:37:41', '2015-10-29 13:37:41'),
(95, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:41', '2015-10-29 13:37:41'),
(96, 1, 'user', NULL, '2015-10-29 13:37:41', '2015-10-29 13:37:41'),
(97, NULL, 'global', NULL, '2015-10-29 13:37:48', '2015-10-29 13:37:48'),
(98, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:48', '2015-10-29 13:37:48'),
(99, 1, 'user', NULL, '2015-10-29 13:37:48', '2015-10-29 13:37:48'),
(100, NULL, 'global', NULL, '2015-10-29 13:37:52', '2015-10-29 13:37:52'),
(101, NULL, 'ip', '127.0.0.1', '2015-10-29 13:37:52', '2015-10-29 13:37:52'),
(102, 1, 'user', NULL, '2015-10-29 13:37:52', '2015-10-29 13:37:52'),
(103, NULL, 'global', NULL, '2015-11-04 03:13:02', '2015-11-04 03:13:02'),
(104, NULL, 'ip', '127.0.0.1', '2015-11-04 03:13:02', '2015-11-04 03:13:02'),
(105, 1, 'user', NULL, '2015-11-04 03:13:02', '2015-11-04 03:13:02'),
(106, NULL, 'global', NULL, '2015-11-07 11:26:55', '2015-11-07 11:26:55'),
(107, NULL, 'ip', '127.0.0.1', '2015-11-07 11:26:55', '2015-11-07 11:26:55'),
(108, NULL, 'global', NULL, '2015-11-07 11:27:01', '2015-11-07 11:27:01'),
(109, NULL, 'ip', '127.0.0.1', '2015-11-07 11:27:01', '2015-11-07 11:27:01'),
(110, NULL, 'global', NULL, '2015-11-18 18:09:36', '2015-11-18 18:09:36'),
(111, NULL, 'ip', '127.0.0.1', '2015-11-18 18:09:36', '2015-11-18 18:09:36'),
(112, NULL, 'global', NULL, '2015-11-18 18:09:43', '2015-11-18 18:09:43'),
(113, NULL, 'ip', '127.0.0.1', '2015-11-18 18:09:43', '2015-11-18 18:09:43'),
(114, NULL, 'global', NULL, '2015-11-18 18:12:14', '2015-11-18 18:12:14'),
(115, NULL, 'ip', '127.0.0.1', '2015-11-18 18:12:14', '2015-11-18 18:12:14'),
(116, NULL, 'global', NULL, '2015-11-18 18:12:27', '2015-11-18 18:12:27'),
(117, NULL, 'ip', '127.0.0.1', '2015-11-18 18:12:27', '2015-11-18 18:12:27'),
(118, NULL, 'global', NULL, '2015-11-18 18:12:36', '2015-11-18 18:12:36'),
(119, NULL, 'ip', '127.0.0.1', '2015-11-18 18:12:36', '2015-11-18 18:12:36'),
(120, NULL, 'global', NULL, '2015-11-18 20:27:03', '2015-11-18 20:27:03'),
(121, NULL, 'ip', '::1', '2015-11-18 20:27:03', '2015-11-18 20:27:03'),
(122, NULL, 'global', NULL, '2015-11-18 20:27:06', '2015-11-18 20:27:06'),
(123, NULL, 'ip', '::1', '2015-11-18 20:27:06', '2015-11-18 20:27:06'),
(124, NULL, 'global', NULL, '2015-11-18 21:26:24', '2015-11-18 21:26:24'),
(125, NULL, 'ip', '::1', '2015-11-18 21:26:24', '2015-11-18 21:26:24'),
(126, 1, 'user', NULL, '2015-11-18 21:26:24', '2015-11-18 21:26:24'),
(127, NULL, 'global', NULL, '2015-11-18 21:26:27', '2015-11-18 21:26:27'),
(128, NULL, 'ip', '::1', '2015-11-18 21:26:27', '2015-11-18 21:26:27'),
(129, 1, 'user', NULL, '2015-11-18 21:26:27', '2015-11-18 21:26:27'),
(130, NULL, 'global', NULL, '2015-11-23 18:13:05', '2015-11-23 18:13:05'),
(131, NULL, 'ip', '::1', '2015-11-23 18:13:05', '2015-11-23 18:13:05'),
(132, 1, 'user', NULL, '2015-11-23 18:13:05', '2015-11-23 18:13:05'),
(133, NULL, 'global', NULL, '2015-11-24 01:14:15', '2015-11-24 01:14:15'),
(134, NULL, 'ip', '::1', '2015-11-24 01:14:15', '2015-11-24 01:14:15'),
(135, NULL, 'global', NULL, '2015-12-03 01:12:12', '2015-12-03 01:12:12'),
(136, NULL, 'ip', '::1', '2015-12-03 01:12:12', '2015-12-03 01:12:12'),
(137, 1, 'user', NULL, '2015-12-03 01:12:12', '2015-12-03 01:12:12'),
(138, NULL, 'global', NULL, '2015-12-03 02:32:03', '2015-12-03 02:32:03'),
(139, NULL, 'ip', '::1', '2015-12-03 02:32:03', '2015-12-03 02:32:03'),
(140, 1, 'user', NULL, '2015-12-03 02:32:03', '2015-12-03 02:32:03'),
(141, NULL, 'global', NULL, '2015-12-03 17:42:59', '2015-12-03 17:42:59'),
(142, NULL, 'ip', '127.0.0.1', '2015-12-03 17:42:59', '2015-12-03 17:42:59'),
(143, 1, 'user', NULL, '2015-12-03 17:42:59', '2015-12-03 17:42:59'),
(144, NULL, 'global', NULL, '2015-12-03 17:43:02', '2015-12-03 17:43:02'),
(145, NULL, 'ip', '127.0.0.1', '2015-12-03 17:43:02', '2015-12-03 17:43:02'),
(146, 1, 'user', NULL, '2015-12-03 17:43:02', '2015-12-03 17:43:02'),
(147, NULL, 'global', NULL, '2015-12-18 22:28:50', '2015-12-18 22:28:50'),
(148, NULL, 'ip', '127.0.0.1', '2015-12-18 22:28:50', '2015-12-18 22:28:50'),
(149, 1, 'user', NULL, '2015-12-18 22:28:50', '2015-12-18 22:28:50'),
(150, NULL, 'global', NULL, '2016-01-09 00:28:55', '2016-01-09 00:28:55'),
(151, NULL, 'ip', '192.168.1.121', '2016-01-09 00:28:55', '2016-01-09 00:28:55'),
(153, NULL, 'global', NULL, '2016-01-09 00:29:26', '2016-01-09 00:29:26'),
(154, NULL, 'ip', '192.168.1.121', '2016-01-09 00:29:26', '2016-01-09 00:29:26'),
(156, NULL, 'global', NULL, '2016-01-09 00:29:41', '2016-01-09 00:29:41'),
(157, NULL, 'ip', '192.168.1.121', '2016-01-09 00:29:41', '2016-01-09 00:29:41'),
(159, NULL, 'global', NULL, '2016-01-09 00:30:07', '2016-01-09 00:30:07'),
(160, NULL, 'ip', '192.168.1.121', '2016-01-09 00:30:07', '2016-01-09 00:30:07'),
(162, NULL, 'global', NULL, '2016-01-11 17:50:41', '2016-01-11 17:50:41'),
(163, NULL, 'ip', '192.168.1.142', '2016-01-11 17:50:41', '2016-01-11 17:50:41'),
(165, NULL, 'global', NULL, '2016-01-11 17:50:50', '2016-01-11 17:50:50'),
(166, NULL, 'ip', '192.168.1.142', '2016-01-11 17:50:50', '2016-01-11 17:50:50'),
(168, NULL, 'global', NULL, '2016-01-11 17:51:03', '2016-01-11 17:51:03'),
(169, NULL, 'ip', '192.168.1.142', '2016-01-11 17:51:03', '2016-01-11 17:51:03'),
(171, NULL, 'global', NULL, '2016-01-11 17:51:16', '2016-01-11 17:51:16'),
(172, NULL, 'ip', '192.168.1.142', '2016-01-11 17:51:16', '2016-01-11 17:51:16'),
(174, NULL, 'global', NULL, '2016-01-11 17:52:23', '2016-01-11 17:52:23'),
(175, NULL, 'ip', '192.168.1.142', '2016-01-11 17:52:23', '2016-01-11 17:52:23'),
(177, NULL, 'global', NULL, '2016-01-11 17:52:58', '2016-01-11 17:52:58'),
(178, NULL, 'ip', '192.168.1.142', '2016-01-11 17:52:58', '2016-01-11 17:52:58'),
(180, NULL, 'global', NULL, '2016-01-11 18:06:27', '2016-01-11 18:06:27'),
(181, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:27', '2016-01-11 18:06:27'),
(183, NULL, 'global', NULL, '2016-01-11 18:06:33', '2016-01-11 18:06:33'),
(184, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:33', '2016-01-11 18:06:33'),
(186, NULL, 'global', NULL, '2016-01-11 18:06:36', '2016-01-11 18:06:36'),
(187, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:36', '2016-01-11 18:06:36'),
(189, NULL, 'global', NULL, '2016-01-11 18:06:41', '2016-01-11 18:06:41'),
(190, NULL, 'ip', '192.168.1.142', '2016-01-11 18:06:41', '2016-01-11 18:06:41'),
(192, NULL, 'global', NULL, '2016-01-11 18:18:58', '2016-01-11 18:18:58'),
(193, NULL, 'ip', '192.168.1.142', '2016-01-11 18:18:58', '2016-01-11 18:18:58'),
(195, NULL, 'global', NULL, '2016-01-24 06:25:32', '2016-01-24 06:25:32'),
(196, NULL, 'ip', '127.0.0.1', '2016-01-24 06:25:32', '2016-01-24 06:25:32'),
(197, NULL, 'global', NULL, '2016-01-26 21:27:09', '2016-01-26 21:27:09'),
(198, NULL, 'ip', '127.0.0.1', '2016-01-26 21:27:09', '2016-01-26 21:27:09'),
(200, NULL, 'global', NULL, '2016-01-28 17:10:03', '2016-01-28 17:10:03'),
(201, NULL, 'ip', '127.0.0.1', '2016-01-28 17:10:03', '2016-01-28 17:10:03'),
(202, 1, 'user', NULL, '2016-01-28 17:10:03', '2016-01-28 17:10:03'),
(203, NULL, 'global', NULL, '2016-02-10 17:29:12', '2016-02-10 17:29:12'),
(204, NULL, 'ip', '127.0.0.1', '2016-02-10 17:29:12', '2016-02-10 17:29:12'),
(205, 1, 'user', NULL, '2016-02-10 17:29:12', '2016-02-10 17:29:12'),
(206, NULL, 'global', NULL, '2016-02-24 06:45:41', '2016-02-24 06:45:41'),
(207, NULL, 'ip', '192.168.1.59', '2016-02-24 06:45:41', '2016-02-24 06:45:41'),
(209, NULL, 'global', NULL, '2016-02-24 06:47:03', '2016-02-24 06:47:03'),
(210, NULL, 'ip', '192.168.1.59', '2016-02-24 06:47:03', '2016-02-24 06:47:03'),
(212, NULL, 'global', NULL, '2016-02-24 06:47:06', '2016-02-24 06:47:06'),
(213, NULL, 'ip', '192.168.1.59', '2016-02-24 06:47:06', '2016-02-24 06:47:06'),
(215, NULL, 'global', NULL, '2016-03-02 03:47:00', '2016-03-02 03:47:00'),
(216, NULL, 'ip', '127.0.0.1', '2016-03-02 03:47:00', '2016-03-02 03:47:00'),
(217, 1, 'user', NULL, '2016-03-02 03:47:00', '2016-03-02 03:47:00'),
(218, NULL, 'global', NULL, '2016-03-28 09:32:49', '2016-03-28 09:32:49'),
(219, NULL, 'ip', '127.0.0.1', '2016-03-28 09:32:49', '2016-03-28 09:32:49'),
(220, NULL, 'global', NULL, '2016-03-28 09:32:56', '2016-03-28 09:32:56'),
(221, NULL, 'ip', '127.0.0.1', '2016-03-28 09:32:56', '2016-03-28 09:32:56'),
(222, 1, 'user', NULL, '2016-03-28 09:32:56', '2016-03-28 09:32:56'),
(223, NULL, 'global', NULL, '2016-03-30 11:37:40', '2016-03-30 11:37:40'),
(224, NULL, 'ip', '::1', '2016-03-30 11:37:41', '2016-03-30 11:37:41'),
(225, NULL, 'global', NULL, '2016-05-02 04:01:37', '2016-05-02 04:01:37'),
(226, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:37', '2016-05-02 04:01:37'),
(227, NULL, 'global', NULL, '2016-05-02 04:01:40', '2016-05-02 04:01:40'),
(228, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:40', '2016-05-02 04:01:40'),
(229, NULL, 'global', NULL, '2016-05-02 04:01:43', '2016-05-02 04:01:43'),
(230, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:43', '2016-05-02 04:01:43'),
(231, NULL, 'global', NULL, '2016-05-02 04:01:49', '2016-05-02 04:01:49'),
(232, NULL, 'ip', '192.168.1.41', '2016-05-02 04:01:49', '2016-05-02 04:01:49'),
(233, NULL, 'global', NULL, '2016-05-02 04:26:12', '2016-05-02 04:26:12'),
(234, NULL, 'ip', '192.168.1.41', '2016-05-02 04:26:12', '2016-05-02 04:26:12'),
(235, NULL, 'global', NULL, '2016-05-02 04:26:40', '2016-05-02 04:26:40'),
(236, NULL, 'ip', '192.168.1.41', '2016-05-02 04:26:40', '2016-05-02 04:26:40'),
(237, NULL, 'global', NULL, '2016-05-02 04:26:58', '2016-05-02 04:26:58'),
(238, NULL, 'ip', '192.168.1.41', '2016-05-02 04:26:58', '2016-05-02 04:26:58'),
(239, NULL, 'global', NULL, '2016-05-02 04:27:01', '2016-05-02 04:27:01'),
(240, NULL, 'ip', '192.168.1.41', '2016-05-02 04:27:01', '2016-05-02 04:27:01'),
(241, NULL, 'global', NULL, '2016-05-02 04:27:07', '2016-05-02 04:27:07'),
(242, NULL, 'ip', '192.168.1.41', '2016-05-02 04:27:07', '2016-05-02 04:27:07'),
(243, NULL, 'global', NULL, '2017-01-15 22:13:01', '2017-01-15 22:13:01'),
(244, NULL, 'ip', '::1', '2017-01-15 22:13:01', '2017-01-15 22:13:01'),
(245, NULL, 'global', NULL, '2017-01-15 22:53:12', '2017-01-15 22:53:12'),
(246, NULL, 'ip', '::1', '2017-01-15 22:53:12', '2017-01-15 22:53:12'),
(248, NULL, 'global', NULL, '2017-01-15 22:53:40', '2017-01-15 22:53:40'),
(249, NULL, 'ip', '::1', '2017-01-15 22:53:40', '2017-01-15 22:53:40'),
(251, NULL, 'global', NULL, '2017-01-18 19:05:37', '2017-01-18 19:05:37'),
(252, NULL, 'ip', '::1', '2017-01-18 19:05:37', '2017-01-18 19:05:37'),
(253, NULL, 'global', NULL, '2017-01-25 06:29:52', '2017-01-25 06:29:52'),
(254, NULL, 'ip', '::1', '2017-01-25 06:29:52', '2017-01-25 06:29:52'),
(255, NULL, 'global', NULL, '2017-04-12 08:55:26', '2017-04-12 08:55:26'),
(256, NULL, 'ip', '112.135.7.121', '2017-04-12 08:55:26', '2017-04-12 08:55:26'),
(257, 9, 'user', NULL, '2017-04-12 08:55:26', '2017-04-12 08:55:26'),
(258, NULL, 'global', NULL, '2017-04-24 06:17:15', '2017-04-24 06:17:15'),
(259, NULL, 'ip', '::1', '2017-04-24 06:17:15', '2017-04-24 06:17:15'),
(260, NULL, 'global', NULL, '2017-04-30 06:53:53', '2017-04-30 06:53:53'),
(261, NULL, 'ip', '123.231.108.3', '2017-04-30 06:53:53', '2017-04-30 06:53:53'),
(262, 9, 'user', NULL, '2017-04-30 06:53:53', '2017-04-30 06:53:53'),
(263, NULL, 'global', NULL, '2017-04-30 06:53:58', '2017-04-30 06:53:58'),
(264, NULL, 'ip', '123.231.108.3', '2017-04-30 06:53:58', '2017-04-30 06:53:58'),
(265, 9, 'user', NULL, '2017-04-30 06:53:58', '2017-04-30 06:53:58'),
(266, NULL, 'global', NULL, '2017-04-30 06:54:05', '2017-04-30 06:54:05'),
(267, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:05', '2017-04-30 06:54:05'),
(268, 9, 'user', NULL, '2017-04-30 06:54:05', '2017-04-30 06:54:05'),
(269, NULL, 'global', NULL, '2017-04-30 06:54:14', '2017-04-30 06:54:14'),
(270, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:14', '2017-04-30 06:54:14'),
(271, 9, 'user', NULL, '2017-04-30 06:54:14', '2017-04-30 06:54:14'),
(272, NULL, 'global', NULL, '2017-04-30 06:54:19', '2017-04-30 06:54:19'),
(273, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:19', '2017-04-30 06:54:19'),
(274, 9, 'user', NULL, '2017-04-30 06:54:19', '2017-04-30 06:54:19'),
(275, NULL, 'global', NULL, '2017-04-30 06:54:24', '2017-04-30 06:54:24'),
(276, NULL, 'ip', '123.231.108.3', '2017-04-30 06:54:24', '2017-04-30 06:54:24'),
(277, 9, 'user', NULL, '2017-04-30 06:54:24', '2017-04-30 06:54:24'),
(278, NULL, 'global', NULL, '2017-08-30 06:27:01', '2017-08-30 06:27:01'),
(279, NULL, 'ip', '113.59.213.196', '2017-08-30 06:27:01', '2017-08-30 06:27:01'),
(280, NULL, 'global', NULL, '2017-09-18 11:32:04', '2017-09-18 11:32:04'),
(281, NULL, 'ip', '::1', '2017-09-18 11:32:04', '2017-09-18 11:32:04'),
(282, NULL, 'global', NULL, '2017-09-26 05:46:17', '2017-09-26 05:46:17'),
(283, NULL, 'ip', '::1', '2017-09-26 05:46:17', '2017-09-26 05:46:17'),
(284, 9, 'user', NULL, '2017-09-26 05:46:17', '2017-09-26 05:46:17'),
(285, NULL, 'global', NULL, '2017-10-17 16:52:42', '2017-10-17 16:52:42'),
(286, NULL, 'ip', '::1', '2017-10-17 16:52:42', '2017-10-17 16:52:42'),
(287, NULL, 'global', NULL, '2018-01-09 15:24:36', '2018-01-09 15:24:36'),
(288, NULL, 'ip', '::1', '2018-01-09 15:24:36', '2018-01-09 15:24:36'),
(289, NULL, 'global', NULL, '2018-02-01 04:54:38', '2018-02-01 04:54:38'),
(290, NULL, 'ip', '::1', '2018-02-01 04:54:38', '2018-02-01 04:54:38'),
(291, 12, 'user', NULL, '2018-02-01 04:54:38', '2018-02-01 04:54:38'),
(292, NULL, 'global', NULL, '2018-02-01 04:54:46', '2018-02-01 04:54:46'),
(293, NULL, 'ip', '::1', '2018-02-01 04:54:46', '2018-02-01 04:54:46'),
(294, 12, 'user', NULL, '2018-02-01 04:54:46', '2018-02-01 04:54:46'),
(295, NULL, 'global', NULL, '2018-02-01 04:55:05', '2018-02-01 04:55:05'),
(296, NULL, 'ip', '::1', '2018-02-01 04:55:05', '2018-02-01 04:55:05'),
(297, 12, 'user', NULL, '2018-02-01 04:55:05', '2018-02-01 04:55:05'),
(298, NULL, 'global', NULL, '2018-02-01 04:56:17', '2018-02-01 04:56:17'),
(299, NULL, 'ip', '::1', '2018-02-01 04:56:17', '2018-02-01 04:56:17'),
(300, 12, 'user', NULL, '2018-02-01 04:56:17', '2018-02-01 04:56:17'),
(301, NULL, 'global', NULL, '2018-02-07 15:23:34', '2018-02-07 15:23:34'),
(302, NULL, 'ip', '::1', '2018-02-07 15:23:34', '2018-02-07 15:23:34'),
(303, 18, 'user', NULL, '2018-02-07 15:23:34', '2018-02-07 15:23:34'),
(304, NULL, 'global', NULL, '2018-02-07 15:28:30', '2018-02-07 15:28:30'),
(305, NULL, 'ip', '::1', '2018-02-07 15:28:30', '2018-02-07 15:28:30'),
(306, 18, 'user', NULL, '2018-02-07 15:28:30', '2018-02-07 15:28:30'),
(307, NULL, 'global', NULL, '2018-02-12 04:48:18', '2018-02-12 04:48:18'),
(308, NULL, 'ip', '::1', '2018-02-12 04:48:18', '2018-02-12 04:48:18'),
(309, NULL, 'global', NULL, '2018-02-14 09:44:29', '2018-02-14 09:44:29'),
(310, NULL, 'ip', '::1', '2018-02-14 09:44:29', '2018-02-14 09:44:29'),
(311, NULL, 'global', NULL, '2018-02-14 09:44:38', '2018-02-14 09:44:38'),
(312, NULL, 'ip', '::1', '2018-02-14 09:44:38', '2018-02-14 09:44:38'),
(313, NULL, 'global', NULL, '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(314, NULL, 'global', NULL, '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(315, NULL, 'ip', '::1', '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(316, NULL, 'ip', '::1', '2018-02-14 09:45:47', '2018-02-14 09:45:47'),
(317, NULL, 'global', NULL, '2018-02-15 16:19:27', '2018-02-15 16:19:27'),
(318, NULL, 'ip', '::1', '2018-02-15 16:19:27', '2018-02-15 16:19:27'),
(319, NULL, 'global', NULL, '2018-02-18 05:14:10', '2018-02-18 05:14:10'),
(320, NULL, 'ip', '::1', '2018-02-18 05:14:10', '2018-02-18 05:14:10'),
(321, NULL, 'global', NULL, '2018-02-18 05:14:16', '2018-02-18 05:14:16'),
(322, NULL, 'ip', '::1', '2018-02-18 05:14:16', '2018-02-18 05:14:16'),
(323, NULL, 'global', NULL, '2018-02-18 05:14:21', '2018-02-18 05:14:21'),
(324, NULL, 'ip', '::1', '2018-02-18 05:14:21', '2018-02-18 05:14:21'),
(325, NULL, 'global', NULL, '2018-02-18 05:14:31', '2018-02-18 05:14:31'),
(326, NULL, 'ip', '::1', '2018-02-18 05:14:31', '2018-02-18 05:14:31'),
(327, NULL, 'global', NULL, '2018-02-18 06:38:25', '2018-02-18 06:38:25'),
(328, NULL, 'ip', '::1', '2018-02-18 06:38:25', '2018-02-18 06:38:25'),
(329, NULL, 'global', NULL, '2018-02-19 17:36:44', '2018-02-19 17:36:44'),
(330, NULL, 'ip', '::1', '2018-02-19 17:36:44', '2018-02-19 17:36:44'),
(331, NULL, 'global', NULL, '2018-02-25 17:41:01', '2018-02-25 17:41:01'),
(332, NULL, 'ip', '::1', '2018-02-25 17:41:01', '2018-02-25 17:41:01'),
(333, NULL, 'global', NULL, '2018-02-26 04:46:57', '2018-02-26 04:46:57'),
(334, NULL, 'ip', '::1', '2018-02-26 04:46:57', '2018-02-26 04:46:57'),
(335, NULL, 'global', NULL, '2018-02-26 04:47:14', '2018-02-26 04:47:14'),
(336, NULL, 'ip', '::1', '2018-02-26 04:47:14', '2018-02-26 04:47:14'),
(337, NULL, 'global', NULL, '2018-02-26 04:47:49', '2018-02-26 04:47:49'),
(338, NULL, 'ip', '::1', '2018-02-26 04:47:49', '2018-02-26 04:47:49'),
(339, NULL, 'global', NULL, '2018-02-26 04:48:00', '2018-02-26 04:48:00'),
(340, NULL, 'ip', '::1', '2018-02-26 04:48:00', '2018-02-26 04:48:00'),
(341, NULL, 'global', NULL, '2018-02-26 04:49:39', '2018-02-26 04:49:39'),
(342, NULL, 'ip', '::1', '2018-02-26 04:49:39', '2018-02-26 04:49:39'),
(343, NULL, 'global', NULL, '2018-02-26 04:53:08', '2018-02-26 04:53:08'),
(344, NULL, 'ip', '::1', '2018-02-26 04:53:08', '2018-02-26 04:53:08'),
(345, NULL, 'global', NULL, '2018-02-26 05:03:13', '2018-02-26 05:03:13'),
(346, NULL, 'ip', '::1', '2018-02-26 05:03:13', '2018-02-26 05:03:13'),
(347, NULL, 'global', NULL, '2018-02-26 05:04:52', '2018-02-26 05:04:52'),
(348, NULL, 'ip', '::1', '2018-02-26 05:04:52', '2018-02-26 05:04:52'),
(349, NULL, 'global', NULL, '2018-02-26 05:08:19', '2018-02-26 05:08:19'),
(350, NULL, 'ip', '::1', '2018-02-26 05:08:19', '2018-02-26 05:08:19'),
(351, NULL, 'global', NULL, '2018-02-26 05:09:22', '2018-02-26 05:09:22'),
(352, NULL, 'ip', '::1', '2018-02-26 05:09:22', '2018-02-26 05:09:22'),
(353, NULL, 'global', NULL, '2018-02-26 05:11:25', '2018-02-26 05:11:25'),
(354, NULL, 'ip', '::1', '2018-02-26 05:11:25', '2018-02-26 05:11:25'),
(355, NULL, 'global', NULL, '2018-02-26 05:12:45', '2018-02-26 05:12:45'),
(356, NULL, 'ip', '::1', '2018-02-26 05:12:45', '2018-02-26 05:12:45'),
(357, NULL, 'global', NULL, '2018-02-26 05:39:05', '2018-02-26 05:39:05'),
(358, NULL, 'ip', '::1', '2018-02-26 05:39:05', '2018-02-26 05:39:05'),
(359, 9, 'user', NULL, '2018-02-26 05:39:05', '2018-02-26 05:39:05'),
(360, NULL, 'global', NULL, '2018-02-26 07:10:30', '2018-02-26 07:10:30'),
(361, NULL, 'ip', '::1', '2018-02-26 07:10:30', '2018-02-26 07:10:30'),
(362, NULL, 'global', NULL, '2018-02-26 14:23:51', '2018-02-26 14:23:51'),
(363, NULL, 'ip', '::1', '2018-02-26 14:23:51', '2018-02-26 14:23:51'),
(364, NULL, 'global', NULL, '2018-02-26 14:24:26', '2018-02-26 14:24:26'),
(365, NULL, 'ip', '::1', '2018-02-26 14:24:26', '2018-02-26 14:24:26'),
(366, NULL, 'global', NULL, '2018-02-26 17:06:07', '2018-02-26 17:06:07'),
(367, NULL, 'ip', '::1', '2018-02-26 17:06:07', '2018-02-26 17:06:07'),
(368, NULL, 'global', NULL, '2018-02-28 05:00:30', '2018-02-28 05:00:30'),
(369, NULL, 'ip', '::1', '2018-02-28 05:00:30', '2018-02-28 05:00:30'),
(370, NULL, 'global', NULL, '2018-02-28 18:09:39', '2018-02-28 18:09:39'),
(371, NULL, 'ip', '::1', '2018-02-28 18:09:39', '2018-02-28 18:09:39'),
(372, NULL, 'global', NULL, '2018-02-28 18:17:32', '2018-02-28 18:17:32'),
(373, NULL, 'ip', '::1', '2018-02-28 18:17:32', '2018-02-28 18:17:32'),
(374, NULL, 'global', NULL, '2018-03-04 11:25:55', '2018-03-04 11:25:55'),
(375, NULL, 'ip', '::1', '2018-03-04 11:25:55', '2018-03-04 11:25:55'),
(376, NULL, 'global', NULL, '2018-03-27 18:24:45', '2018-03-27 18:24:45'),
(377, NULL, 'ip', '::1', '2018-03-27 18:24:45', '2018-03-27 18:24:45'),
(378, NULL, 'global', NULL, '2018-03-27 18:25:49', '2018-03-27 18:25:49'),
(379, NULL, 'ip', '::1', '2018-03-27 18:25:49', '2018-03-27 18:25:49'),
(380, NULL, 'global', NULL, '2018-03-27 18:25:52', '2018-03-27 18:25:52'),
(381, NULL, 'ip', '::1', '2018-03-27 18:25:52', '2018-03-27 18:25:52'),
(382, NULL, 'global', NULL, '2018-03-27 18:26:04', '2018-03-27 18:26:04'),
(383, NULL, 'ip', '::1', '2018-03-27 18:26:04', '2018-03-27 18:26:04'),
(384, NULL, 'global', NULL, '2018-03-27 18:34:31', '2018-03-27 18:34:31'),
(385, NULL, 'ip', '::1', '2018-03-27 18:34:31', '2018-03-27 18:34:31'),
(386, NULL, 'global', NULL, '2018-03-27 18:35:04', '2018-03-27 18:35:04'),
(387, NULL, 'ip', '::1', '2018-03-27 18:35:04', '2018-03-27 18:35:04'),
(388, NULL, 'global', NULL, '2018-11-02 16:01:29', '2018-11-02 16:01:29'),
(389, NULL, 'ip', '::1', '2018-11-02 16:01:29', '2018-11-02 16:01:29'),
(390, NULL, 'global', NULL, '2018-11-03 04:54:53', '2018-11-03 04:54:53'),
(391, NULL, 'ip', '::1', '2018-11-03 04:54:53', '2018-11-03 04:54:53'),
(392, NULL, 'global', NULL, '2018-11-03 04:55:06', '2018-11-03 04:55:06'),
(393, NULL, 'ip', '::1', '2018-11-03 04:55:06', '2018-11-03 04:55:06'),
(394, NULL, 'global', NULL, '2018-11-03 04:56:05', '2018-11-03 04:56:05'),
(395, NULL, 'ip', '::1', '2018-11-03 04:56:05', '2018-11-03 04:56:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fb_id` text COLLATE utf8_unicode_ci NOT NULL,
  `g_id` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` text COLLATE utf8_unicode_ci,
  `branch` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `supervisor_id` int(25) DEFAULT NULL,
  `lft` int(25) DEFAULT NULL,
  `rgt` int(25) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fb_id`, `g_id`, `first_name`, `last_name`, `username`, `email`, `mobile`, `branch`, `password`, `permissions`, `last_login`, `supervisor_id`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
(1, '', '', 'Developer', 'Cybertech', 'developer@cybertech.lk', 'developer@cybertech.lk', NULL, 0, '$2y$10$rbGZ4vVYnbeoZxJctm4AGOF/Z1fq09QFNtfR1QQ/.7bxwp0JkFJAW', NULL, '2018-11-04 11:16:39', NULL, 1, 4, 0, 1, '2018-11-02 16:07:29', '2018-11-04 11:16:39'),
(2, '', '', 'Dealer', 'Cybertech', 'dealer@cybertech.lk', 'dealer@cybertech.lk', NULL, 0, '$2y$10$unetgy8st5R.OkBV0ILfVOXEcttIo5c2C8SuaQar.MiVVAQVMpedm', NULL, '2018-11-04 11:15:46', 1, 2, 3, 1, 1, '2018-11-02 16:07:30', '2018-11-04 11:16:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fonts-list`
--
ALTER TABLE `fonts-list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `sa_branch`
--
ALTER TABLE `sa_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sa_channel_catagory`
--
ALTER TABLE `sa_channel_catagory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sa_city`
--
ALTER TABLE `sa_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sa_serviceprovider`
--
ALTER TABLE `sa_serviceprovider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fonts-list`
--
ALTER TABLE `fonts-list`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sa_branch`
--
ALTER TABLE `sa_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sa_channel_catagory`
--
ALTER TABLE `sa_channel_catagory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sa_city`
--
ALTER TABLE `sa_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sa_serviceprovider`
--
ALTER TABLE `sa_serviceprovider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=396;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
