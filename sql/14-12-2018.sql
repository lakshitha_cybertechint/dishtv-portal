-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.13 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dishtv-portal.activations
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.activations: ~4 rows (approximately)
DELETE FROM `activations`;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 'ojiLiaMKi2NpL8chlTyJBcoOoaCR2ij9', 1, '2018-11-02 21:37:29', '2018-11-02 21:37:29', '2018-11-02 21:37:29'),
	(2, 2, '5m7TnAxhEntAAvOBvLPWuLccXAjB0k2w', 1, '2018-11-02 21:37:30', '2018-11-02 21:37:30', '2018-11-02 21:37:30'),
	(3, 3, '9xOkxfEJWH5I7289Fe7vLx97IDY3WJPD', 1, '2018-11-26 21:16:15', '2018-11-26 21:16:15', '2018-11-26 21:16:15'),
	(4, 7, 'Lj67dxZJxfrOvjHV80P15nOnN84jjxRL', 1, '2018-11-26 21:30:43', '2018-11-26 21:30:43', '2018-11-26 21:30:43');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.fonts-list
CREATE TABLE IF NOT EXISTS `fonts-list` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `unicode` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=utf8;

-- Dumping data for table dishtv-portal.fonts-list: ~593 rows (approximately)
DELETE FROM `fonts-list`;
/*!40000 ALTER TABLE `fonts-list` DISABLE KEYS */;
INSERT INTO `fonts-list` (`id`, `type`, `icon`, `unicode`) VALUES
	(1, 'fa', 'fa-adjust', '&#xf042;'),
	(2, 'fa', 'fa-adn', '&#xf170;'),
	(3, 'fa', 'fa-align-center', '&#xf037;'),
	(4, 'fa', 'fa-align-justify', '&#xf039;'),
	(5, 'fa', 'fa-align-left', '&#xf036;'),
	(6, 'fa', 'fa-align-right', '&#xf038;'),
	(7, 'fa', 'fa-ambulance', '&#xf0f9;'),
	(8, 'fa', 'fa-anchor', '&#xf13d;'),
	(9, 'fa', 'fa-android', '&#xf17b;'),
	(10, 'fa', 'fa-angellist', '&#xf209;'),
	(11, 'fa', 'fa-angle-double-down', '&#xf103;'),
	(12, 'fa', 'fa-angle-double-left', '&#xf100;'),
	(13, 'fa', 'fa-angle-double-right', '&#xf101;'),
	(14, 'fa', 'fa-angle-double-up', '&#xf102;'),
	(15, 'fa', 'fa-angle-down', '&#xf107;'),
	(16, 'fa', 'fa-angle-left', '&#xf104;'),
	(17, 'fa', 'fa-angle-right', '&#xf105;'),
	(18, 'fa', 'fa-angle-up', '&#xf106;'),
	(19, 'fa', 'fa-apple', '&#xf179;'),
	(20, 'fa', 'fa-archive', '&#xf187;'),
	(21, 'fa', 'fa-area-chart', '&#xf1fe;'),
	(22, 'fa', 'fa-arrow-circle-down', '&#xf0ab;'),
	(23, 'fa', 'fa-arrow-circle-left', '&#xf0a8;'),
	(24, 'fa', 'fa-arrow-circle-o-down', '&#xf01a;'),
	(25, 'fa', 'fa-arrow-circle-o-left', '&#xf190;'),
	(26, 'fa', 'fa-arrow-circle-o-right', '&#xf18e;'),
	(27, 'fa', 'fa-arrow-circle-o-up', '&#xf01b;'),
	(28, 'fa', 'fa-arrow-circle-right', '&#xf0a9;'),
	(29, 'fa', 'fa-arrow-circle-up', '&#xf0aa;'),
	(30, 'fa', 'fa-arrow-down', '&#xf063;'),
	(31, 'fa', 'fa-arrow-left', '&#xf060;'),
	(32, 'fa', 'fa-arrow-right', '&#xf061;'),
	(33, 'fa', 'fa-arrow-up', '&#xf062;'),
	(34, 'fa', 'fa-arrows', '&#xf047;'),
	(35, 'fa', 'fa-arrows-alt', '&#xf0b2;'),
	(36, 'fa', 'fa-arrows-h', '&#xf07e;'),
	(37, 'fa', 'fa-arrows-v', '&#xf07d;'),
	(38, 'fa', 'fa-asterisk', '&#xf069;'),
	(39, 'fa', 'fa-at', '&#xf1fa;'),
	(40, 'fa', 'fa-automobile(alias)', '&#xf1b9;'),
	(41, 'fa', 'fa-backward', '&#xf04a;'),
	(42, 'fa', 'fa-ban', '&#xf05e;'),
	(43, 'fa', 'fa-bank(alias)', '&#xf19c;'),
	(44, 'fa', 'fa-bar-chart', '&#xf080;'),
	(45, 'fa', 'fa-bar-chart-o(alias)', '&#xf080;'),
	(46, 'fa', 'fa-barcode', '&#xf02a;'),
	(47, 'fa', 'fa-bars', '&#xf0c9;'),
	(48, 'fa', 'fa-bed', '&#xf236;'),
	(49, 'fa', 'fa-beer', '&#xf0fc;'),
	(50, 'fa', 'fa-behance', '&#xf1b4;'),
	(51, 'fa', 'fa-behance-square', '&#xf1b5;'),
	(52, 'fa', 'fa-bell', '&#xf0f3;'),
	(53, 'fa', 'fa-bell-o', '&#xf0a2;'),
	(54, 'fa', 'fa-bell-slash', '&#xf1f6;'),
	(55, 'fa', 'fa-bell-slash-o', '&#xf1f7;'),
	(56, 'fa', 'fa-bicycle', '&#xf206;'),
	(57, 'fa', 'fa-binoculars', '&#xf1e5;'),
	(58, 'fa', 'fa-birthday-cake', '&#xf1fd;'),
	(59, 'fa', 'fa-bitbucket', '&#xf171;'),
	(60, 'fa', 'fa-bitbucket-square', '&#xf172;'),
	(61, 'fa', 'fa-bitcoin(alias)', '&#xf15a;'),
	(62, 'fa', 'fa-bold', '&#xf032;'),
	(63, 'fa', 'fa-bolt', '&#xf0e7;'),
	(64, 'fa', 'fa-bomb', '&#xf1e2;'),
	(65, 'fa', 'fa-book', '&#xf02d;'),
	(66, 'fa', 'fa-bookmark', '&#xf02e;'),
	(67, 'fa', 'fa-bookmark-o', '&#xf097;'),
	(68, 'fa', 'fa-briefcase', '&#xf0b1;'),
	(69, 'fa', 'fa-btc', '&#xf15a;'),
	(70, 'fa', 'fa-bug', '&#xf188;'),
	(71, 'fa', 'fa-building', '&#xf1ad;'),
	(72, 'fa', 'fa-building-o', '&#xf0f7;'),
	(73, 'fa', 'fa-bullhorn', '&#xf0a1;'),
	(74, 'fa', 'fa-bullseye', '&#xf140;'),
	(75, 'fa', 'fa-bus', '&#xf207;'),
	(76, 'fa', 'fa-buysellads', NULL),
	(77, 'fa', 'fa-cab(alias)', NULL),
	(78, 'fa', 'fa-calculator', NULL),
	(79, 'fa', 'fa-calendar', NULL),
	(80, 'fa', 'fa-calendar-o', NULL),
	(81, 'fa', 'fa-camera', NULL),
	(82, 'fa', 'fa-camera-retro', NULL),
	(83, 'fa', 'fa-car', NULL),
	(84, 'fa', 'fa-caret-down', NULL),
	(85, 'fa', 'fa-caret-left', NULL),
	(86, 'fa', 'fa-caret-right', NULL),
	(87, 'fa', 'fa-caret-square-o-down', NULL),
	(88, 'fa', 'fa-caret-square-o-left', NULL),
	(89, 'fa', 'fa-caret-square-o-right', NULL),
	(90, 'fa', 'fa-caret-square-o-up', NULL),
	(91, 'fa', 'fa-caret-up', NULL),
	(92, 'fa', 'fa-cart-arrow-down', NULL),
	(93, 'fa', 'fa-cart-plus', NULL),
	(94, 'fa', 'fa-cc', NULL),
	(95, 'fa', 'fa-cc-amex', NULL),
	(96, 'fa', 'fa-cc-discover', NULL),
	(97, 'fa', 'fa-cc-mastercard', NULL),
	(98, 'fa', 'fa-cc-paypal', NULL),
	(99, 'fa', 'fa-cc-stripe', NULL),
	(100, 'fa', 'fa-cc-visa', NULL),
	(101, 'fa', 'fa-certificate', NULL),
	(102, 'fa', 'fa-chain(alias)', NULL),
	(103, 'fa', 'fa-chain-broken', NULL),
	(104, 'fa', 'fa-check', NULL),
	(105, 'fa', 'fa-check-circle', NULL),
	(106, 'fa', 'fa-check-circle-o', NULL),
	(107, 'fa', 'fa-check-square', NULL),
	(108, 'fa', 'fa-check-square-o', NULL),
	(109, 'fa', 'fa-chevron-circle-down', NULL),
	(110, 'fa', 'fa-chevron-circle-left', NULL),
	(111, 'fa', 'fa-chevron-circle-right', NULL),
	(112, 'fa', 'fa-chevron-circle-up', NULL),
	(113, 'fa', 'fa-chevron-down', NULL),
	(114, 'fa', 'fa-chevron-left', NULL),
	(115, 'fa', 'fa-chevron-right', NULL),
	(116, 'fa', 'fa-chevron-up', NULL),
	(117, 'fa', 'fa-child', NULL),
	(118, 'fa', 'fa-circle', NULL),
	(119, 'fa', 'fa-circle-o', NULL),
	(120, 'fa', 'fa-circle-o-notch', NULL),
	(121, 'fa', 'fa-circle-thin', NULL),
	(122, 'fa', 'fa-clipboard', NULL),
	(123, 'fa', 'fa-clock-o', NULL),
	(124, 'fa', 'fa-close(alias)', NULL),
	(125, 'fa', 'fa-cloud', NULL),
	(126, 'fa', 'fa-cloud-download', NULL),
	(127, 'fa', 'fa-cloud-upload', NULL),
	(128, 'fa', 'fa-cny(alias)', NULL),
	(129, 'fa', 'fa-code', NULL),
	(130, 'fa', 'fa-code-fork', NULL),
	(131, 'fa', 'fa-codepen', NULL),
	(132, 'fa', 'fa-coffee', NULL),
	(133, 'fa', 'fa-cog', NULL),
	(134, 'fa', 'fa-cogs', NULL),
	(135, 'fa', 'fa-columns', NULL),
	(136, 'fa', 'fa-comment', NULL),
	(137, 'fa', 'fa-comment-o', NULL),
	(138, 'fa', 'fa-comments', NULL),
	(139, 'fa', 'fa-comments-o', NULL),
	(140, 'fa', 'fa-compass', NULL),
	(141, 'fa', 'fa-compress', NULL),
	(142, 'fa', 'fa-connectdevelop', NULL),
	(143, 'fa', 'fa-copy(alias)', NULL),
	(144, 'fa', 'fa-copyright', NULL),
	(145, 'fa', 'fa-credit-card', NULL),
	(146, 'fa', 'fa-crop', NULL),
	(147, 'fa', 'fa-crosshairs', NULL),
	(148, 'fa', 'fa-css3', NULL),
	(149, 'fa', 'fa-cube', NULL),
	(150, 'fa', 'fa-cubes', NULL),
	(151, 'fa', 'fa-cut(alias)', NULL),
	(152, 'fa', 'fa-cutlery', NULL),
	(153, 'fa', 'fa-dashboard(alias)', NULL),
	(154, 'fa', 'fa-dashcube', NULL),
	(155, 'fa', 'fa-database', NULL),
	(156, 'fa', 'fa-dedent(alias)', NULL),
	(157, 'fa', 'fa-delicious', NULL),
	(158, 'fa', 'fa-desktop', NULL),
	(159, 'fa', 'fa-deviantart', NULL),
	(160, 'fa', 'fa-diamond', NULL),
	(161, 'fa', 'fa-digg', NULL),
	(162, 'fa', 'fa-dollar(alias)', NULL),
	(163, 'fa', 'fa-dot-circle-o', NULL),
	(164, 'fa', 'fa-download', NULL),
	(165, 'fa', 'fa-dribbble', NULL),
	(166, 'fa', 'fa-dropbox', NULL),
	(167, 'fa', 'fa-drupal', NULL),
	(168, 'fa', 'fa-edit(alias)', NULL),
	(169, 'fa', 'fa-eject', NULL),
	(170, 'fa', 'fa-ellipsis-h', NULL),
	(171, 'fa', 'fa-ellipsis-v', NULL),
	(172, 'fa', 'fa-empire', NULL),
	(173, 'fa', 'fa-envelope', NULL),
	(174, 'fa', 'fa-envelope-o', NULL),
	(175, 'fa', 'fa-envelope-square', NULL),
	(176, 'fa', 'fa-eraser', NULL),
	(177, 'fa', 'fa-eur', NULL),
	(178, 'fa', 'fa-euro(alias)', NULL),
	(179, 'fa', 'fa-exchange', NULL),
	(180, 'fa', 'fa-exclamation', NULL),
	(181, 'fa', 'fa-exclamation-circle', NULL),
	(182, 'fa', 'fa-exclamation-triangle', NULL),
	(183, 'fa', 'fa-expand', NULL),
	(184, 'fa', 'fa-external-link', NULL),
	(185, 'fa', 'fa-external-link-square', NULL),
	(186, 'fa', 'fa-eye', NULL),
	(187, 'fa', 'fa-eye-slash', NULL),
	(188, 'fa', 'fa-eyedropper', NULL),
	(189, 'fa', 'fa-facebook', NULL),
	(190, 'fa', 'fa-facebook-f(alias)', NULL),
	(191, 'fa', 'fa-facebook-official', NULL),
	(192, 'fa', 'fa-facebook-square', NULL),
	(193, 'fa', 'fa-fast-backward', NULL),
	(194, 'fa', 'fa-fast-forward', NULL),
	(195, 'fa', 'fa-fax', NULL),
	(196, 'fa', 'fa-female', NULL),
	(197, 'fa', 'fa-fighter-jet', NULL),
	(198, 'fa', 'fa-file', NULL),
	(199, 'fa', 'fa-file-archive-o', NULL),
	(200, 'fa', 'fa-file-audio-o', NULL),
	(201, 'fa', 'fa-file-code-o', NULL),
	(202, 'fa', 'fa-file-excel-o', NULL),
	(203, 'fa', 'fa-file-image-o', NULL),
	(204, 'fa', 'fa-file-movie-o(alias)', NULL),
	(205, 'fa', 'fa-file-o', NULL),
	(206, 'fa', 'fa-file-pdf-o', NULL),
	(207, 'fa', 'fa-file-photo-o(alias)', NULL),
	(208, 'fa', 'fa-file-picture-o(alias)', NULL),
	(209, 'fa', 'fa-file-powerpoint-o', NULL),
	(210, 'fa', 'fa-file-sound-o(alias)', NULL),
	(211, 'fa', 'fa-file-text', NULL),
	(212, 'fa', 'fa-file-text-o', NULL),
	(213, 'fa', 'fa-file-video-o', NULL),
	(214, 'fa', 'fa-file-word-o', NULL),
	(215, 'fa', 'fa-file-zip-o(alias)', NULL),
	(216, 'fa', 'fa-files-o', NULL),
	(217, 'fa', 'fa-film', NULL),
	(218, 'fa', 'fa-filter', NULL),
	(219, 'fa', 'fa-fire', NULL),
	(220, 'fa', 'fa-fire-extinguisher', NULL),
	(221, 'fa', 'fa-flag', NULL),
	(222, 'fa', 'fa-flag-checkered', NULL),
	(223, 'fa', 'fa-flag-o', NULL),
	(224, 'fa', 'fa-flash(alias)', NULL),
	(225, 'fa', 'fa-flask', NULL),
	(226, 'fa', 'fa-flickr', NULL),
	(227, 'fa', 'fa-floppy-o', NULL),
	(228, 'fa', 'fa-folder', NULL),
	(229, 'fa', 'fa-folder-o', NULL),
	(230, 'fa', 'fa-folder-open', NULL),
	(231, 'fa', 'fa-folder-open-o', NULL),
	(232, 'fa', 'fa-font', NULL),
	(233, 'fa', 'fa-forumbee', NULL),
	(234, 'fa', 'fa-forward', NULL),
	(235, 'fa', 'fa-foursquare', NULL),
	(236, 'fa', 'fa-frown-o', NULL),
	(237, 'fa', 'fa-futbol-o', NULL),
	(238, 'fa', 'fa-gamepad', NULL),
	(239, 'fa', 'fa-gavel', NULL),
	(240, 'fa', 'fa-gbp', NULL),
	(241, 'fa', 'fa-ge(alias)', NULL),
	(242, 'fa', 'fa-gear(alias)', NULL),
	(243, 'fa', 'fa-gears(alias)', NULL),
	(244, 'fa', 'fa-genderless(alias)', NULL),
	(245, 'fa', 'fa-gift', NULL),
	(246, 'fa', 'fa-git', NULL),
	(247, 'fa', 'fa-git-square', NULL),
	(248, 'fa', 'fa-github', NULL),
	(249, 'fa', 'fa-github-alt', NULL),
	(250, 'fa', 'fa-github-square', NULL),
	(251, 'fa', 'fa-gittip(alias)', NULL),
	(252, 'fa', 'fa-glass', NULL),
	(253, 'fa', 'fa-globe', NULL),
	(254, 'fa', 'fa-google', NULL),
	(255, 'fa', 'fa-google-plus', NULL),
	(256, 'fa', 'fa-google-plus-square', NULL),
	(257, 'fa', 'fa-google-wallet', NULL),
	(258, 'fa', 'fa-graduation-cap', NULL),
	(259, 'fa', 'fa-gratipay', NULL),
	(260, 'fa', 'fa-group(alias)', NULL),
	(261, 'fa', 'fa-h-square', NULL),
	(262, 'fa', 'fa-hacker-news', NULL),
	(263, 'fa', 'fa-hand-o-down', NULL),
	(264, 'fa', 'fa-hand-o-left', NULL),
	(265, 'fa', 'fa-hand-o-right', NULL),
	(266, 'fa', 'fa-hand-o-up', NULL),
	(267, 'fa', 'fa-hdd-o', NULL),
	(268, 'fa', 'fa-header', NULL),
	(269, 'fa', 'fa-headphones', NULL),
	(270, 'fa', 'fa-heart', NULL),
	(271, 'fa', 'fa-heart-o', NULL),
	(272, 'fa', 'fa-heartbeat', NULL),
	(273, 'fa', 'fa-history', NULL),
	(274, 'fa', 'fa-home', NULL),
	(275, 'fa', 'fa-hospital-o', NULL),
	(276, 'fa', 'fa-hotel(alias)', NULL),
	(277, 'fa', 'fa-html5', NULL),
	(278, 'fa', 'fa-ils', NULL),
	(279, 'fa', 'fa-image(alias)', NULL),
	(280, 'fa', 'fa-inbox', NULL),
	(281, 'fa', 'fa-indent', NULL),
	(282, 'fa', 'fa-info', NULL),
	(283, 'fa', 'fa-info-circle', NULL),
	(284, 'fa', 'fa-inr', NULL),
	(285, 'fa', 'fa-instagram', NULL),
	(286, 'fa', 'fa-institution(alias)', NULL),
	(287, 'fa', 'fa-ioxhost', NULL),
	(288, 'fa', 'fa-italic', NULL),
	(289, 'fa', 'fa-joomla', NULL),
	(290, 'fa', 'fa-jpy', NULL),
	(291, 'fa', 'fa-jsfiddle', NULL),
	(292, 'fa', 'fa-key', NULL),
	(293, 'fa', 'fa-keyboard-o', NULL),
	(294, 'fa', 'fa-krw', NULL),
	(295, 'fa', 'fa-language', NULL),
	(296, 'fa', 'fa-laptop', NULL),
	(297, 'fa', 'fa-lastfm', NULL),
	(298, 'fa', 'fa-lastfm-square', NULL),
	(299, 'fa', 'fa-leaf', NULL),
	(300, 'fa', 'fa-leanpub', NULL),
	(301, 'fa', 'fa-legal(alias)', NULL),
	(302, 'fa', 'fa-lemon-o', NULL),
	(303, 'fa', 'fa-level-down', NULL),
	(304, 'fa', 'fa-level-up', NULL),
	(305, 'fa', 'fa-life-bouy(alias)', NULL),
	(306, 'fa', 'fa-life-buoy(alias)', NULL),
	(307, 'fa', 'fa-life-ring', NULL),
	(308, 'fa', 'fa-life-saver(alias)', NULL),
	(309, 'fa', 'fa-lightbulb-o', NULL),
	(310, 'fa', 'fa-line-chart', NULL),
	(311, 'fa', 'fa-link', NULL),
	(312, 'fa', 'fa-linkedin', NULL),
	(313, 'fa', 'fa-linkedin-square', NULL),
	(314, 'fa', 'fa-linux', NULL),
	(315, 'fa', 'fa-list', NULL),
	(316, 'fa', 'fa-list-alt', NULL),
	(317, 'fa', 'fa-list-ol', NULL),
	(318, 'fa', 'fa-list-ul', NULL),
	(319, 'fa', 'fa-location-arrow', NULL),
	(320, 'fa', 'fa-lock', NULL),
	(321, 'fa', 'fa-long-arrow-down', NULL),
	(322, 'fa', 'fa-long-arrow-left', NULL),
	(323, 'fa', 'fa-long-arrow-right', NULL),
	(324, 'fa', 'fa-long-arrow-up', NULL),
	(325, 'fa', 'fa-magic', NULL),
	(326, 'fa', 'fa-magnet', NULL),
	(327, 'fa', 'fa-mail-forward(alias)', NULL),
	(328, 'fa', 'fa-mail-reply(alias)', NULL),
	(329, 'fa', 'fa-mail-reply-all(alias)', NULL),
	(330, 'fa', 'fa-male', NULL),
	(331, 'fa', 'fa-map-marker', NULL),
	(332, 'fa', 'fa-mars', NULL),
	(333, 'fa', 'fa-mars-double', NULL),
	(334, 'fa', 'fa-mars-stroke', NULL),
	(335, 'fa', 'fa-mars-stroke-h', NULL),
	(336, 'fa', 'fa-mars-stroke-v', NULL),
	(337, 'fa', 'fa-maxcdn', NULL),
	(338, 'fa', 'fa-meanpath', NULL),
	(339, 'fa', 'fa-medium', NULL),
	(340, 'fa', 'fa-medkit', NULL),
	(341, 'fa', 'fa-meh-o', NULL),
	(342, 'fa', 'fa-mercury', NULL),
	(343, 'fa', 'fa-microphone', NULL),
	(344, 'fa', 'fa-microphone-slash', NULL),
	(345, 'fa', 'fa-minus', NULL),
	(346, 'fa', 'fa-minus-circle', NULL),
	(347, 'fa', 'fa-minus-square', NULL),
	(348, 'fa', 'fa-minus-square-o', NULL),
	(349, 'fa', 'fa-mobile', NULL),
	(350, 'fa', 'fa-mobile-phone(alias)', NULL),
	(351, 'fa', 'fa-money', NULL),
	(352, 'fa', 'fa-moon-o', NULL),
	(353, 'fa', 'fa-mortar-board(alias)', NULL),
	(354, 'fa', 'fa-motorcycle', NULL),
	(355, 'fa', 'fa-music', NULL),
	(356, 'fa', 'fa-navicon(alias)', NULL),
	(357, 'fa', 'fa-neuter', NULL),
	(358, 'fa', 'fa-newspaper-o', NULL),
	(359, 'fa', 'fa-openid', NULL),
	(360, 'fa', 'fa-outdent', NULL),
	(361, 'fa', 'fa-pagelines', NULL),
	(362, 'fa', 'fa-paint-brush', NULL),
	(363, 'fa', 'fa-paper-plane', NULL),
	(364, 'fa', 'fa-paper-plane-o', NULL),
	(365, 'fa', 'fa-paperclip', NULL),
	(366, 'fa', 'fa-paragraph', NULL),
	(367, 'fa', 'fa-paste(alias)', NULL),
	(368, 'fa', 'fa-pause', NULL),
	(369, 'fa', 'fa-paw', NULL),
	(370, 'fa', 'fa-paypal', NULL),
	(371, 'fa', 'fa-pencil', NULL),
	(372, 'fa', 'fa-pencil-square', NULL),
	(373, 'fa', 'fa-pencil-square-o', NULL),
	(374, 'fa', 'fa-phone', NULL),
	(375, 'fa', 'fa-phone-square', NULL),
	(376, 'fa', 'fa-photo(alias)', NULL),
	(377, 'fa', 'fa-picture-o', NULL),
	(378, 'fa', 'fa-pie-chart', NULL),
	(379, 'fa', 'fa-pied-piper', NULL),
	(380, 'fa', 'fa-pied-piper-alt', NULL),
	(381, 'fa', 'fa-pinterest', NULL),
	(382, 'fa', 'fa-pinterest-p', NULL),
	(383, 'fa', 'fa-pinterest-square', NULL),
	(384, 'fa', 'fa-plane', NULL),
	(385, 'fa', 'fa-play', NULL),
	(386, 'fa', 'fa-play-circle', NULL),
	(387, 'fa', 'fa-play-circle-o', NULL),
	(388, 'fa', 'fa-plug', NULL),
	(389, 'fa', 'fa-plus', NULL),
	(390, 'fa', 'fa-plus-circle', NULL),
	(391, 'fa', 'fa-plus-square', NULL),
	(392, 'fa', 'fa-plus-square-o', NULL),
	(393, 'fa', 'fa-power-off', NULL),
	(394, 'fa', 'fa-print', NULL),
	(395, 'fa', 'fa-puzzle-piece', NULL),
	(396, 'fa', 'fa-qq', NULL),
	(397, 'fa', 'fa-qrcode', NULL),
	(398, 'fa', 'fa-question', NULL),
	(399, 'fa', 'fa-question-circle', NULL),
	(400, 'fa', 'fa-quote-left', NULL),
	(401, 'fa', 'fa-quote-right', NULL),
	(402, 'fa', 'fa-ra(alias)', NULL),
	(403, 'fa', 'fa-random', NULL),
	(404, 'fa', 'fa-rebel', NULL),
	(405, 'fa', 'fa-recycle', NULL),
	(406, 'fa', 'fa-reddit', NULL),
	(407, 'fa', 'fa-reddit-square', NULL),
	(408, 'fa', 'fa-refresh', NULL),
	(409, 'fa', 'fa-remove(alias)', NULL),
	(410, 'fa', 'fa-renren', NULL),
	(411, 'fa', 'fa-reorder(alias)', NULL),
	(412, 'fa', 'fa-repeat', NULL),
	(413, 'fa', 'fa-reply', NULL),
	(414, 'fa', 'fa-reply-all', NULL),
	(415, 'fa', 'fa-retweet', NULL),
	(416, 'fa', 'fa-rmb(alias)', NULL),
	(417, 'fa', 'fa-road', NULL),
	(418, 'fa', 'fa-rocket', NULL),
	(419, 'fa', 'fa-rotate-left(alias)', NULL),
	(420, 'fa', 'fa-rotate-right(alias)', NULL),
	(421, 'fa', 'fa-rouble(alias)', NULL),
	(422, 'fa', 'fa-rss', NULL),
	(423, 'fa', 'fa-rss-square', NULL),
	(424, 'fa', 'fa-rub', NULL),
	(425, 'fa', 'fa-ruble(alias)', NULL),
	(426, 'fa', 'fa-rupee(alias)', NULL),
	(427, 'fa', 'fa-save(alias)', NULL),
	(428, 'fa', 'fa-scissors', NULL),
	(429, 'fa', 'fa-search', NULL),
	(430, 'fa', 'fa-search-minus', NULL),
	(431, 'fa', 'fa-search-plus', NULL),
	(432, 'fa', 'fa-sellsy', NULL),
	(433, 'fa', 'fa-send(alias)', NULL),
	(434, 'fa', 'fa-send-o(alias)', NULL),
	(435, 'fa', 'fa-server', NULL),
	(436, 'fa', 'fa-share', NULL),
	(437, 'fa', 'fa-share-alt', NULL),
	(438, 'fa', 'fa-share-alt-square', NULL),
	(439, 'fa', 'fa-share-square', NULL),
	(440, 'fa', 'fa-share-square-o', NULL),
	(441, 'fa', 'fa-shekel(alias)', NULL),
	(442, 'fa', 'fa-sheqel(alias)', NULL),
	(443, 'fa', 'fa-shield', NULL),
	(444, 'fa', 'fa-ship', NULL),
	(445, 'fa', 'fa-shirtsinbulk', NULL),
	(446, 'fa', 'fa-shopping-cart', NULL),
	(447, 'fa', 'fa-sign-in', NULL),
	(448, 'fa', 'fa-sign-out', NULL),
	(449, 'fa', 'fa-signal', NULL),
	(450, 'fa', 'fa-simplybuilt', NULL),
	(451, 'fa', 'fa-sitemap', NULL),
	(452, 'fa', 'fa-skyatlas', NULL),
	(453, 'fa', 'fa-skype', NULL),
	(454, 'fa', 'fa-slack', NULL),
	(455, 'fa', 'fa-sliders', NULL),
	(456, 'fa', 'fa-slideshare', NULL),
	(457, 'fa', 'fa-smile-o', NULL),
	(458, 'fa', 'fa-soccer-ball-o(alias)', NULL),
	(459, 'fa', 'fa-sort', NULL),
	(460, 'fa', 'fa-sort-alpha-asc', NULL),
	(461, 'fa', 'fa-sort-alpha-desc', NULL),
	(462, 'fa', 'fa-sort-amount-asc', NULL),
	(463, 'fa', 'fa-sort-amount-desc', NULL),
	(464, 'fa', 'fa-sort-asc', NULL),
	(465, 'fa', 'fa-sort-desc', NULL),
	(466, 'fa', 'fa-sort-down(alias)', NULL),
	(467, 'fa', 'fa-sort-numeric-asc', NULL),
	(468, 'fa', 'fa-sort-numeric-desc', NULL),
	(469, 'fa', 'fa-sort-up(alias)', NULL),
	(470, 'fa', 'fa-soundcloud', NULL),
	(471, 'fa', 'fa-space-shuttle', NULL),
	(472, 'fa', 'fa-spinner', NULL),
	(473, 'fa', 'fa-spoon', NULL),
	(474, 'fa', 'fa-spotify', NULL),
	(475, 'fa', 'fa-square', NULL),
	(476, 'fa', 'fa-square-o', NULL),
	(477, 'fa', 'fa-stack-exchange', NULL),
	(478, 'fa', 'fa-stack-overflow', NULL),
	(479, 'fa', 'fa-star', NULL),
	(480, 'fa', 'fa-star-half', NULL),
	(481, 'fa', 'fa-star-half-empty(alias)', NULL),
	(482, 'fa', 'fa-star-half-full(alias)', NULL),
	(483, 'fa', 'fa-star-half-o', NULL),
	(484, 'fa', 'fa-star-o', NULL),
	(485, 'fa', 'fa-steam', NULL),
	(486, 'fa', 'fa-steam-square', NULL),
	(487, 'fa', 'fa-step-backward', NULL),
	(488, 'fa', 'fa-step-forward', NULL),
	(489, 'fa', 'fa-stethoscope', NULL),
	(490, 'fa', 'fa-stop', NULL),
	(491, 'fa', 'fa-street-view', NULL),
	(492, 'fa', 'fa-strikethrough', NULL),
	(493, 'fa', 'fa-stumbleupon', NULL),
	(494, 'fa', 'fa-stumbleupon-circle', NULL),
	(495, 'fa', 'fa-subscript', NULL),
	(496, 'fa', 'fa-subway', NULL),
	(497, 'fa', 'fa-suitcase', NULL),
	(498, 'fa', 'fa-sun-o', NULL),
	(499, 'fa', 'fa-superscript', NULL),
	(500, 'fa', 'fa-support(alias)', NULL),
	(501, 'fa', 'fa-table', NULL),
	(502, 'fa', 'fa-tablet', NULL),
	(503, 'fa', 'fa-tachometer', NULL),
	(504, 'fa', 'fa-tag', NULL),
	(505, 'fa', 'fa-tags', NULL),
	(506, 'fa', 'fa-tasks', NULL),
	(507, 'fa', 'fa-taxi', NULL),
	(508, 'fa', 'fa-tencent-weibo', NULL),
	(509, 'fa', 'fa-terminal', NULL),
	(510, 'fa', 'fa-text-height', NULL),
	(511, 'fa', 'fa-text-width', NULL),
	(512, 'fa', 'fa-th', NULL),
	(513, 'fa', 'fa-th-large', NULL),
	(514, 'fa', 'fa-th-list', NULL),
	(515, 'fa', 'fa-thumb-tack', NULL),
	(516, 'fa', 'fa-thumbs-down', NULL),
	(517, 'fa', 'fa-thumbs-o-down', NULL),
	(518, 'fa', 'fa-thumbs-o-up', NULL),
	(519, 'fa', 'fa-thumbs-up', NULL),
	(520, 'fa', 'fa-ticket', NULL),
	(521, 'fa', 'fa-times', NULL),
	(522, 'fa', 'fa-times-circle', NULL),
	(523, 'fa', 'fa-times-circle-o', NULL),
	(524, 'fa', 'fa-tint', NULL),
	(525, 'fa', 'fa-toggle-down(alias)', NULL),
	(526, 'fa', 'fa-toggle-left(alias)', NULL),
	(527, 'fa', 'fa-toggle-off', NULL),
	(528, 'fa', 'fa-toggle-on', NULL),
	(529, 'fa', 'fa-toggle-right(alias)', NULL),
	(530, 'fa', 'fa-toggle-up(alias)', NULL),
	(531, 'fa', 'fa-train', NULL),
	(532, 'fa', 'fa-transgender', NULL),
	(533, 'fa', 'fa-transgender-alt', NULL),
	(534, 'fa', 'fa-trash', NULL),
	(535, 'fa', 'fa-trash-o', NULL),
	(536, 'fa', 'fa-tree', NULL),
	(537, 'fa', 'fa-trello', NULL),
	(538, 'fa', 'fa-trophy', NULL),
	(539, 'fa', 'fa-truck', NULL),
	(540, 'fa', 'fa-try', NULL),
	(541, 'fa', 'fa-tty', NULL),
	(542, 'fa', 'fa-tumblr', NULL),
	(543, 'fa', 'fa-tumblr-square', NULL),
	(544, 'fa', 'fa-turkish-lira(alias)', NULL),
	(545, 'fa', 'fa-twitch', NULL),
	(546, 'fa', 'fa-twitter', NULL),
	(547, 'fa', 'fa-twitter-square', NULL),
	(548, 'fa', 'fa-umbrella', NULL),
	(549, 'fa', 'fa-underline', NULL),
	(550, 'fa', 'fa-undo', NULL),
	(551, 'fa', 'fa-university', NULL),
	(552, 'fa', 'fa-unlink(alias)', NULL),
	(553, 'fa', 'fa-unlock', NULL),
	(554, 'fa', 'fa-unlock-alt', NULL),
	(555, 'fa', 'fa-unsorted(alias)', NULL),
	(556, 'fa', 'fa-upload', NULL),
	(557, 'fa', 'fa-usd', NULL),
	(558, 'fa', 'fa-user', NULL),
	(559, 'fa', 'fa-user-md', NULL),
	(560, 'fa', 'fa-user-plus', NULL),
	(561, 'fa', 'fa-user-secret', NULL),
	(562, 'fa', 'fa-user-times', NULL),
	(563, 'fa', 'fa-users', NULL),
	(564, 'fa', 'fa-venus', NULL),
	(565, 'fa', 'fa-venus-double', NULL),
	(566, 'fa', 'fa-venus-mars', NULL),
	(567, 'fa', 'fa-viacoin', NULL),
	(568, 'fa', 'fa-video-camera', NULL),
	(569, 'fa', 'fa-vimeo-square', NULL),
	(570, 'fa', 'fa-vine', NULL),
	(571, 'fa', 'fa-vk', NULL),
	(572, 'fa', 'fa-volume-down', NULL),
	(573, 'fa', 'fa-volume-off', NULL),
	(574, 'fa', 'fa-volume-up', NULL),
	(575, 'fa', 'fa-warning(alias)', NULL),
	(576, 'fa', 'fa-wechat(alias)', NULL),
	(577, 'fa', 'fa-weibo', NULL),
	(578, 'fa', 'fa-weixin', NULL),
	(579, 'fa', 'fa-whatsapp', NULL),
	(580, 'fa', 'fa-wheelchair', NULL),
	(581, 'fa', 'fa-wifi', NULL),
	(582, 'fa', 'fa-windows', NULL),
	(583, 'fa', 'fa-won(alias)', NULL),
	(584, 'fa', 'fa-wordpress', NULL),
	(585, 'fa', 'fa-wrench', NULL),
	(586, 'fa', 'fa-xing', NULL),
	(587, 'fa', 'fa-xing-square', NULL),
	(588, 'fa', 'fa-yahoo', NULL),
	(589, 'fa', 'fa-yelp', NULL),
	(590, 'fa', 'fa-yen(alias)', NULL),
	(591, 'fa', 'fa-youtube', NULL),
	(592, 'fa', 'fa-youtube-play', NULL),
	(593, 'fa', 'fa-youtube-square', NULL);
/*!40000 ALTER TABLE `fonts-list` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.groups: ~0 rows (approximately)
DELETE FROM `groups`;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `lft` int(11) NOT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.menu: ~15 rows (approximately)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `label`, `link`, `icon`, `parent`, `permissions`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Root Menu', '#', NULL, NULL, NULL, 1, 36, 0, 1, '2016-09-21 00:00:00', '2018-11-26 21:46:13'),
	(2, 'MENU MANAGEMENT', 'menu/list', NULL, 1, '["menu.add","admin"]', 2, 3, 1, 1, '2016-09-21 00:00:00', '2017-12-24 16:39:01'),
	(4, 'USER MANAGEMENT', '#', NULL, 1, '["user.list","user.add","user.edit","user.delete","admin"]', 6, 15, 1, 1, '2016-09-21 10:56:25', '2018-11-26 22:07:20'),
	(5, 'PERMISSION', 'permission/list', NULL, 4, '["admin"]', 7, 8, 2, 1, '2016-09-21 10:56:51', '2017-12-24 16:39:01'),
	(6, 'ROLE', 'user/role/list', NULL, 4, '["admin"]', 9, 10, 2, 1, '2016-09-21 10:57:15', '2017-12-24 16:39:01'),
	(7, 'USER', 'user/list', NULL, 4, '["user.list","user.add","user.edit","user.delete","admin"]', 11, 12, 2, 1, '2016-09-21 10:57:51', '2017-12-24 16:39:01'),
	(8, 'DEALER MANAGEMENT', 'dealer/list', NULL, 1, '["dealer.add","dealer.edit","dealer.list","dealer.status","dealer.delete","admin"]', 16, 17, 1, 1, '2018-11-02 20:54:04', '2018-11-26 22:07:20'),
	(9, 'SERVICE PROVIDER MANAGEMENT', 'serviceProvider/list', NULL, 1, '["serviceProvider.add","serviceProvider.edit","serviceProvider.list","serviceProvider.status","serviceProvider.delete","admin"]', 18, 19, 1, 1, '2018-11-02 20:54:37', '2018-11-26 22:07:20'),
	(10, 'Customer Management', 'customer/list', NULL, 1, '["customer.add","customer.edit","customer.delete","customer.list","admin"]', 28, 29, 1, 1, '2018-11-02 21:44:57', '2018-11-26 22:07:20'),
	(11, 'CHANNEL MANAGEMET', 'channel/list', NULL, 1, '["channel.category.add","channel.category.edit","channel.category.list","channel.category.status","channel.category.delete","channel.lang.add","channel.lang.edit","channel.lang.list","channel.add","channel.edit","channel.list","channel.status","channel.delete","admin"]', 20, 27, 1, 1, '2018-11-19 12:34:37', '2018-11-26 22:07:20'),
	(12, 'CATEGORY', 'channel/category/list', NULL, 11, '["channel.category.add","channel.category.edit","channel.category.list","channel.category.status","channel.category.delete","admin"]', 23, 24, 2, 1, '2018-11-19 12:35:27', '2018-11-26 22:07:20'),
	(13, 'CHANNEL', 'channel/list', NULL, 11, '["channel.add","channel.edit","channel.list","channel.status","channel.delete","admin"]', 21, 22, 2, 1, '2018-11-19 12:37:54', '2018-11-26 22:07:20'),
	(14, 'PACKAGE MANAGEMENT ', 'package/list', NULL, 1, '["package.add","package.edit","package.list","package.status","package.delete","admin"]', 32, 33, 1, 1, '2018-11-19 14:56:53', '2018-11-26 22:07:20'),
	(15, 'MEMBER MANGEMENT', 'member/list', NULL, 1, '["member.add","member.edit","member.list","member.status","member.delete","admin"]', 34, 35, 1, 1, '2018-11-19 15:06:34', '2018-11-26 22:07:21'),
	(16, 'LANGUAGES', 'channel/lang/list', NULL, 11, '["channel.lang.add","channel.lang.edit","channel.lang.list","channel.lang.status","channel.lang.delete","admin"]', 25, 26, 2, 1, '2018-11-26 21:46:13', '2018-11-26 22:11:59');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.migrations: ~13 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2018_11_04_100613_create_service_provider_table', 1),
	('2018_11_04_150302_create_channel_catagory_table', 1),
	('2018_11_04_150447_create_channel_table', 2),
	('2018_11_06_211712_create_dealer_table', 2),
	('2018_11_10_001333_create_channel_service_providers_table', 3),
	('2018_11_10_161744_create_packages_table', 3),
	('2018_11_10_162014_create_package_channels_table', 3),
	('2018_11_16_001844_create_members_table', 3),
	('2018_11_16_003836_create_customer_packages_table', 3),
	('2018_11_16_101231_create_customer_addone_channels_table', 3),
	('2018_11_16_115256_create_customers_table', 3),
	('2018_11_17_093054_create_customer_base_packages_table', 3),
	('2018_11_19_152403_create_channel_lang_table', 4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.permissions: ~65 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `description`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
	(1, 'user', 'Normal Registered User', 1, 1, '2015-07-25 00:00:00', '2015-12-02 04:54:39'),
	(2, 'menu.add', NULL, 1, 1, '2015-07-25 00:00:00', '2015-12-03 08:02:41'),
	(3, 'menu.list', NULL, 1, 1, '2015-07-25 00:00:00', '2015-12-02 04:54:54'),
	(4, 'menu.edit', NULL, 1, 1, '2015-07-25 00:00:00', '2015-12-02 04:54:57'),
	(5, 'menu.status', NULL, 1, 1, '2015-07-25 00:00:00', '2015-12-02 04:55:01'),
	(6, 'admin', 'Super Admin Permission', 1, 1, '2015-07-25 00:00:00', '2015-07-25 00:00:00'),
	(7, 'index', 'Home Page Permission', 1, 1, '2015-07-25 00:00:00', '2015-12-02 04:55:03'),
	(8, 'menu.delete', NULL, 1, 1, '2015-09-06 14:30:06', '2015-09-06 14:30:09'),
	(9, 'user.add', NULL, 1, 1, '2015-10-16 00:00:00', '2015-10-16 00:00:00'),
	(10, 'user.edit', NULL, 1, 1, '2015-10-16 00:00:00', '2015-10-16 00:00:00'),
	(11, 'user.delete', NULL, 1, 1, '2015-10-16 00:00:00', '2015-10-16 00:00:00'),
	(12, 'user.list', NULL, 1, 1, '2015-10-20 00:00:00', '2015-10-20 20:01:57'),
	(13, 'user.role.add', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(14, 'user.role.edit', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(15, 'user.role.list', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(16, 'user.role.delete', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(17, 'permission.add', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(18, 'permission.edit', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(19, 'permission.delete', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(20, 'permission.list', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(21, 'permission.group.add', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(22, 'permission.group.edit', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(23, 'permission.group.list', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(24, 'permission.group.delete', NULL, 1, 1, '2015-10-22 00:00:00', '2015-10-22 00:00:00'),
	(25, 'user.status', NULL, 1, 1, '2015-12-19 00:00:47', '2015-12-19 00:00:47'),
	(27, 'dashboard', 'DASHBOARD', 1, 1, '2017-01-23 18:48:39', '2017-01-23 18:48:39'),
	(53, 'customer.add', 'New Customer', 1, 1, '2018-11-02 21:42:55', '2018-11-02 21:42:55'),
	(54, 'customer.edit', 'Update Customer', 1, 1, '2018-11-02 21:43:19', '2018-11-02 21:43:19'),
	(55, 'customer.delete', 'Delete Customer', 1, 1, '2018-11-02 21:43:42', '2018-11-02 21:43:42'),
	(56, 'customer.list', 'Customer List', 1, 1, '2018-11-02 21:43:57', '2018-11-02 21:43:57'),
	(57, 'channel.category.add', 'channel.category.add', 1, 1, '2018-11-26 21:19:56', '2018-11-26 21:19:56'),
	(58, 'channel.category.edit', 'channel.category.edit', 1, 1, '2018-11-26 21:20:10', '2018-11-26 21:20:10'),
	(59, 'channel.category.list', 'channel.category.list', 1, 1, '2018-11-26 21:20:25', '2018-11-26 21:20:25'),
	(60, 'channel.category.status', 'channel.category.status', 1, 1, '2018-11-26 21:21:07', '2018-11-26 21:21:07'),
	(61, 'channel.category.delete', 'channel.category.delete', 1, 1, '2018-11-26 21:21:21', '2018-11-26 21:21:21'),
	(62, 'channel.lang.add', 'channel.lang.add', 1, 1, '2018-11-26 21:22:12', '2018-11-26 21:22:12'),
	(63, 'channel.lang.edit', 'channel.lang.edit', 1, 1, '2018-11-26 21:22:26', '2018-11-26 21:22:26'),
	(64, 'channel.lang.list', 'channel.lang.list', 1, 1, '2018-11-26 21:22:38', '2018-11-26 21:22:38'),
	(65, 'channel.lang.status', 'channel.lang.status', 1, 1, '2018-11-26 21:22:55', '2018-11-26 21:22:55'),
	(66, 'channel.lang.delete', 'channel.lang.delete', 1, 1, '2018-11-26 21:23:08', '2018-11-26 21:23:08'),
	(67, 'channel.add', 'channel.add', 1, 1, '2018-11-26 21:23:23', '2018-11-26 21:23:23'),
	(68, 'channel.edit', 'channel.edit', 1, 1, '2018-11-26 21:23:33', '2018-11-26 21:23:33'),
	(69, 'channel.list', 'channel.list', 1, 1, '2018-11-26 21:23:43', '2018-11-26 21:23:43'),
	(70, 'channel.status', 'channel.status', 1, 1, '2018-11-26 21:24:00', '2018-11-26 21:24:00'),
	(71, 'channel.delete', 'channel.delete', 1, 1, '2018-11-26 21:24:18', '2018-11-26 21:24:18'),
	(72, 'serviceProvider.add', 'serviceProvider.add', 1, 1, '2018-11-26 21:47:09', '2018-11-26 21:47:09'),
	(73, 'serviceProvider.edit', 'serviceProvider.edit', 1, 1, '2018-11-26 21:47:24', '2018-11-26 21:47:24'),
	(74, 'serviceProvider.list', 'serviceProvider.list', 1, 1, '2018-11-26 21:47:34', '2018-11-26 21:47:34'),
	(75, 'serviceProvider.status', 'serviceProvider.status', 1, 1, '2018-11-26 21:47:53', '2018-11-26 21:47:53'),
	(76, 'serviceProvider.delete', 'serviceProvider.delete', 1, 1, '2018-11-26 21:48:05', '2018-11-26 21:48:05'),
	(77, 'package.add', 'package.add', 1, 1, '2018-11-26 21:50:41', '2018-11-26 21:50:41'),
	(78, 'package.edit', 'package.edit', 1, 1, '2018-11-26 21:51:00', '2018-11-26 21:51:00'),
	(79, 'package.list', 'package.list', 1, 1, '2018-11-26 21:51:14', '2018-11-26 21:51:14'),
	(80, 'package.status', 'package.status', 1, 1, '2018-11-26 21:51:38', '2018-11-26 21:51:38'),
	(81, 'package.delete', 'package.delete', 1, 1, '2018-11-26 21:51:51', '2018-11-26 21:51:51'),
	(82, 'dealer.add', 'dealer.add', 1, 1, '2018-11-26 21:58:59', '2018-11-26 21:58:59'),
	(83, 'dealer.edit', 'dealer.edit', 1, 1, '2018-11-26 21:59:10', '2018-11-26 21:59:10'),
	(84, 'dealer.list', 'dealer.list', 1, 1, '2018-11-26 21:59:23', '2018-11-26 21:59:23'),
	(85, 'dealer.status', 'dealer.status', 1, 1, '2018-11-26 21:59:53', '2018-11-26 21:59:53'),
	(86, 'dealer.delete', 'dealer.delete', 1, 1, '2018-11-26 22:00:04', '2018-11-26 22:00:04'),
	(87, 'member.add', 'member.add', 1, 1, '2018-11-26 22:03:50', '2018-11-26 22:03:50'),
	(88, 'member.edit', 'member.edit', 1, 1, '2018-11-26 22:04:02', '2018-11-26 22:04:02'),
	(89, 'member.list', 'member.list', 1, 1, '2018-11-26 22:04:14', '2018-11-26 22:04:14'),
	(90, 'member.status', 'member.status', 1, 1, '2018-11-26 22:04:30', '2018-11-26 22:04:30'),
	(91, 'member.delete', 'member.delete', 1, 1, '2018-11-26 22:04:47', '2018-11-26 22:04:47');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.persistences
CREATE TABLE IF NOT EXISTS `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.persistences: ~10 rows (approximately)
DELETE FROM `persistences`;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
	(7, 1, 'aVYJcuD4yC1ib02pQ6rS5ow81K5u8h4K', '2018-11-02 21:47:57', '2018-11-02 21:47:57'),
	(8, 1, '76IvDUQ99WAPmoJZG32r8b0PYGM924em', '2018-11-03 18:24:22', '2018-11-03 18:24:22'),
	(9, 1, 'GkRFajLbXFgNq5CiNZNSzZRs5kH0hsID', '2018-11-04 20:10:31', '2018-11-04 20:10:31'),
	(10, 1, 'YWtIDWHUzyAyj72JNbJGoXiDT2QqWE04', '2018-11-05 11:23:26', '2018-11-05 11:23:26'),
	(12, 1, 'qBsrWKYZT7rV97byoY2LuomGqlBdxAbj', '2018-11-08 20:04:14', '2018-11-08 20:04:14'),
	(13, 1, 'rmMvHUqGYlfPLu2LMiUOK3WoymboJFvG', '2018-11-19 12:22:09', '2018-11-19 12:22:09'),
	(15, 2, 'A8mpsDZxa8q9BM5D9PM4D7bWPpNHqMw3', '2018-11-21 13:26:17', '2018-11-21 13:26:17'),
	(16, 1, 'BZNAihTXfjHygWPijqwCltA0gqlZYQRg', '2018-11-26 21:11:43', '2018-11-26 21:11:43'),
	(18, 1, 'UN5rzNbEBZ5KQ6HqGxxGDkrjtnxjgu38', '2018-11-26 21:32:34', '2018-11-26 21:32:34'),
	(20, 3, 'qxiWXPhPGkVEF9As1DQfBRzb5UOqmaoA', '2018-11-26 22:10:48', '2018-11-26 22:10:48'),
	(21, 1, 'YaamY1MtA8wMkUx906GiqsqVKfwIbCOA', '2018-12-04 21:08:50', '2018-12-04 21:08:50'),
	(26, 7, 'tDNPphRQFYbPUIi36tFwoDPQSWo1p1aM', '2018-12-14 10:09:35', '2018-12-14 10:09:35');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.reminders
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.reminders: ~0 rows (approximately)
DELETE FROM `reminders`;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_by` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.roles: ~3 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_by`, `created_at`, `updated_at`) VALUES
	(1, 'developer', 'Developer', '{"admin":true}', 1, '2018-11-02 21:24:30', '2018-11-02 21:24:32'),
	(2, 'dealer', 'Dealer', '{"index":true,"dashboard":true,"customer.add":true,"customer.edit":true,"customer.delete":true,"customer.list":true,"channel.category.list":true,"channel.lang.list":true,"channel.list":true,"package.add":true,"package.edit":true,"package.list":true,"package.status":true,"package.delete":true,"member.add":true,"member.edit":true,"member.list":true,"member.status":true,"member.delete":true}', 1, '2018-11-02 21:25:14', '2018-11-26 22:10:09'),
	(3, 'admin', 'Admin', '{"dashboard":true,"customer.add":true,"customer.edit":true,"customer.delete":true,"customer.list":true,"channel.category.add":true,"channel.category.edit":true,"channel.category.list":true,"channel.category.status":true,"channel.category.delete":true,"channel.lang.add":true,"channel.lang.edit":true,"channel.lang.list":true,"channel.lang.status":true,"channel.lang.delete":true,"channel.add":true,"channel.edit":true,"channel.list":true,"channel.status":true,"channel.delete":true,"serviceProvider.add":true,"serviceProvider.edit":true,"serviceProvider.list":true,"serviceProvider.status":true,"serviceProvider.delete":true,"package.add":true,"package.edit":true,"package.list":true,"package.status":true,"package.delete":true,"dealer.add":true,"dealer.edit":true,"dealer.list":true,"dealer.status":true,"dealer.delete":true,"member.add":true,"member.edit":true,"member.list":true,"member.status":true,"member.delete":true}', 1, '2018-11-26 21:25:27', '2018-11-26 22:05:19');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.role_users
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.role_users: ~4 rows (approximately)
DELETE FROM `role_users`;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2018-11-02 21:37:30', '2018-11-02 21:37:30'),
	(2, 2, '2018-11-02 21:37:31', '2018-11-02 21:37:31'),
	(3, 2, '2018-11-26 21:18:17', '2018-11-26 21:18:17'),
	(7, 3, '2018-11-26 21:30:43', '2018-11-26 21:30:43');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_branch
CREATE TABLE IF NOT EXISTS `sa_branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table dishtv-portal.sa_branch: ~2 rows (approximately)
DELETE FROM `sa_branch`;
/*!40000 ALTER TABLE `sa_branch` DISABLE KEYS */;
INSERT INTO `sa_branch` (`id`, `name`, `code`, `address`, `tel`, `city_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'HEADOFFICE', '-', 'NEGAMBO', '756388155', 1, 1, '2016-09-29 16:31:30', '2016-10-13 09:48:48', NULL),
	(2, 'KANDY', 'K-001', 'Kandy', '7995', 2, 1, '2017-01-18 19:03:04', '2017-01-18 19:03:04', NULL);
/*!40000 ALTER TABLE `sa_branch` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_channel
CREATE TABLE IF NOT EXISTS `sa_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `logo_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `serviceProvider` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_channel: ~0 rows (approximately)
DELETE FROM `sa_channel`;
/*!40000 ALTER TABLE `sa_channel` DISABLE KEYS */;
INSERT INTO `sa_channel` (`id`, `name`, `code`, `price`, `description`, `logo_name`, `category`, `serviceProvider`, `language`, `created_at`, `updated_at`) VALUES
	(1, 'SCOOBY', 'SD-001', '258', 'cartoon', 'channel-logo-20181119123947.png', 'CARTOON NETWORK', '', 'Tamil', '2018-11-19 12:39:47', '2018-11-21 13:21:41');
/*!40000 ALTER TABLE `sa_channel` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_channel_catagory
CREATE TABLE IF NOT EXISTS `sa_channel_catagory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_channel_catagory: ~0 rows (approximately)
DELETE FROM `sa_channel_catagory`;
/*!40000 ALTER TABLE `sa_channel_catagory` DISABLE KEYS */;
INSERT INTO `sa_channel_catagory` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'CARTOON NETWORK', 'CARTOON NETWORK', '2018-11-19 12:36:26', '2018-11-19 12:36:54');
/*!40000 ALTER TABLE `sa_channel_catagory` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_channel_lang
CREATE TABLE IF NOT EXISTS `sa_channel_lang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_channel_lang: ~2 rows (approximately)
DELETE FROM `sa_channel_lang`;
/*!40000 ALTER TABLE `sa_channel_lang` DISABLE KEYS */;
INSERT INTO `sa_channel_lang` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Tamil', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'Hindi', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sa_channel_lang` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_channel_service_providers
CREATE TABLE IF NOT EXISTS `sa_channel_service_providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `service_providers_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_channel_service_providers: ~2 rows (approximately)
DELETE FROM `sa_channel_service_providers`;
/*!40000 ALTER TABLE `sa_channel_service_providers` DISABLE KEYS */;
INSERT INTO `sa_channel_service_providers` (`id`, `channel_id`, `service_providers_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(6, 1, 4, NULL, '2018-11-21 13:21:40', '2018-11-21 13:21:40'),
	(7, 1, 10, NULL, '2018-11-21 13:21:40', '2018-11-21 13:21:40');
/*!40000 ALTER TABLE `sa_channel_service_providers` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_city
CREATE TABLE IF NOT EXISTS `sa_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table dishtv-portal.sa_city: ~2 rows (approximately)
DELETE FROM `sa_city`;
/*!40000 ALTER TABLE `sa_city` DISABLE KEYS */;
INSERT INTO `sa_city` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Galle', '2018-03-28 01:07:19', '2018-03-28 01:07:19'),
	(2, 'Colombo', '2018-03-28 01:07:19', '2018-03-28 01:07:19');
/*!40000 ALTER TABLE `sa_city` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_customers
CREATE TABLE IF NOT EXISTS `sa_customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dateOfRecharge` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dateOfExpire` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dealer` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_customers: ~0 rows (approximately)
DELETE FROM `sa_customers`;
/*!40000 ALTER TABLE `sa_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `sa_customers` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_customer_addone_channels
CREATE TABLE IF NOT EXISTS `sa_customer_addone_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_customer_addone_channels: ~0 rows (approximately)
DELETE FROM `sa_customer_addone_channels`;
/*!40000 ALTER TABLE `sa_customer_addone_channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `sa_customer_addone_channels` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_customer_base_packages
CREATE TABLE IF NOT EXISTS `sa_customer_base_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_customer_base_packages: ~0 rows (approximately)
DELETE FROM `sa_customer_base_packages`;
/*!40000 ALTER TABLE `sa_customer_base_packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `sa_customer_base_packages` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_customer_packages
CREATE TABLE IF NOT EXISTS `sa_customer_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `regName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vcNumber` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `basePackINR` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `basePackLKR` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `periodOfMonth` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment` tinyint(1) NOT NULL,
  `totalINR` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `totalLKR` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_customer_packages: ~0 rows (approximately)
DELETE FROM `sa_customer_packages`;
/*!40000 ALTER TABLE `sa_customer_packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `sa_customer_packages` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_dealers
CREATE TABLE IF NOT EXISTS `sa_dealers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tel` bigint(20) NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `logo_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `regNo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_dealers: ~0 rows (approximately)
DELETE FROM `sa_dealers`;
/*!40000 ALTER TABLE `sa_dealers` DISABLE KEYS */;
INSERT INTO `sa_dealers` (`id`, `name`, `email`, `tel`, `address`, `logo_name`, `regNo`, `user`, `created_at`, `updated_at`) VALUES
	(1, 'AFRO GLOBAL9', 'afro@gmail.com2', 773799390, 'sbdvsdkjvl', 'dealer-business-logo-20181119123038.png', 'RG-5582', '2', '2018-11-19 12:29:53', '2018-11-19 12:30:38');
/*!40000 ALTER TABLE `sa_dealers` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_members
CREATE TABLE IF NOT EXISTS `sa_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tel` bigint(20) NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `logo_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `regNo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_members: ~0 rows (approximately)
DELETE FROM `sa_members`;
/*!40000 ALTER TABLE `sa_members` DISABLE KEYS */;
INSERT INTO `sa_members` (`id`, `name`, `email`, `tel`, `address`, `logo_name`, `regNo`, `user`, `created_at`, `updated_at`) VALUES
	(1, 'Insaf', 'insaf.zak@gmail.com', 773799390, 'Galle', '', 'RG-558', '2', '2018-11-21 13:22:29', '2018-11-21 13:22:29');
/*!40000 ALTER TABLE `sa_members` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_packages
CREATE TABLE IF NOT EXISTS `sa_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `service_providers` int(11) NOT NULL,
  `lkr` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `inr` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_packages: ~0 rows (approximately)
DELETE FROM `sa_packages`;
/*!40000 ALTER TABLE `sa_packages` DISABLE KEYS */;
INSERT INTO `sa_packages` (`id`, `name`, `service_providers`, `lkr`, `inr`, `created_at`, `updated_at`) VALUES
	(1, 'PAKAGE 1', 10, '258', '108.36', '2018-11-19 15:25:21', '2018-11-19 15:25:21');
/*!40000 ALTER TABLE `sa_packages` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_package_channels
CREATE TABLE IF NOT EXISTS `sa_package_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_package_channels: ~0 rows (approximately)
DELETE FROM `sa_package_channels`;
/*!40000 ALTER TABLE `sa_package_channels` DISABLE KEYS */;
INSERT INTO `sa_package_channels` (`id`, `package_id`, `channel_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2018-11-19 15:25:22', '2018-11-19 15:25:22');
/*!40000 ALTER TABLE `sa_package_channels` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.sa_serviceprovider
CREATE TABLE IF NOT EXISTS `sa_serviceprovider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.sa_serviceprovider: ~10 rows (approximately)
DELETE FROM `sa_serviceprovider`;
/*!40000 ALTER TABLE `sa_serviceprovider` DISABLE KEYS */;
INSERT INTO `sa_serviceprovider` (`id`, `name`, `logo`, `created_at`, `updated_at`) VALUES
	(1, 'orwger', '', '2018-11-04 20:30:18', '2018-11-04 20:30:18'),
	(2, 'sdvsd', '', '2018-11-04 20:32:52', '2018-11-04 20:32:52'),
	(3, 'dssd', '', '2018-11-04 20:33:26', '2018-11-04 20:33:26'),
	(4, 'ABC', 'service-provider-logo-20181104204538.png', '2018-11-04 20:33:55', '2018-11-04 20:45:38'),
	(5, 'sd', '', '2018-11-04 20:34:57', '2018-11-04 20:34:57'),
	(6, 'BGSVCG', '', '2018-11-04 20:36:13', '2018-11-04 20:36:13'),
	(7, 'rfe', '', '2018-11-04 20:39:18', '2018-11-04 20:39:18'),
	(8, 'fdf', '', '2018-11-04 20:40:44', '2018-11-04 20:40:44'),
	(9, 'eifj', 'service-provider-logo-20181104204439.png', '2018-11-04 20:44:39', '2018-11-04 20:44:39'),
	(10, 'DISH TV', 'service-provider-logo-20181119123233.png', '2018-11-19 12:32:33', '2018-11-19 12:32:33');
/*!40000 ALTER TABLE `sa_serviceprovider` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.throttle
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=400 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.throttle: ~378 rows (approximately)
DELETE FROM `throttle`;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'global', NULL, '2015-07-11 11:35:27', '2015-07-11 11:35:27'),
	(2, NULL, 'ip', '127.0.0.1', '2015-07-11 11:35:28', '2015-07-11 11:35:28'),
	(3, NULL, 'global', NULL, '2015-07-11 11:36:28', '2015-07-11 11:36:28'),
	(4, NULL, 'ip', '127.0.0.1', '2015-07-11 11:36:28', '2015-07-11 11:36:28'),
	(5, NULL, 'global', NULL, '2015-07-13 10:14:23', '2015-07-13 10:14:23'),
	(6, NULL, 'ip', '127.0.0.1', '2015-07-13 10:14:23', '2015-07-13 10:14:23'),
	(7, 1, 'user', NULL, '2015-07-13 10:14:23', '2015-07-13 10:14:23'),
	(8, NULL, 'global', NULL, '2015-07-26 15:54:02', '2015-07-26 15:54:02'),
	(9, NULL, 'ip', '127.0.0.1', '2015-07-26 15:54:02', '2015-07-26 15:54:02'),
	(10, 1, 'user', NULL, '2015-07-26 15:54:02', '2015-07-26 15:54:02'),
	(11, NULL, 'global', NULL, '2015-08-13 15:18:09', '2015-08-13 15:18:09'),
	(12, NULL, 'ip', '127.0.0.1', '2015-08-13 15:18:09', '2015-08-13 15:18:09'),
	(13, 1, 'user', NULL, '2015-08-13 15:18:09', '2015-08-13 15:18:09'),
	(14, NULL, 'global', NULL, '2015-08-13 15:20:45', '2015-08-13 15:20:45'),
	(15, NULL, 'ip', '127.0.0.1', '2015-08-13 15:20:45', '2015-08-13 15:20:45'),
	(16, 1, 'user', NULL, '2015-08-13 15:20:45', '2015-08-13 15:20:45'),
	(17, NULL, 'global', NULL, '2015-08-18 09:01:17', '2015-08-18 09:01:17'),
	(18, NULL, 'ip', '127.0.0.1', '2015-08-18 09:01:17', '2015-08-18 09:01:17'),
	(19, 1, 'user', NULL, '2015-08-18 09:01:17', '2015-08-18 09:01:17'),
	(20, NULL, 'global', NULL, '2015-08-20 09:27:44', '2015-08-20 09:27:44'),
	(21, NULL, 'ip', '127.0.0.1', '2015-08-20 09:27:44', '2015-08-20 09:27:44'),
	(22, 1, 'user', NULL, '2015-08-20 09:27:45', '2015-08-20 09:27:45'),
	(23, NULL, 'global', NULL, '2015-08-20 10:22:53', '2015-08-20 10:22:53'),
	(24, NULL, 'ip', '127.0.0.1', '2015-08-20 10:22:53', '2015-08-20 10:22:53'),
	(25, 1, 'user', NULL, '2015-08-20 10:22:54', '2015-08-20 10:22:54'),
	(26, NULL, 'global', NULL, '2015-08-20 10:23:06', '2015-08-20 10:23:06'),
	(27, NULL, 'ip', '127.0.0.1', '2015-08-20 10:23:06', '2015-08-20 10:23:06'),
	(28, 1, 'user', NULL, '2015-08-20 10:23:06', '2015-08-20 10:23:06'),
	(29, NULL, 'global', NULL, '2015-08-20 10:23:09', '2015-08-20 10:23:09'),
	(30, NULL, 'ip', '127.0.0.1', '2015-08-20 10:23:09', '2015-08-20 10:23:09'),
	(31, 1, 'user', NULL, '2015-08-20 10:23:09', '2015-08-20 10:23:09'),
	(32, NULL, 'global', NULL, '2015-08-20 10:25:59', '2015-08-20 10:25:59'),
	(33, NULL, 'ip', '127.0.0.1', '2015-08-20 10:25:59', '2015-08-20 10:25:59'),
	(34, 1, 'user', NULL, '2015-08-20 10:25:59', '2015-08-20 10:25:59'),
	(35, NULL, 'global', NULL, '2015-08-20 10:26:18', '2015-08-20 10:26:18'),
	(36, NULL, 'ip', '127.0.0.1', '2015-08-20 10:26:19', '2015-08-20 10:26:19'),
	(37, 1, 'user', NULL, '2015-08-20 10:26:19', '2015-08-20 10:26:19'),
	(38, NULL, 'global', NULL, '2015-08-20 10:27:25', '2015-08-20 10:27:25'),
	(39, NULL, 'ip', '127.0.0.1', '2015-08-20 10:27:25', '2015-08-20 10:27:25'),
	(40, 1, 'user', NULL, '2015-08-20 10:27:25', '2015-08-20 10:27:25'),
	(41, NULL, 'global', NULL, '2015-08-24 20:36:12', '2015-08-24 20:36:12'),
	(42, NULL, 'ip', '127.0.0.1', '2015-08-24 20:36:12', '2015-08-24 20:36:12'),
	(43, NULL, 'global', NULL, '2015-08-24 20:38:25', '2015-08-24 20:38:25'),
	(44, NULL, 'ip', '127.0.0.1', '2015-08-24 20:38:25', '2015-08-24 20:38:25'),
	(45, NULL, 'global', NULL, '2015-08-24 20:39:09', '2015-08-24 20:39:09'),
	(46, NULL, 'ip', '127.0.0.1', '2015-08-24 20:39:09', '2015-08-24 20:39:09'),
	(47, NULL, 'global', NULL, '2015-08-24 20:39:44', '2015-08-24 20:39:44'),
	(48, NULL, 'ip', '127.0.0.1', '2015-08-24 20:39:44', '2015-08-24 20:39:44'),
	(49, NULL, 'global', NULL, '2015-08-24 20:39:49', '2015-08-24 20:39:49'),
	(50, NULL, 'ip', '127.0.0.1', '2015-08-24 20:39:50', '2015-08-24 20:39:50'),
	(51, NULL, 'global', NULL, '2015-08-24 20:41:29', '2015-08-24 20:41:29'),
	(52, NULL, 'ip', '127.0.0.1', '2015-08-24 20:41:29', '2015-08-24 20:41:29'),
	(53, NULL, 'global', NULL, '2015-08-25 12:56:45', '2015-08-25 12:56:45'),
	(54, NULL, 'ip', '127.0.0.1', '2015-08-25 12:56:45', '2015-08-25 12:56:45'),
	(55, NULL, 'global', NULL, '2015-08-26 13:18:20', '2015-08-26 13:18:20'),
	(56, NULL, 'ip', '192.168.1.35', '2015-08-26 13:18:21', '2015-08-26 13:18:21'),
	(57, NULL, 'global', NULL, '2015-08-26 13:18:23', '2015-08-26 13:18:23'),
	(58, NULL, 'ip', '192.168.1.35', '2015-08-26 13:18:23', '2015-08-26 13:18:23'),
	(59, NULL, 'global', NULL, '2015-08-26 13:18:27', '2015-08-26 13:18:27'),
	(60, NULL, 'ip', '192.168.1.35', '2015-08-26 13:18:27', '2015-08-26 13:18:27'),
	(61, NULL, 'global', NULL, '2015-08-26 13:18:31', '2015-08-26 13:18:31'),
	(62, NULL, 'ip', '192.168.1.35', '2015-08-26 13:18:31', '2015-08-26 13:18:31'),
	(63, NULL, 'global', NULL, '2015-08-26 13:18:36', '2015-08-26 13:18:36'),
	(64, NULL, 'ip', '192.168.1.35', '2015-08-26 13:18:36', '2015-08-26 13:18:36'),
	(65, NULL, 'global', NULL, '2015-08-26 13:18:48', '2015-08-26 13:18:48'),
	(66, NULL, 'global', NULL, '2015-08-27 08:50:50', '2015-08-27 08:50:50'),
	(67, NULL, 'ip', '127.0.0.1', '2015-08-27 08:50:50', '2015-08-27 08:50:50'),
	(68, 1, 'user', NULL, '2015-08-27 08:50:50', '2015-08-27 08:50:50'),
	(69, NULL, 'global', NULL, '2015-08-30 12:12:57', '2015-08-30 12:12:57'),
	(70, NULL, 'ip', '127.0.0.1', '2015-08-30 12:12:57', '2015-08-30 12:12:57'),
	(71, NULL, 'global', NULL, '2015-08-30 12:21:13', '2015-08-30 12:21:13'),
	(72, NULL, 'ip', '127.0.0.1', '2015-08-30 12:21:14', '2015-08-30 12:21:14'),
	(73, NULL, 'global', NULL, '2015-09-06 16:33:36', '2015-09-06 16:33:36'),
	(74, NULL, 'ip', '127.0.0.1', '2015-09-06 16:33:36', '2015-09-06 16:33:36'),
	(75, NULL, 'global', NULL, '2015-09-18 11:15:18', '2015-09-18 11:15:18'),
	(76, NULL, 'ip', '192.168.1.15', '2015-09-18 11:15:18', '2015-09-18 11:15:18'),
	(77, NULL, 'global', NULL, '2015-09-18 11:15:22', '2015-09-18 11:15:22'),
	(78, NULL, 'ip', '192.168.1.15', '2015-09-18 11:15:22', '2015-09-18 11:15:22'),
	(79, NULL, 'global', NULL, '2015-09-18 11:15:30', '2015-09-18 11:15:30'),
	(80, NULL, 'ip', '192.168.1.15', '2015-09-18 11:15:30', '2015-09-18 11:15:30'),
	(81, NULL, 'global', NULL, '2015-09-18 11:15:34', '2015-09-18 11:15:34'),
	(82, NULL, 'ip', '192.168.1.15', '2015-09-18 11:15:34', '2015-09-18 11:15:34'),
	(83, NULL, 'global', NULL, '2015-09-18 11:15:40', '2015-09-18 11:15:40'),
	(84, NULL, 'ip', '192.168.1.15', '2015-09-18 11:15:40', '2015-09-18 11:15:40'),
	(85, NULL, 'global', NULL, '2015-10-29 19:07:26', '2015-10-29 19:07:26'),
	(86, NULL, 'ip', '127.0.0.1', '2015-10-29 19:07:26', '2015-10-29 19:07:26'),
	(87, 1, 'user', NULL, '2015-10-29 19:07:26', '2015-10-29 19:07:26'),
	(88, NULL, 'global', NULL, '2015-10-29 19:07:30', '2015-10-29 19:07:30'),
	(89, NULL, 'ip', '127.0.0.1', '2015-10-29 19:07:30', '2015-10-29 19:07:30'),
	(90, 1, 'user', NULL, '2015-10-29 19:07:30', '2015-10-29 19:07:30'),
	(91, NULL, 'global', NULL, '2015-10-29 19:07:34', '2015-10-29 19:07:34'),
	(92, NULL, 'ip', '127.0.0.1', '2015-10-29 19:07:34', '2015-10-29 19:07:34'),
	(93, 1, 'user', NULL, '2015-10-29 19:07:34', '2015-10-29 19:07:34'),
	(94, NULL, 'global', NULL, '2015-10-29 19:07:41', '2015-10-29 19:07:41'),
	(95, NULL, 'ip', '127.0.0.1', '2015-10-29 19:07:41', '2015-10-29 19:07:41'),
	(96, 1, 'user', NULL, '2015-10-29 19:07:41', '2015-10-29 19:07:41'),
	(97, NULL, 'global', NULL, '2015-10-29 19:07:48', '2015-10-29 19:07:48'),
	(98, NULL, 'ip', '127.0.0.1', '2015-10-29 19:07:48', '2015-10-29 19:07:48'),
	(99, 1, 'user', NULL, '2015-10-29 19:07:48', '2015-10-29 19:07:48'),
	(100, NULL, 'global', NULL, '2015-10-29 19:07:52', '2015-10-29 19:07:52'),
	(101, NULL, 'ip', '127.0.0.1', '2015-10-29 19:07:52', '2015-10-29 19:07:52'),
	(102, 1, 'user', NULL, '2015-10-29 19:07:52', '2015-10-29 19:07:52'),
	(103, NULL, 'global', NULL, '2015-11-04 08:43:02', '2015-11-04 08:43:02'),
	(104, NULL, 'ip', '127.0.0.1', '2015-11-04 08:43:02', '2015-11-04 08:43:02'),
	(105, 1, 'user', NULL, '2015-11-04 08:43:02', '2015-11-04 08:43:02'),
	(106, NULL, 'global', NULL, '2015-11-07 16:56:55', '2015-11-07 16:56:55'),
	(107, NULL, 'ip', '127.0.0.1', '2015-11-07 16:56:55', '2015-11-07 16:56:55'),
	(108, NULL, 'global', NULL, '2015-11-07 16:57:01', '2015-11-07 16:57:01'),
	(109, NULL, 'ip', '127.0.0.1', '2015-11-07 16:57:01', '2015-11-07 16:57:01'),
	(110, NULL, 'global', NULL, '2015-11-18 23:39:36', '2015-11-18 23:39:36'),
	(111, NULL, 'ip', '127.0.0.1', '2015-11-18 23:39:36', '2015-11-18 23:39:36'),
	(112, NULL, 'global', NULL, '2015-11-18 23:39:43', '2015-11-18 23:39:43'),
	(113, NULL, 'ip', '127.0.0.1', '2015-11-18 23:39:43', '2015-11-18 23:39:43'),
	(114, NULL, 'global', NULL, '2015-11-18 23:42:14', '2015-11-18 23:42:14'),
	(115, NULL, 'ip', '127.0.0.1', '2015-11-18 23:42:14', '2015-11-18 23:42:14'),
	(116, NULL, 'global', NULL, '2015-11-18 23:42:27', '2015-11-18 23:42:27'),
	(117, NULL, 'ip', '127.0.0.1', '2015-11-18 23:42:27', '2015-11-18 23:42:27'),
	(118, NULL, 'global', NULL, '2015-11-18 23:42:36', '2015-11-18 23:42:36'),
	(119, NULL, 'ip', '127.0.0.1', '2015-11-18 23:42:36', '2015-11-18 23:42:36'),
	(120, NULL, 'global', NULL, '2015-11-19 01:57:03', '2015-11-19 01:57:03'),
	(121, NULL, 'ip', '::1', '2015-11-19 01:57:03', '2015-11-19 01:57:03'),
	(122, NULL, 'global', NULL, '2015-11-19 01:57:06', '2015-11-19 01:57:06'),
	(123, NULL, 'ip', '::1', '2015-11-19 01:57:06', '2015-11-19 01:57:06'),
	(124, NULL, 'global', NULL, '2015-11-19 02:56:24', '2015-11-19 02:56:24'),
	(125, NULL, 'ip', '::1', '2015-11-19 02:56:24', '2015-11-19 02:56:24'),
	(126, 1, 'user', NULL, '2015-11-19 02:56:24', '2015-11-19 02:56:24'),
	(127, NULL, 'global', NULL, '2015-11-19 02:56:27', '2015-11-19 02:56:27'),
	(128, NULL, 'ip', '::1', '2015-11-19 02:56:27', '2015-11-19 02:56:27'),
	(129, 1, 'user', NULL, '2015-11-19 02:56:27', '2015-11-19 02:56:27'),
	(130, NULL, 'global', NULL, '2015-11-23 23:43:05', '2015-11-23 23:43:05'),
	(131, NULL, 'ip', '::1', '2015-11-23 23:43:05', '2015-11-23 23:43:05'),
	(132, 1, 'user', NULL, '2015-11-23 23:43:05', '2015-11-23 23:43:05'),
	(133, NULL, 'global', NULL, '2015-11-24 06:44:15', '2015-11-24 06:44:15'),
	(134, NULL, 'ip', '::1', '2015-11-24 06:44:15', '2015-11-24 06:44:15'),
	(135, NULL, 'global', NULL, '2015-12-03 06:42:12', '2015-12-03 06:42:12'),
	(136, NULL, 'ip', '::1', '2015-12-03 06:42:12', '2015-12-03 06:42:12'),
	(137, 1, 'user', NULL, '2015-12-03 06:42:12', '2015-12-03 06:42:12'),
	(138, NULL, 'global', NULL, '2015-12-03 08:02:03', '2015-12-03 08:02:03'),
	(139, NULL, 'ip', '::1', '2015-12-03 08:02:03', '2015-12-03 08:02:03'),
	(140, 1, 'user', NULL, '2015-12-03 08:02:03', '2015-12-03 08:02:03'),
	(141, NULL, 'global', NULL, '2015-12-03 23:12:59', '2015-12-03 23:12:59'),
	(142, NULL, 'ip', '127.0.0.1', '2015-12-03 23:12:59', '2015-12-03 23:12:59'),
	(143, 1, 'user', NULL, '2015-12-03 23:12:59', '2015-12-03 23:12:59'),
	(144, NULL, 'global', NULL, '2015-12-03 23:13:02', '2015-12-03 23:13:02'),
	(145, NULL, 'ip', '127.0.0.1', '2015-12-03 23:13:02', '2015-12-03 23:13:02'),
	(146, 1, 'user', NULL, '2015-12-03 23:13:02', '2015-12-03 23:13:02'),
	(147, NULL, 'global', NULL, '2015-12-19 03:58:50', '2015-12-19 03:58:50'),
	(148, NULL, 'ip', '127.0.0.1', '2015-12-19 03:58:50', '2015-12-19 03:58:50'),
	(149, 1, 'user', NULL, '2015-12-19 03:58:50', '2015-12-19 03:58:50'),
	(150, NULL, 'global', NULL, '2016-01-09 05:58:55', '2016-01-09 05:58:55'),
	(151, NULL, 'ip', '192.168.1.121', '2016-01-09 05:58:55', '2016-01-09 05:58:55'),
	(153, NULL, 'global', NULL, '2016-01-09 05:59:26', '2016-01-09 05:59:26'),
	(154, NULL, 'ip', '192.168.1.121', '2016-01-09 05:59:26', '2016-01-09 05:59:26'),
	(156, NULL, 'global', NULL, '2016-01-09 05:59:41', '2016-01-09 05:59:41'),
	(157, NULL, 'ip', '192.168.1.121', '2016-01-09 05:59:41', '2016-01-09 05:59:41'),
	(159, NULL, 'global', NULL, '2016-01-09 06:00:07', '2016-01-09 06:00:07'),
	(160, NULL, 'ip', '192.168.1.121', '2016-01-09 06:00:07', '2016-01-09 06:00:07'),
	(162, NULL, 'global', NULL, '2016-01-11 23:20:41', '2016-01-11 23:20:41'),
	(163, NULL, 'ip', '192.168.1.142', '2016-01-11 23:20:41', '2016-01-11 23:20:41'),
	(165, NULL, 'global', NULL, '2016-01-11 23:20:50', '2016-01-11 23:20:50'),
	(166, NULL, 'ip', '192.168.1.142', '2016-01-11 23:20:50', '2016-01-11 23:20:50'),
	(168, NULL, 'global', NULL, '2016-01-11 23:21:03', '2016-01-11 23:21:03'),
	(169, NULL, 'ip', '192.168.1.142', '2016-01-11 23:21:03', '2016-01-11 23:21:03'),
	(171, NULL, 'global', NULL, '2016-01-11 23:21:16', '2016-01-11 23:21:16'),
	(172, NULL, 'ip', '192.168.1.142', '2016-01-11 23:21:16', '2016-01-11 23:21:16'),
	(174, NULL, 'global', NULL, '2016-01-11 23:22:23', '2016-01-11 23:22:23'),
	(175, NULL, 'ip', '192.168.1.142', '2016-01-11 23:22:23', '2016-01-11 23:22:23'),
	(177, NULL, 'global', NULL, '2016-01-11 23:22:58', '2016-01-11 23:22:58'),
	(178, NULL, 'ip', '192.168.1.142', '2016-01-11 23:22:58', '2016-01-11 23:22:58'),
	(180, NULL, 'global', NULL, '2016-01-11 23:36:27', '2016-01-11 23:36:27'),
	(181, NULL, 'ip', '192.168.1.142', '2016-01-11 23:36:27', '2016-01-11 23:36:27'),
	(183, NULL, 'global', NULL, '2016-01-11 23:36:33', '2016-01-11 23:36:33'),
	(184, NULL, 'ip', '192.168.1.142', '2016-01-11 23:36:33', '2016-01-11 23:36:33'),
	(186, NULL, 'global', NULL, '2016-01-11 23:36:36', '2016-01-11 23:36:36'),
	(187, NULL, 'ip', '192.168.1.142', '2016-01-11 23:36:36', '2016-01-11 23:36:36'),
	(189, NULL, 'global', NULL, '2016-01-11 23:36:41', '2016-01-11 23:36:41'),
	(190, NULL, 'ip', '192.168.1.142', '2016-01-11 23:36:41', '2016-01-11 23:36:41'),
	(192, NULL, 'global', NULL, '2016-01-11 23:48:58', '2016-01-11 23:48:58'),
	(193, NULL, 'ip', '192.168.1.142', '2016-01-11 23:48:58', '2016-01-11 23:48:58'),
	(195, NULL, 'global', NULL, '2016-01-24 11:55:32', '2016-01-24 11:55:32'),
	(196, NULL, 'ip', '127.0.0.1', '2016-01-24 11:55:32', '2016-01-24 11:55:32'),
	(197, NULL, 'global', NULL, '2016-01-27 02:57:09', '2016-01-27 02:57:09'),
	(198, NULL, 'ip', '127.0.0.1', '2016-01-27 02:57:09', '2016-01-27 02:57:09'),
	(200, NULL, 'global', NULL, '2016-01-28 22:40:03', '2016-01-28 22:40:03'),
	(201, NULL, 'ip', '127.0.0.1', '2016-01-28 22:40:03', '2016-01-28 22:40:03'),
	(202, 1, 'user', NULL, '2016-01-28 22:40:03', '2016-01-28 22:40:03'),
	(203, NULL, 'global', NULL, '2016-02-10 22:59:12', '2016-02-10 22:59:12'),
	(204, NULL, 'ip', '127.0.0.1', '2016-02-10 22:59:12', '2016-02-10 22:59:12'),
	(205, 1, 'user', NULL, '2016-02-10 22:59:12', '2016-02-10 22:59:12'),
	(206, NULL, 'global', NULL, '2016-02-24 12:15:41', '2016-02-24 12:15:41'),
	(207, NULL, 'ip', '192.168.1.59', '2016-02-24 12:15:41', '2016-02-24 12:15:41'),
	(209, NULL, 'global', NULL, '2016-02-24 12:17:03', '2016-02-24 12:17:03'),
	(210, NULL, 'ip', '192.168.1.59', '2016-02-24 12:17:03', '2016-02-24 12:17:03'),
	(212, NULL, 'global', NULL, '2016-02-24 12:17:06', '2016-02-24 12:17:06'),
	(213, NULL, 'ip', '192.168.1.59', '2016-02-24 12:17:06', '2016-02-24 12:17:06'),
	(215, NULL, 'global', NULL, '2016-03-02 09:17:00', '2016-03-02 09:17:00'),
	(216, NULL, 'ip', '127.0.0.1', '2016-03-02 09:17:00', '2016-03-02 09:17:00'),
	(217, 1, 'user', NULL, '2016-03-02 09:17:00', '2016-03-02 09:17:00'),
	(218, NULL, 'global', NULL, '2016-03-28 15:02:49', '2016-03-28 15:02:49'),
	(219, NULL, 'ip', '127.0.0.1', '2016-03-28 15:02:49', '2016-03-28 15:02:49'),
	(220, NULL, 'global', NULL, '2016-03-28 15:02:56', '2016-03-28 15:02:56'),
	(221, NULL, 'ip', '127.0.0.1', '2016-03-28 15:02:56', '2016-03-28 15:02:56'),
	(222, 1, 'user', NULL, '2016-03-28 15:02:56', '2016-03-28 15:02:56'),
	(223, NULL, 'global', NULL, '2016-03-30 17:07:40', '2016-03-30 17:07:40'),
	(224, NULL, 'ip', '::1', '2016-03-30 17:07:41', '2016-03-30 17:07:41'),
	(225, NULL, 'global', NULL, '2016-05-02 09:31:37', '2016-05-02 09:31:37'),
	(226, NULL, 'ip', '192.168.1.41', '2016-05-02 09:31:37', '2016-05-02 09:31:37'),
	(227, NULL, 'global', NULL, '2016-05-02 09:31:40', '2016-05-02 09:31:40'),
	(228, NULL, 'ip', '192.168.1.41', '2016-05-02 09:31:40', '2016-05-02 09:31:40'),
	(229, NULL, 'global', NULL, '2016-05-02 09:31:43', '2016-05-02 09:31:43'),
	(230, NULL, 'ip', '192.168.1.41', '2016-05-02 09:31:43', '2016-05-02 09:31:43'),
	(231, NULL, 'global', NULL, '2016-05-02 09:31:49', '2016-05-02 09:31:49'),
	(232, NULL, 'ip', '192.168.1.41', '2016-05-02 09:31:49', '2016-05-02 09:31:49'),
	(233, NULL, 'global', NULL, '2016-05-02 09:56:12', '2016-05-02 09:56:12'),
	(234, NULL, 'ip', '192.168.1.41', '2016-05-02 09:56:12', '2016-05-02 09:56:12'),
	(235, NULL, 'global', NULL, '2016-05-02 09:56:40', '2016-05-02 09:56:40'),
	(236, NULL, 'ip', '192.168.1.41', '2016-05-02 09:56:40', '2016-05-02 09:56:40'),
	(237, NULL, 'global', NULL, '2016-05-02 09:56:58', '2016-05-02 09:56:58'),
	(238, NULL, 'ip', '192.168.1.41', '2016-05-02 09:56:58', '2016-05-02 09:56:58'),
	(239, NULL, 'global', NULL, '2016-05-02 09:57:01', '2016-05-02 09:57:01'),
	(240, NULL, 'ip', '192.168.1.41', '2016-05-02 09:57:01', '2016-05-02 09:57:01'),
	(241, NULL, 'global', NULL, '2016-05-02 09:57:07', '2016-05-02 09:57:07'),
	(242, NULL, 'ip', '192.168.1.41', '2016-05-02 09:57:07', '2016-05-02 09:57:07'),
	(243, NULL, 'global', NULL, '2017-01-16 03:43:01', '2017-01-16 03:43:01'),
	(244, NULL, 'ip', '::1', '2017-01-16 03:43:01', '2017-01-16 03:43:01'),
	(245, NULL, 'global', NULL, '2017-01-16 04:23:12', '2017-01-16 04:23:12'),
	(246, NULL, 'ip', '::1', '2017-01-16 04:23:12', '2017-01-16 04:23:12'),
	(248, NULL, 'global', NULL, '2017-01-16 04:23:40', '2017-01-16 04:23:40'),
	(249, NULL, 'ip', '::1', '2017-01-16 04:23:40', '2017-01-16 04:23:40'),
	(251, NULL, 'global', NULL, '2017-01-19 00:35:37', '2017-01-19 00:35:37'),
	(252, NULL, 'ip', '::1', '2017-01-19 00:35:37', '2017-01-19 00:35:37'),
	(253, NULL, 'global', NULL, '2017-01-25 11:59:52', '2017-01-25 11:59:52'),
	(254, NULL, 'ip', '::1', '2017-01-25 11:59:52', '2017-01-25 11:59:52'),
	(255, NULL, 'global', NULL, '2017-04-12 14:25:26', '2017-04-12 14:25:26'),
	(256, NULL, 'ip', '112.135.7.121', '2017-04-12 14:25:26', '2017-04-12 14:25:26'),
	(257, 9, 'user', NULL, '2017-04-12 14:25:26', '2017-04-12 14:25:26'),
	(258, NULL, 'global', NULL, '2017-04-24 11:47:15', '2017-04-24 11:47:15'),
	(259, NULL, 'ip', '::1', '2017-04-24 11:47:15', '2017-04-24 11:47:15'),
	(260, NULL, 'global', NULL, '2017-04-30 12:23:53', '2017-04-30 12:23:53'),
	(261, NULL, 'ip', '123.231.108.3', '2017-04-30 12:23:53', '2017-04-30 12:23:53'),
	(262, 9, 'user', NULL, '2017-04-30 12:23:53', '2017-04-30 12:23:53'),
	(263, NULL, 'global', NULL, '2017-04-30 12:23:58', '2017-04-30 12:23:58'),
	(264, NULL, 'ip', '123.231.108.3', '2017-04-30 12:23:58', '2017-04-30 12:23:58'),
	(265, 9, 'user', NULL, '2017-04-30 12:23:58', '2017-04-30 12:23:58'),
	(266, NULL, 'global', NULL, '2017-04-30 12:24:05', '2017-04-30 12:24:05'),
	(267, NULL, 'ip', '123.231.108.3', '2017-04-30 12:24:05', '2017-04-30 12:24:05'),
	(268, 9, 'user', NULL, '2017-04-30 12:24:05', '2017-04-30 12:24:05'),
	(269, NULL, 'global', NULL, '2017-04-30 12:24:14', '2017-04-30 12:24:14'),
	(270, NULL, 'ip', '123.231.108.3', '2017-04-30 12:24:14', '2017-04-30 12:24:14'),
	(271, 9, 'user', NULL, '2017-04-30 12:24:14', '2017-04-30 12:24:14'),
	(272, NULL, 'global', NULL, '2017-04-30 12:24:19', '2017-04-30 12:24:19'),
	(273, NULL, 'ip', '123.231.108.3', '2017-04-30 12:24:19', '2017-04-30 12:24:19'),
	(274, 9, 'user', NULL, '2017-04-30 12:24:19', '2017-04-30 12:24:19'),
	(275, NULL, 'global', NULL, '2017-04-30 12:24:24', '2017-04-30 12:24:24'),
	(276, NULL, 'ip', '123.231.108.3', '2017-04-30 12:24:24', '2017-04-30 12:24:24'),
	(277, 9, 'user', NULL, '2017-04-30 12:24:24', '2017-04-30 12:24:24'),
	(278, NULL, 'global', NULL, '2017-08-30 11:57:01', '2017-08-30 11:57:01'),
	(279, NULL, 'ip', '113.59.213.196', '2017-08-30 11:57:01', '2017-08-30 11:57:01'),
	(280, NULL, 'global', NULL, '2017-09-18 17:02:04', '2017-09-18 17:02:04'),
	(281, NULL, 'ip', '::1', '2017-09-18 17:02:04', '2017-09-18 17:02:04'),
	(282, NULL, 'global', NULL, '2017-09-26 11:16:17', '2017-09-26 11:16:17'),
	(283, NULL, 'ip', '::1', '2017-09-26 11:16:17', '2017-09-26 11:16:17'),
	(284, 9, 'user', NULL, '2017-09-26 11:16:17', '2017-09-26 11:16:17'),
	(285, NULL, 'global', NULL, '2017-10-17 22:22:42', '2017-10-17 22:22:42'),
	(286, NULL, 'ip', '::1', '2017-10-17 22:22:42', '2017-10-17 22:22:42'),
	(287, NULL, 'global', NULL, '2018-01-09 20:54:36', '2018-01-09 20:54:36'),
	(288, NULL, 'ip', '::1', '2018-01-09 20:54:36', '2018-01-09 20:54:36'),
	(289, NULL, 'global', NULL, '2018-02-01 10:24:38', '2018-02-01 10:24:38'),
	(290, NULL, 'ip', '::1', '2018-02-01 10:24:38', '2018-02-01 10:24:38'),
	(291, 12, 'user', NULL, '2018-02-01 10:24:38', '2018-02-01 10:24:38'),
	(292, NULL, 'global', NULL, '2018-02-01 10:24:46', '2018-02-01 10:24:46'),
	(293, NULL, 'ip', '::1', '2018-02-01 10:24:46', '2018-02-01 10:24:46'),
	(294, 12, 'user', NULL, '2018-02-01 10:24:46', '2018-02-01 10:24:46'),
	(295, NULL, 'global', NULL, '2018-02-01 10:25:05', '2018-02-01 10:25:05'),
	(296, NULL, 'ip', '::1', '2018-02-01 10:25:05', '2018-02-01 10:25:05'),
	(297, 12, 'user', NULL, '2018-02-01 10:25:05', '2018-02-01 10:25:05'),
	(298, NULL, 'global', NULL, '2018-02-01 10:26:17', '2018-02-01 10:26:17'),
	(299, NULL, 'ip', '::1', '2018-02-01 10:26:17', '2018-02-01 10:26:17'),
	(300, 12, 'user', NULL, '2018-02-01 10:26:17', '2018-02-01 10:26:17'),
	(301, NULL, 'global', NULL, '2018-02-07 20:53:34', '2018-02-07 20:53:34'),
	(302, NULL, 'ip', '::1', '2018-02-07 20:53:34', '2018-02-07 20:53:34'),
	(303, 18, 'user', NULL, '2018-02-07 20:53:34', '2018-02-07 20:53:34'),
	(304, NULL, 'global', NULL, '2018-02-07 20:58:30', '2018-02-07 20:58:30'),
	(305, NULL, 'ip', '::1', '2018-02-07 20:58:30', '2018-02-07 20:58:30'),
	(306, 18, 'user', NULL, '2018-02-07 20:58:30', '2018-02-07 20:58:30'),
	(307, NULL, 'global', NULL, '2018-02-12 10:18:18', '2018-02-12 10:18:18'),
	(308, NULL, 'ip', '::1', '2018-02-12 10:18:18', '2018-02-12 10:18:18'),
	(309, NULL, 'global', NULL, '2018-02-14 15:14:29', '2018-02-14 15:14:29'),
	(310, NULL, 'ip', '::1', '2018-02-14 15:14:29', '2018-02-14 15:14:29'),
	(311, NULL, 'global', NULL, '2018-02-14 15:14:38', '2018-02-14 15:14:38'),
	(312, NULL, 'ip', '::1', '2018-02-14 15:14:38', '2018-02-14 15:14:38'),
	(313, NULL, 'global', NULL, '2018-02-14 15:15:47', '2018-02-14 15:15:47'),
	(314, NULL, 'global', NULL, '2018-02-14 15:15:47', '2018-02-14 15:15:47'),
	(315, NULL, 'ip', '::1', '2018-02-14 15:15:47', '2018-02-14 15:15:47'),
	(316, NULL, 'ip', '::1', '2018-02-14 15:15:47', '2018-02-14 15:15:47'),
	(317, NULL, 'global', NULL, '2018-02-15 21:49:27', '2018-02-15 21:49:27'),
	(318, NULL, 'ip', '::1', '2018-02-15 21:49:27', '2018-02-15 21:49:27'),
	(319, NULL, 'global', NULL, '2018-02-18 10:44:10', '2018-02-18 10:44:10'),
	(320, NULL, 'ip', '::1', '2018-02-18 10:44:10', '2018-02-18 10:44:10'),
	(321, NULL, 'global', NULL, '2018-02-18 10:44:16', '2018-02-18 10:44:16'),
	(322, NULL, 'ip', '::1', '2018-02-18 10:44:16', '2018-02-18 10:44:16'),
	(323, NULL, 'global', NULL, '2018-02-18 10:44:21', '2018-02-18 10:44:21'),
	(324, NULL, 'ip', '::1', '2018-02-18 10:44:21', '2018-02-18 10:44:21'),
	(325, NULL, 'global', NULL, '2018-02-18 10:44:31', '2018-02-18 10:44:31'),
	(326, NULL, 'ip', '::1', '2018-02-18 10:44:31', '2018-02-18 10:44:31'),
	(327, NULL, 'global', NULL, '2018-02-18 12:08:25', '2018-02-18 12:08:25'),
	(328, NULL, 'ip', '::1', '2018-02-18 12:08:25', '2018-02-18 12:08:25'),
	(329, NULL, 'global', NULL, '2018-02-19 23:06:44', '2018-02-19 23:06:44'),
	(330, NULL, 'ip', '::1', '2018-02-19 23:06:44', '2018-02-19 23:06:44'),
	(331, NULL, 'global', NULL, '2018-02-25 23:11:01', '2018-02-25 23:11:01'),
	(332, NULL, 'ip', '::1', '2018-02-25 23:11:01', '2018-02-25 23:11:01'),
	(333, NULL, 'global', NULL, '2018-02-26 10:16:57', '2018-02-26 10:16:57'),
	(334, NULL, 'ip', '::1', '2018-02-26 10:16:57', '2018-02-26 10:16:57'),
	(335, NULL, 'global', NULL, '2018-02-26 10:17:14', '2018-02-26 10:17:14'),
	(336, NULL, 'ip', '::1', '2018-02-26 10:17:14', '2018-02-26 10:17:14'),
	(337, NULL, 'global', NULL, '2018-02-26 10:17:49', '2018-02-26 10:17:49'),
	(338, NULL, 'ip', '::1', '2018-02-26 10:17:49', '2018-02-26 10:17:49'),
	(339, NULL, 'global', NULL, '2018-02-26 10:18:00', '2018-02-26 10:18:00'),
	(340, NULL, 'ip', '::1', '2018-02-26 10:18:00', '2018-02-26 10:18:00'),
	(341, NULL, 'global', NULL, '2018-02-26 10:19:39', '2018-02-26 10:19:39'),
	(342, NULL, 'ip', '::1', '2018-02-26 10:19:39', '2018-02-26 10:19:39'),
	(343, NULL, 'global', NULL, '2018-02-26 10:23:08', '2018-02-26 10:23:08'),
	(344, NULL, 'ip', '::1', '2018-02-26 10:23:08', '2018-02-26 10:23:08'),
	(345, NULL, 'global', NULL, '2018-02-26 10:33:13', '2018-02-26 10:33:13'),
	(346, NULL, 'ip', '::1', '2018-02-26 10:33:13', '2018-02-26 10:33:13'),
	(347, NULL, 'global', NULL, '2018-02-26 10:34:52', '2018-02-26 10:34:52'),
	(348, NULL, 'ip', '::1', '2018-02-26 10:34:52', '2018-02-26 10:34:52'),
	(349, NULL, 'global', NULL, '2018-02-26 10:38:19', '2018-02-26 10:38:19'),
	(350, NULL, 'ip', '::1', '2018-02-26 10:38:19', '2018-02-26 10:38:19'),
	(351, NULL, 'global', NULL, '2018-02-26 10:39:22', '2018-02-26 10:39:22'),
	(352, NULL, 'ip', '::1', '2018-02-26 10:39:22', '2018-02-26 10:39:22'),
	(353, NULL, 'global', NULL, '2018-02-26 10:41:25', '2018-02-26 10:41:25'),
	(354, NULL, 'ip', '::1', '2018-02-26 10:41:25', '2018-02-26 10:41:25'),
	(355, NULL, 'global', NULL, '2018-02-26 10:42:45', '2018-02-26 10:42:45'),
	(356, NULL, 'ip', '::1', '2018-02-26 10:42:45', '2018-02-26 10:42:45'),
	(357, NULL, 'global', NULL, '2018-02-26 11:09:05', '2018-02-26 11:09:05'),
	(358, NULL, 'ip', '::1', '2018-02-26 11:09:05', '2018-02-26 11:09:05'),
	(359, 9, 'user', NULL, '2018-02-26 11:09:05', '2018-02-26 11:09:05'),
	(360, NULL, 'global', NULL, '2018-02-26 12:40:30', '2018-02-26 12:40:30'),
	(361, NULL, 'ip', '::1', '2018-02-26 12:40:30', '2018-02-26 12:40:30'),
	(362, NULL, 'global', NULL, '2018-02-26 19:53:51', '2018-02-26 19:53:51'),
	(363, NULL, 'ip', '::1', '2018-02-26 19:53:51', '2018-02-26 19:53:51'),
	(364, NULL, 'global', NULL, '2018-02-26 19:54:26', '2018-02-26 19:54:26'),
	(365, NULL, 'ip', '::1', '2018-02-26 19:54:26', '2018-02-26 19:54:26'),
	(366, NULL, 'global', NULL, '2018-02-26 22:36:07', '2018-02-26 22:36:07'),
	(367, NULL, 'ip', '::1', '2018-02-26 22:36:07', '2018-02-26 22:36:07'),
	(368, NULL, 'global', NULL, '2018-02-28 10:30:30', '2018-02-28 10:30:30'),
	(369, NULL, 'ip', '::1', '2018-02-28 10:30:30', '2018-02-28 10:30:30'),
	(370, NULL, 'global', NULL, '2018-02-28 23:39:39', '2018-02-28 23:39:39'),
	(371, NULL, 'ip', '::1', '2018-02-28 23:39:39', '2018-02-28 23:39:39'),
	(372, NULL, 'global', NULL, '2018-02-28 23:47:32', '2018-02-28 23:47:32'),
	(373, NULL, 'ip', '::1', '2018-02-28 23:47:32', '2018-02-28 23:47:32'),
	(374, NULL, 'global', NULL, '2018-03-04 16:55:55', '2018-03-04 16:55:55'),
	(375, NULL, 'ip', '::1', '2018-03-04 16:55:55', '2018-03-04 16:55:55'),
	(376, NULL, 'global', NULL, '2018-03-27 23:54:45', '2018-03-27 23:54:45'),
	(377, NULL, 'ip', '::1', '2018-03-27 23:54:45', '2018-03-27 23:54:45'),
	(378, NULL, 'global', NULL, '2018-03-27 23:55:49', '2018-03-27 23:55:49'),
	(379, NULL, 'ip', '::1', '2018-03-27 23:55:49', '2018-03-27 23:55:49'),
	(380, NULL, 'global', NULL, '2018-03-27 23:55:52', '2018-03-27 23:55:52'),
	(381, NULL, 'ip', '::1', '2018-03-27 23:55:52', '2018-03-27 23:55:52'),
	(382, NULL, 'global', NULL, '2018-03-27 23:56:04', '2018-03-27 23:56:04'),
	(383, NULL, 'ip', '::1', '2018-03-27 23:56:04', '2018-03-27 23:56:04'),
	(384, NULL, 'global', NULL, '2018-03-28 00:04:31', '2018-03-28 00:04:31'),
	(385, NULL, 'ip', '::1', '2018-03-28 00:04:31', '2018-03-28 00:04:31'),
	(386, NULL, 'global', NULL, '2018-03-28 00:05:04', '2018-03-28 00:05:04'),
	(387, NULL, 'ip', '::1', '2018-03-28 00:05:04', '2018-03-28 00:05:04'),
	(388, NULL, 'global', NULL, '2018-11-02 21:31:29', '2018-11-02 21:31:29'),
	(389, NULL, 'ip', '::1', '2018-11-02 21:31:29', '2018-11-02 21:31:29'),
	(390, NULL, 'global', NULL, '2018-11-05 11:23:16', '2018-11-05 11:23:16'),
	(391, NULL, 'ip', '::1', '2018-11-05 11:23:16', '2018-11-05 11:23:16'),
	(392, NULL, 'global', NULL, '2018-11-05 11:24:04', '2018-11-05 11:24:04'),
	(393, NULL, 'ip', '::1', '2018-11-05 11:24:04', '2018-11-05 11:24:04'),
	(394, NULL, 'global', NULL, '2018-11-08 20:02:13', '2018-11-08 20:02:13'),
	(395, NULL, 'ip', '::1', '2018-11-08 20:02:14', '2018-11-08 20:02:14'),
	(396, NULL, 'global', NULL, '2018-12-14 10:04:56', '2018-12-14 10:04:56'),
	(397, NULL, 'ip', '::1', '2018-12-14 10:04:56', '2018-12-14 10:04:56'),
	(398, NULL, 'global', NULL, '2018-12-14 10:05:03', '2018-12-14 10:05:03'),
	(399, NULL, 'ip', '::1', '2018-12-14 10:05:03', '2018-12-14 10:05:03');
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;

-- Dumping structure for table dishtv-portal.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fb_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `g_id` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `branch` int(11) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `supervisor_id` int(25) DEFAULT NULL,
  `lft` int(25) DEFAULT NULL,
  `rgt` int(25) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dishtv-portal.users: ~4 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `fb_id`, `g_id`, `first_name`, `last_name`, `username`, `email`, `mobile`, `branch`, `password`, `permissions`, `last_login`, `supervisor_id`, `lft`, `rgt`, `depth`, `status`, `created_at`, `updated_at`) VALUES
	(1, '', '', 'Developer', 'Cybertech', 'developer@cybertech.lk', 'developer@cybertech.lk', NULL, 0, '$2y$10$rbGZ4vVYnbeoZxJctm4AGOF/Z1fq09QFNtfR1QQ/.7bxwp0JkFJAW', NULL, '2018-12-14 10:07:53', NULL, 1, 8, 0, 1, '2018-11-02 21:37:29', '2018-12-14 10:09:03'),
	(2, '', '', 'Dealer', 'Cybertech', 'dealer@cybertech.lk', 'dealer@cybertech.lk', NULL, 0, '$2y$10$unetgy8st5R.OkBV0ILfVOXEcttIo5c2C8SuaQar.MiVVAQVMpedm', NULL, '2018-11-21 13:26:17', 1, 2, 3, 1, 1, '2018-11-02 21:37:30', '2018-11-21 13:26:18'),
	(3, '', '', 'AFROOS', 'MOHAMED', 'afroos@gmail.com', 'afroos@gmail.com', NULL, 0, '$2y$10$cCCTtBa8IkTLyowpUr1k2.vIcw7pDHVX18HaLEOXIbFTzb4lWfZmq', NULL, '2018-11-26 22:10:49', 1, 4, 5, 1, NULL, '2018-11-26 21:16:14', '2018-11-26 22:10:49'),
	(7, '', '', 'ADMIN', 'DISH-TV', 'admin@cybertech.lk', 'admin@cybertech.lk', NULL, 0, '$2y$10$jps7/3qp1UdKnzUh8hjmiefABjc4izQkt8quoXW09HYCDE/AOzMA2', NULL, '2018-12-14 10:09:36', 1, 6, 7, 1, NULL, '2018-11-26 21:30:42', '2018-12-14 10:09:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
