<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sa_customer_packages', function ($table) {
          $table->increments('id');
          $table->integer('customer_id');
          $table->string('regName');
          $table->string('vcNumber');
          $table->string('provider');
          $table->string('basePackINR');
          $table->string('basePackLKR');
          $table->string('totalINR');
          $table->string('totalLKR');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sa_customer_packages');
    }
}
