<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	/*DEVELOPER CREATION*/
        $credentials_developer = [
                    'first_name' => 'Developer',
                    'last_name' => 'Cybertech',
                    'email' => 'developer@cybertech.lk',                    
                    'username' => 'developer@cybertech.lk',
                    'password' =>'123456',
                    'status'=>1
                ];
         $user_developer = Sentinel::registerAndActivate($credentials_developer);
         $user_developer->makeRoot();
         $role_developer= Sentinel::findRoleById(1);
         $role_developer->users()->attach($user_developer);

         /*SAMBOLE ADMIN CREATION*/
        $credentials_admin = [
                    'first_name' => 'Dealer',
                    'last_name' => 'Cybertech',
                    'email' => 'dealer@cybertech.lk',                    
                    'username' => 'dealer@cybertech.lk',
                    'password' =>'123456',
                    'status'=>1
                ];
         $user_admin = Sentinel::registerAndActivate($credentials_admin);
         $user_admin->makeChildOf($user_developer);
         $role_admin = Sentinel::findRoleById(2);
         $role_admin->users()->attach($user_admin);
    }
}
