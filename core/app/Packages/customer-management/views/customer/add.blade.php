@extends('layouts.back.master') @section('current_title','New Customer')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<style type="text/css">
  h4{
    text-align: center;
    color: #ef5a5a;
    padding-bottom: 20px;
  }

  sup{
    color: #ef5a5a;
  }
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('customer/list')}}">Customer Management</a></li>
       
        <li class="active">
            <span>New Customer</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="myForm">
                	{!!Form::token()!!}
                  <div class="row">
                    <div class="col-md-6">
                      <h4>Customer Information</h4>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Name <sup>*</sup></label>
                        <div class="col-sm-8"><input type="text" class="form-control" name="name"></div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">Mobile </label>
                          <div class="col-sm-8">
                            <div class="input-group">
                                <input type="number" class="form-control" name="mobile" id="mobiles">
                                <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-phone"></span></span>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">E-mail </label>
                          <div class="col-sm-8">
                            <div class="input-group">
                                <input type="email" class="form-control" name="email" id="email">
                                <span class="input-group-addon" id="start-date"><span class="glyphicon glyphicon-envelope"></span></span>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Member </label>
                        <div class="col-sm-8">
                          <select class="form-control js-source-states1" name="dealer" id="dealer">
                            <option></option>
                            @foreach ($members as $member)
                              <option value="{{ $member['id'] }}">{{ $member['name'] }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Location </label>
                        <div class="col-sm-8">
                          <select class="form-control js-source-states3" name="location" id="location">
                            <option></option>
                            @foreach ($cities as $city)
                              <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Notes </label>
                        <div class="col-sm-8"><textarea class="form-control" name="notes" id="notes"></textarea></div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h4>Pack Information</h4>
                      <div class="form-group">
                        <label class="col-sm-4 control-label">Registered Name </label>
                        <div class="col-sm-8"><input type="text" class="form-control" name="regName"></div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">VC Number <sup>*</sup></label>
                        <div class="col-sm-8"><input type="text" class="form-control" name="vcNumber"></div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Provider </label>
                        <div class="col-sm-8">
                          <select class="form-control js-source-states2" name="provider" id="provider">
                            <option></option>
                            @foreach ($serviceProvider as $provider)
                              <option value="{{ $provider['id'] }}">{{ $provider['name'] }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Base Pack </label>
                        <div class="col-sm-8">
                          <select class="form-control js-source-states4" multiple="" name="package[]" id="package">
                            <option></option>
                            @foreach ($packages as $package)
                              <option value="{{ $package['id'] }}">{{ $package['name'] }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Addons </label>
                        <div class="col-sm-8">
                          <select class="form-control js-source-states5" multiple="" name="channels[]" id="channels">
                            <option></option>
                            @foreach ($channels as $channel)
                              <option value="{{ $channel['id'] }}">{{ $channel['name'] }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">Base Pack INR </label>
                          <div class="col-sm-8">
                            <div class="input-group">
                                <input type="number" class="form-control" name="baseINR" id="baseINR">
                                <span class="input-group-addon" id="start-date"><span>&#x20B9;</span></span>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">Base Pack LKR  </label>
                          <div class="col-sm-8">
                            <div class="input-group">
                                <input type="number" class="form-control" name="baseLKR" id="baseLKR">
                                <span class="input-group-addon" id="start-date"><span style="font-size: xx-small">&#8360;</span></span>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">Total INR </label>
                          <div class="col-sm-8">
                            <div class="input-group">
                                <input type="number" class="form-control" name="totalINR" id="totalINR">
                                <span class="input-group-addon" id="start-date"><span>&#x20B9;</span></span>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="col-sm-4 control-label">Total LKR  </label>
                          <div class="col-sm-8">
                            <div class="input-group">
                                <input type="number" class="form-control" name="totalLKR" id="totalLKR">
                                <span class="input-group-addon" id="start-date"><span style="font-size: xx-small">&#8360;</span></span>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div> 

                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <div style="padding-right: 20px">                          
                          <button class="btn btn-primary pull-right" type="submit">Done</button>
                          <button class="btn btn-default pull-right" type="button" onclick="location.reload();">Cancel</button>
                      </div>
                  </div>               	
                </form>
        </div>
    </div>
</div>


@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states1").select2({
      placeholder: "Select Member"
  });

    $(".js-source-states2").select2({
      placeholder: "Select Service Provider"
  });

    $(".js-source-states3").select2({
      placeholder: "Select Location"
  });

    $(".js-source-states4").select2({
      placeholder: "Select Package"
  });

    $(".js-source-states5").select2({
      placeholder: "Add Channels"
  });

        $('.js-source-states4').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            // alert(valueSelected);
            $('#baseINR').val('');
            $('#baseLKR').val('');
            $('#totalINR').val('');
            $('#totalLKR').val('');
                    $.ajax({
                    type: 'GET',
                    url: 'json/getPrice',
                    data: { q: valueSelected },
                    // resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        let dataObj = json;
                        $('#baseINR').val(dataObj[0]["inr"]);
                        $('#baseLKR').val(dataObj[0]["lkr"]);
                        $('#totalINR').val(dataObj[0]["inr"]);
                        $('#totalLKR').val(dataObj[0]["lkr"]);
                    }
                });
        });


        $('#channels').on('change',function() {
          var selectedChannel=$(this).val();
          // console.log(selectedChannel);
          var sum=parseInt($('#totalINR').val());
          var sum=parseInt($('#totalLKR').val());

          selectedChannel.forEach(function(element) {
            // console.log(element);
            $.ajax({
                    type: 'GET',
                    url: 'json/getSumPrice',
                    data: { q: element },
                    // resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        let dataObj = json;
                        sum+=parseInt(dataObj[0]["price"]);
                        $('#totalINR').val(sum*0.42);
                        $('#totalLKR').val(sum);
                        console.log(sum);
                    }
                });
          });
          

        });



		$("#myForm").validate({
            rules: {
                name: {
                    required: true                  
                },
                lkr: {
                    required: true                  
                },
                inr: {
                    required: true                  
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });


    $('#lkr').on('input', function() {
          console.log($(this).val() );

          $('#inr').val($(this).val()*0.42);
          // $.ajax({
          //           type: 'GET',
          //           url: 'http://openexchangerates.org/api/latest.json?app_id=eb574cbcd9704cd8a6c626a03d20f259',
          //           // data: { q: valueSelected },
          //           // resultObj: this.resultObj,
          //           dataType:'JSON',
          //           success: function (json) {
          //              console.log(json);
                                              
          //           }
          //       });
      });

	});
	
	
</script>
@stop