<?php
namespace CustomerManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Branch Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class Customer extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_customers';

	protected $primaryKey = 'id';

	public function customerPackage()
		{
			return $this->hasOne('CustomerManage\Models\CustomerPackage', 'customer_id', 'id');
		}

	public function addonsChannels()
		{
			return $this->hasMany('CustomerManage\Models\CustomerAddoneChannels', 'customer_id', 'id');
		}

	public function customerBasePackage()
		{
			return $this->hasMany('CustomerManage\Models\CustomerBasePackage', 'customer_id', 'id');
		}


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','mobile','email','member','location','notes'];


}
