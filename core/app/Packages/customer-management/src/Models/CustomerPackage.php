<?php
namespace CustomerManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Gallery Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */

class CustomerPackage extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_customer_packages';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['customer_id','regName','vcNumber','provider','basePack','basePackINR','basePackLKR','periodOfMonth','payment','totalINR','totalLKR'];


	// public function customer()
 //    {
 //        return $this->belongsTo('CustomerManage\Models\Customer', 'id', 'customer_id');
 //    }

}
