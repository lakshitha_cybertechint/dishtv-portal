<?php
namespace CustomerManage\Http\Controllers;

use App\Http\Controllers\Controller;
use CustomerManage\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use CustomerManage\Models\Customer;
use CustomerManage\Models\CustomerAddoneChannels;
use CustomerManage\Models\CustomerBasePackage;
use CustomerManage\Models\CustomerPackage;
use ServiceProviderManage\Models\ServiceProvider;
use ChannelManage\Models\ChannelSeriveProviders;
use ChannelManage\Models\Channel;
use DealerManage\Models\Dealer;
use MemberManage\Models\Member;
use PackageManage\Models\Package;
use Permissions\Models\Permission;
use DB;
use Sentinel;
use Response;
use File;



class CustomerController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Customer Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "customer management page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the customer add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$serviceProvider = ServiceProvider::get();
		$channels = Channel::get();
		$dealers = Dealer::get();
		$members = Member::get();
		$packages = Package::get();
		$cities = DB::table('sa_city')->get();
		return view( 'CustomerManage::customer.add')->with([ 
				'serviceProvider' => $serviceProvider,
				'channels' => $channels,
				'dealers' => $dealers,
				'members' => $members,
				'packages' => $packages,
				'cities' => $cities
				 ]);
	}


	public function getPrice(Request $request) {
		$package = Package::where('id','=',$request->q)->get();
        return $package;
    }

    public function getSumPrice(Request $request) {
    	$sum = 0;
    	foreach ($request as $key => $value) {
    		$channel = Channel::where('id','=',$request->q)->get();
    	}
		
        return $channel;    
    }

	/**
	 * Add new customer data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(CustomerRequest $request)
	{	
		// dd($request->request);
		$paid=0;
		if($request->paid!=""){
			$paid=1;
		}
		$count= Customer::where('name', '=',$request->name)->count();
		if ($count==0) {
			$Customer = new Customer();
			$Customer->name = $request->name;
			$Customer->mobile = $request->mobile;
			$Customer->email = $request->email;
			$Customer->member = $request->dealer;
			$Customer->location = $request->location;
			$Customer->notes = $request->notes;

			$Customer->save();	
			$custId = $Customer->id;
			if($Customer){			
				CustomerPackage::create([
					'customer_id' => $custId,
					'regName' => $request->regName,
					'vcNumber' => $request->vcNumber,
					'provider' => $request->provider,
					'basePackINR' => $request->baseINR,
					'basePackLKR' => $request->baseLKR,
					'totalINR' => $request->totalINR,
					'totalLKR' => $request->totalLKR
				]);				
			}

			if($Customer){
				if ($request->package && count($request->package) > 0) {
		            foreach($request->package as $key => $packageID){
		              CustomerBasePackage::create([
		              	'customer_id' => $custId,
		                'package_id' => $packageID
		            ]);
		        }
			}
		}

			if($Customer){
				if ($request->channels && count($request->channels) > 0) {
		            foreach($request->channels as $key => $channelId){
		              CustomerAddoneChannels::create([
		              	'customer_id' => $custId,
		                'channel_id' => $channelId
		            ]);
		        }
			}
		}	


		return redirect('customer/add')->with([ 'success' => true,
				'success.message'=> 'Customer Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('customer/add')->with([ 'error' => true,
				'error.message'=> 'Customer Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View customer List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'CustomerManage::customer.list' );
	}

	/**
	 * customer list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Customer::get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $Customer) {
				$member= Member::find($Customer->member);
				// $location= DB::table('sa_city')->where('id','=',$Customer->location)->get();
				$dd = array();

				array_push($dd, $i);				
				array_push($dd, $Customer->name);
				array_push($dd, $Customer->mobile);
				array_push($dd, $member['name']);
				// array_push($dd, $location['name']);
					
				$permissions = Permission::whereIn('name',['customer.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('customer/edit/'.$Customer->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['customer.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="Customer-delete" data-id="'.$Customer->id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}



	/**
	 * Delete a customer
	 * @param  Request $request customer id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$Customer = Customer::find($id);
			CustomerPackage::where('Customer_id','=',$id)->delete();
			CustomerBasePackage::where('Customer_id','=',$id)->delete();
			CustomerAddoneChannels::where('Customer_id','=',$id)->delete();
			if($Customer){
				$Customer->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the customer edit screen to the customer.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curCustomer=Customer::find($id);
		$serviceProvider = ServiceProvider::get();
		$channels = Channel::get();
		$dealers = Dealer::get();
		$members = Member::get();
		$packages = Package::get();
		$cities = DB::table('sa_city')->get();

		if($curCustomer){
			return view( 'CustomerManage::customer.edit' )->with([ 
				'curCustomer' => $curCustomer,
				'serviceProvider'=>$serviceProvider,
				'channels'=>$channels,
				'dealers' => $dealers,
				'members' => $members,
				'packages' => $packages,
				'cities' => $cities
				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to customer add
	 */
	public function edit(CustomerRequest $request, $id)
	{		
		// dd($request->request);
		$paid=0;
		if($request->paid!=""){
			$paid=1;
		}
		$count= Customer::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		$Customer =  Customer::find($id);
		
			if($count==0){
				$CustomerOld =  Customer::where('id',$id)->take(1)->get();
				$customer=$CustomerOld[0];			
				$customer->name = $request->get('name');
				$customer->mobile = $request->get('mobile');
				$customer->email = $request->get('email');
				$customer->member = $request->get('dealer');
				$customer->location = $request->get('location');
				$customer->notes = $request->get('notes');

				CustomerPackage::where('Customer_id', $customer->id)->delete();
				CustomerBasePackage::where('Customer_id', $customer->id)->delete();
				CustomerAddoneChannels::where('Customer_id', $customer->id)->delete();

		        if($Customer){			
				CustomerPackage::create([
					'customer_id' => $id,
					'regName' => $request->regName,
					'vcNumber' => $request->vcNumber,
					'provider' => $request->provider,
					'basePackINR' => $request->baseINR,
					'basePackLKR' => $request->baseLKR,
					'totalINR' => $request->totalINR,
					'totalLKR' => $request->totalLKR
				]);				
			}

				if($Customer){
					if ($request->package && count($request->package) > 0) {
			            foreach($request->package as $key => $packageID){
			              CustomerBasePackage::create([
			              	'customer_id' => $id,
			                'package_id' => $packageID
			            ]);
			        }
				}
			}

				if($Customer){
					if ($request->channels && count($request->channels) > 0) {
			            foreach($request->channels as $key => $channelId){
			              CustomerAddoneChannels::create([
			              	'customer_id' => $id,
			                'channel_id' => $channelId
			            ]);
			        }
				}
			}
				

				$customer->save();
				
				return redirect( 'customer/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Customer updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('customer/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Customer Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}


}
