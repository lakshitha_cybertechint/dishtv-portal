<?php
namespace CustomerManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class CustomerRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){    

	
		$rules = [
			'name' => 'required',			
		];
	
		
		return $rules;
	}

}
