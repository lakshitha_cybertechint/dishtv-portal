<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'customer', 'namespace' => 'CustomerManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'customer.add', 'uses' => 'customerController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'customer.edit', 'uses' => 'customerController@editView'
      ]);

      Route::get('list', [
        'as' => 'customer.list', 'uses' => 'customerController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'customer.list', 'uses' => 'customerController@jsonList'
      ]);

      Route::get('json/getPrice', [
        'as' => 'customer.add', 'uses' => 'customerController@getPrice'
      ]);

      Route::get('json/getSumPrice', [
        'as' => 'customer.add', 'uses' => 'customerController@getSumPrice'
      ]);



      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'customer.add', 'uses' => 'customerController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'customer.edit', 'uses' => 'customerController@edit'
      ]);

      Route::post('status', [
        'as' => 'customer.status', 'uses' => 'customerController@status'
      ]);

      Route::post('delete', [
        'as' => 'customer.delete', 'uses' => 'customerController@delete'
      ]);
    });
});