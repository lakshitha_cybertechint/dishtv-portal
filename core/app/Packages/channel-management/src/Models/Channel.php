<?php
namespace ChannelManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Gallery Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class Channel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_channel';

	protected $primaryKey = 'id';

	public function service_providers()
		{
			return $this->hasMany('ChannelManage\Models\channelSeriveProviders',  'channel_id', 'id');
		}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','code','price','description','logo_name','category','serviceProvider','language'];

}
