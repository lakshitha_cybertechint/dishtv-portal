<?php
namespace ChannelManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Gallery Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class ChannelSeriveProviders extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_channel_service_providers';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['channel_id','service_providers_id'];


	public function service_provider()
    {
        return $this->belongsTo('ServiceProviderManage\Models\ServiceProvider', 'id', 'service_providers_id');
    }

}
