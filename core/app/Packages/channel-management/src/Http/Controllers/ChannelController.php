<?php
namespace ChannelManage\Http\Controllers;

use App\Http\Controllers\Controller;
use ChannelManage\Http\Requests\ChannelRequest;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ChannelManage\Models\Channel;
use ChannelManage\Models\ChannelCategory;
use ChannelManage\Models\ChannelLang;
use ChannelManage\Models\ChannelSeriveProviders;
use ServiceProviderManage\Models\ServiceProvider;

// use DeviceManage\Models\Device;
use Sentinel;
use Response;
use File;


class ChannelController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Channel Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Channel CATEGORY add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_category()
	{
		return view( 'ChannelManage::category.add');
	}

	/**
	 * Add new Channel CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_category(ChannelRequest $request)
	{
		// dd($request);

        $count= ChannelCategory::where('name', '=',$request->get('name' ))->count();

        if ($count==0) {

            $ChannelCategory = new ChannelCategory();
            $ChannelCategory->name = $request->name;
            $ChannelCategory->description = $request->description;

            $ChannelCategory->save();

            return redirect('channel/category/add')->with(['success' => true,
                    'success.message' => 'Channel Category Created successfully!',
                    'success.title' => 'Well Done!']);
            
        }
        else{
            return redirect('channel/category/add')->with([ 'error' => true,
                'error.message'=> 'Channel Category Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View Channel CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_category()
	{
		return view( 'ChannelManage::category.list' );
	}

	/**
	 * Channel CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_category(Request $request)
	{
		if($request->ajax()){
			$channelCategories= ChannelCategory::get();
			$jsonList = array();
			$i=1;
			foreach ($channelCategories as $key => $channelCategory) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$channelCategory->name);

				$permissions = Permission::whereIn('name',['channel.category.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('channel/category/edit/'.$channelCategory->id).'\'" data-toggle="tooltip" data-placement="top" title="Channel Category Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['Channel.category.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="channel-category-delete" data-id="'.$channelCategory->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}



	/**
	 * Delete a Channel CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_category(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$channelCategory = ChannelCategory::find($id);
			if($channelCategory){
                $channelCategory->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the Channel CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_category($id)
	{
		$channelCategory=ChannelCategory::find($id);
		if($channelCategory){
			return view( 'ChannelManage::category.edit' )->with(['channelCategory'=>$channelCategory]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new Channel CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_category(ChannelRequest $request, $id)
	{
		$count= ChannelCategory::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();

			if($count==0){
                $channelCategory =  ChannelCategory::find($id);
                $channelCategory->name = $request->get('name');
                $channelCategory->description = $request->get('description');

                $channelCategory->save();

				return redirect( 'channel/category/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Channel Category updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('channel/category/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Channel Category Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

	}

	/****************************************Channel ******/



	// 

	/**
	 * Show the Channel Lang add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_lang()
	{
		return view( 'ChannelManage::lang.add');
	}

	/**
	 * Add new Channel Lang data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_lang(ChannelRequest $request)
	{
		// dd($request);

        $count= ChannelLang::where('name', '=',$request->get('lang' ))->count();

        if ($count==0) {

            $ChannelLang = new ChannelLang();
            $ChannelLang->name = $request->lang;

            $ChannelLang->save();

            return redirect('channel/lang/add')->with(['success' => true,
                    'success.message' => 'Language Created successfully!',
                    'success.title' => 'Well Done!']);
            
        }
        else{
            return redirect('channel/lang/add')->with([ 'error' => true,
                'error.message'=> 'Language Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View Channel Lang List View
	 *
	 * @return Response
	 */
	public function listView_lang()
	{
		return view( 'ChannelManage::lang.list' );
	}

	/**
	 * Channel Lang list
	 *
	 * @return Response
	 */
	public function jsonList_lang(Request $request)
	{
		if($request->ajax()){
			$channelLangs= ChannelLang::get();
			$jsonList = array();
			$i=1;
			foreach ($channelLangs as $key => $channelLang) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$channelLang->name);

				$permissions = Permission::whereIn('name',['channel.Lang.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('channel/lang/edit/'.$channelLang->id).'\'" data-toggle="tooltip" data-placement="top" title="Channel Lang Branch"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['Channel.Lang.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="channel-lang-delete" data-id="'.$channelLang->id.'" data-toggle="tooltip" data-placement="top" title="Delete Series"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}



	/**
	 * Delete a Channel Lang
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_lang(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$channelLang = ChannelLang::find($id);
			if($channelLang){
                $channelLang->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the Channel Lang edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_Lang($id)
	{
		$channelLang=ChannelLang::find($id);
		if($channelLang){
			return view( 'ChannelManage::lang.edit' )->with(['channelLang'=>$channelLang]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new Channel Lang data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_Lang(ChannelRequest $request, $id)
	{
		$count= ChannelLang::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();

			if($count==0){
                $channelLang =  ChannelLang::find($id);
                $channelLang->name = $request->get('name');

                $channelLang->save();

				return redirect( 'channel/lang/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Channel Language updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('channel/lang/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Channel Language Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

	}
//////////////////////////////////////////////////////////////////////////////////////////
	


	/**
	 * Show the Channel  add screen to the user.
	 *
	 * @return Response
	 */
	public function addView_channel()
	{
		$channelCategory=ChannelCategory::get();
		$serviceProvider=ServiceProvider::get();
		$langs=ChannelLang::get();
		return view( 'ChannelManage::channel.add')->with(['channelCategory'=>$channelCategory,'serviceProvider'=>$serviceProvider,'langs'=>$langs]);
	}

	/**
	 * Add new Channel CATEGORY data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add_Channel(ChannelRequest $request)
	{
		$cat ="";
		$lang= "";
		// $sProvider="";
		if($request->channel_category){
		$cat = implode(',',$request->channel_category);
		}
		if($request->language){
		$lang = implode(',',$request->language);
		}
		// if($request->serviceProvider){
		// $sProvider = implode(',',$request->serviceProvider);
		// }
		
        $count= Channel::where('name', '=',$request->get('name' ))->count();
        
        if ($count==0) {
			$channel = new Channel();
			$channel->name = $request->name;
			$channel->code = $request->code;
			$channel->price = $request->price;
			$channel->description = $request->description;

			if($request->logo_img){
			$logopath = 'uploads/images/logo/channel';
        	$logoName = 'channel-logo-' . date('YmdHis') . '.png';
        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

        	$channel->logo_name = $logoName;
        	}

        	$channel->category = $cat;
        	// $channel->serviceProvider = $sProvider;
        	$channel->language = $lang;
			$channel->save();

			if($channel){
				if ($request->serviceProvider && count($request->serviceProvider) > 0) {
		            foreach($request->serviceProvider as $key => $sId){
		              ChannelSeriveProviders::create([
		              	'channel_id' => $channel->id,
		                'service_providers_id' => $sId
		            ]);
		        }
			}
		}

                return redirect('channel/add')->with(['success' => true,
                    'success.message' => 'Channel  Created successfully!',
                    'success.title' => 'Well Done!']);
            
        }
        else{
            return redirect('channel/add')->with([ 'error' => true,
                'error.message'=> 'Channel  Already Exsist!',
                'error.message'=> 'Channel  Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
	}

	/**
	 * View Channel CATEGORY List View
	 *
	 * @return Response
	 */
	public function listView_Channel()
	{
		return view( 'ChannelManage::channel.list' );
	}

	/**
	 * Channel CATEGORY list
	 *
	 * @return Response
	 */
	public function jsonList_Channel(Request $request)
	{
		if($request->ajax()){
			$channels= Channel::get();
			$jsonList = array();
			$i=1;
			foreach ($channels as $key => $channel) {

				$dd = array();
				array_push($dd, $i);
                array_push($dd,$channel->name);
                array_push($dd,$channel->code);
                array_push($dd,$channel->price);

				$permissions = Permission::whereIn('name',['channel.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('channel/edit/'.$channel->id).'\'" data-toggle="tooltip" data-placement="top" title="Channel "><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['channel.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="channel-delete" data-id="'.$channel->id.'" data-toggle="tooltip" data-placement="top" title="Delete Channel"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Delete a Channel CATEGORY
	 * @param  Request $request series id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete_Channel(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$channel = Channel::find($id);
			ChannelSeriveProviders::where('channel_id','=',$id)->delete();
			if($channel){
                $channel->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the Channel CATEGORY edit screen to the series.
	 *
	 * @return Response
	 */
	public function editView_Channel($id)
	{
		$channel=Channel::where('id', $id)->first();
		$channelCategory=ChannelCategory::get();
		$serviceProvider=ServiceProvider::get();
		$langs=ChannelLang::get();

		if($channel){
			return view( 'ChannelManage::channel.edit' )->with(['channel'=>$channel,'channelCategory'=>$channelCategory, 'serviceProvider'=>$serviceProvider, 'langs'=>$langs]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new Channel CATEGORY data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit_Channel(ChannelRequest $request, $id)
	{
		$cat ="";
		$lang= "";
		if($request->channel_category){
			$cat = implode(",",$request["channel_category"]);
	        $cat = preg_replace('/\s*,\s*/', ',', $cat);
	        $cat = explode(',', $cat);
	        $cat = array_unique($cat);
	        $cat = implode(",",$cat);
	    }
	    if($request->language){
	        $lang = implode(",",$request["language"]);
	        $lang = preg_replace('/\s*,\s*/', ',', $lang);
	        $lang = explode(',', $lang);
	        $lang = array_unique($lang);
	        $lang = implode(",",$lang);
	    }

		// dd($request);

		$count= Channel::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		$channel =  Channel::find($id);
		
			if($count==0){
				$channelOld =  Channel::where('id',$id)->take(1)->get();
				$branch=$channelOld[0];			
				$branch->name = $request->get('name');
				$branch->code = $request->get('code');
				$branch->price = $request->get('price');
				$branch->description = $request->get('description');
				if ($request->logo_img != '') {
					File::delete(storage_path('core/storage/uploads/images/logo/channel/' . $channel->logo_name));
			        $logopath = 'uploads/images/logo/channel';
		        	$logoName = 'channel-logo-' . date('YmdHis') . '.png';
		        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

		        	$branch->logo_name = $logoName;
			        }
			    $branch->category = $cat;
			    $branch->language = $lang;

			    ChannelSeriveProviders::where('channel_id', $branch->id)->delete();

		        if ($request->serviceProvider && count($request->serviceProvider) > 0) {
		          foreach($request->serviceProvider as $key => $catId){
		            ChannelSeriveProviders::create([
		              'channel_id' => $branch->id,
		              'service_providers_id' => $catId
		            ]);
		          }
		        }

				$branch->save();
				
				return redirect( 'channel/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Channel updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('channel/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Channel Already Exsist!',
				'error.title' => 'Duplicate!']);
			}


	}
	public function jsonImageFileDelete_Channel(Request $request)
	{
		$image_id=$request->get('key');
		$image=ChannelImage::find($image_id);
		$image->delete();
		return 1;
	}


	function save_logo_image($image, $path){// sase base64 encoded image,  path with image name

      list($type, $image) = explode(';', $image);
      list(, $image)      = explode(',', $image);
      $image = base64_decode($image);

      file_put_contents($path, $image);

    }

}
