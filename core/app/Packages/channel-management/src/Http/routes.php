<?php
/**
 * Gallery MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'channel/category', 'namespace' => 'ChannelManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'channel.category.add', 'uses' => 'ChannelController@addView_category'
      ]);

      Route::get('edit/{id}', [
        'as' => 'channel.category.edit', 'uses' => 'ChannelController@editView_category'
      ]);

      Route::get('list', [
        'as' => 'channel.category.list', 'uses' => 'ChannelController@listView_category'
      ]);

      Route::get('json/list', [
        'as' => 'channel.category.list', 'uses' => 'ChannelController@jsonList_category'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'channel.category.add', 'uses' => 'ChannelController@add_category'
      ]);

      Route::post('edit/{id}', [
        'as' => 'channel.category.edit', 'uses' => 'ChannelController@edit_category'
      ]);

      Route::post('status', [
        'as' => 'channel.category.status', 'uses' => 'ChannelController@status_category'
      ]);

      Route::post('delete', [
        'as' => 'channel.category.delete', 'uses' => 'ChannelController@delete_category'
      ]);
    });


    Route::group(['prefix' => 'channel/lang', 'namespace' => 'ChannelManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'channel.lang.add', 'uses' => 'ChannelController@addView_lang'
      ]);

      Route::get('edit/{id}', [
        'as' => 'channel.lang.edit', 'uses' => 'ChannelController@editView_lang'
      ]);

      Route::get('list', [
        'as' => 'channel.lang.list', 'uses' => 'ChannelController@listView_lang'
      ]);

      Route::get('json/list', [
        'as' => 'channel.lang.list', 'uses' => 'ChannelController@jsonList_lang'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'channel.lang.add', 'uses' => 'ChannelController@add_lang'
      ]);

      Route::post('edit/{id}', [
        'as' => 'channel.lang.edit', 'uses' => 'ChannelController@edit_lang'
      ]);

      Route::post('status', [
        'as' => 'channel.lang.status', 'uses' => 'ChannelController@status_lang'
      ]);

      Route::post('delete', [
        'as' => 'channel.lang.delete', 'uses' => 'ChannelController@delete_lang'
      ]);
    });

    
   Route::group(['prefix' => 'channel', 'namespace' => 'ChannelManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'channel.add', 'uses' => 'ChannelController@addView_channel'
      ]);

      Route::get('edit/{id}', [
        'as' => 'channel.edit', 'uses' => 'ChannelController@editView_channel'
      ]);

      Route::get('list', [
        'as' => 'channel.list', 'uses' => 'ChannelController@listView_channel'
      ]);

      Route::get('json/list', [
        'as' => 'channel.list', 'uses' => 'ChannelController@jsonList_channel'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'channel.add', 'uses' => 'ChannelController@add_channel'
      ]);

      Route::post('edit/{id}', [
        'as' => 'channel.edit', 'uses' => 'ChannelController@edit_channel'
      ]);

      Route::post('status', [
        'as' => 'channel.status', 'uses' => 'ChannelController@status_channel'
      ]);

      Route::post('delete', [
        'as' => 'channel.delete', 'uses' => 'ChannelController@delete_channel'
      ]);
      Route::delete('image/deleteFile', [
      'as' => 'channel.edit', 'uses' => 'ChannelController@jsonImageFileDelete_channel',
    ]);
    });
});
