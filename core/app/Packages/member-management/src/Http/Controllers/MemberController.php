<?php
namespace MemberManage\Http\Controllers;

use App\Http\Controllers\Controller;
use MemberManage\Http\Requests\MemberRequest;
use Illuminate\Http\Request;
use MemberManage\Models\Member;
use UserManage\Models\User;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;



class MemberController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Branch Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$users = User::get();
		return view( 'MemberManage::member.add')->with([ 
				'users' => $users
				 ]);
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(MemberRequest $request)
	{
		$count= Member::where('name', '=',$request->name)->count();

		if ($count==0) {
			$member = new Member();
			$member->name = $request->name;
			$member->email = $request->email;
			$member->tel = $request->tel;
			$member->address = $request->address;

			if($request->logo_img){

			$logopath = 'uploads/images/logo/member';
        	$logoName = 'member-business-logo-' . date('YmdHis') . '.png';
        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

        	$member->logo_name = $logoName;
        	}
        	$member->regNo = $request->regNo;
        	$member->user = Sentinel::getUser()->id;
			$member->save();		


		return redirect('member/add')->with([ 'success' => true,
				'success.message'=> 'Member Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('member/add')->with([ 'error' => true,
				'error.message'=> 'Member Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'MemberManage::member.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Member::get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $member) {
				
				$dd = array();
				array_push($dd, $i);
				
				array_push($dd, $member->name);
				if($member->email != ""){
					array_push($dd, $member->email);
				}else{
					array_push($dd, "-");
				}
				if($member->tel != ""){
					array_push($dd, $member->tel);
				}else{
					array_push($dd, "-");
				}	
				$permissions = Permission::whereIn('name',['member.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('member/edit/'.$member->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['member.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="member-delete" data-id="'.$member->id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Delete a branch
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$member = Member::find($id);
			if($member){
				$member->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curmember=Member::find($id);
		$users=User::get();
		if($curmember){
			return view( 'MemberManage::member.edit' )->with([ 
				'curmember' => $curmember, 'users' => $users
				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(MemberRequest $request, $id)
	{		
		// dd($request->request);
		$count= Member::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		$member =  Member::find($id);
		
			if($count==0){
				$memberOld =  Member::where('id',$id)->take(1)->get();
				$branch=$memberOld[0];			
				$branch->name = $request->get('name');
				$branch->email = $request->get('email');
				$branch->tel = $request->get('tel');
				$branch->address = $request->get('address');
								if ($request->logo_img != '') {
					File::delete(storage_path('core/storage/uploads/images/logo/member/' . $member->logo_name));
			        $logopath = 'uploads/images/logo/member';
		        	$logoName = 'member-business-logo-' . date('YmdHis') . '.png';
		        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

		        	$branch->logo_name = $logoName;
			        }
			    $branch->regNo = $request->get('regNo');
			    $branch->user = Sentinel::getUser()->id;


				$branch->save();
				
				return redirect( 'member/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Member updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('member/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Member Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}

	function save_logo_image($image, $path){// sase base64 encoded image,  path with image name

      list($type, $image) = explode(';', $image);
      list(, $image)      = explode(',', $image);
      $image = base64_decode($image);

      file_put_contents($path, $image);

    }

}
