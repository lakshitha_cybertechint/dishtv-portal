<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'member', 'namespace' => 'MemberManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'member.add', 'uses' => 'MemberController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'member.edit', 'uses' => 'MemberController@editView'
      ]);

      Route::get('list', [
        'as' => 'member.list', 'uses' => 'MemberController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'member.list', 'uses' => 'MemberController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'member.add', 'uses' => 'MemberController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'member.edit', 'uses' => 'MemberController@edit'
      ]);

      Route::post('status', [
        'as' => 'member.status', 'uses' => 'MemberController@status'
      ]);

      Route::post('delete', [
        'as' => 'member.delete', 'uses' => 'MemberController@delete'
      ]);
    });
});