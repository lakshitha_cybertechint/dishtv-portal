@extends('layouts.back.master') @section('current_title','Update Package')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('package/list')}}">Package Management</a></li>
       
        <li class="active">
            <span>Update Package</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="myForm" files="true" enctype="multipart/form-data">
                	{!!Form::token()!!}

                	<div class="form-group"><label class="col-sm-2 control-label">PACKAGE NAME <sup>*</sup></label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{ $curPackage['name']}}"></div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">SERVICE PROVIDER</label>
                         <div class="col-sm-10">
                         <select class="js-source-states1 required" name="serviceProvider" style="width: 100%" required>
                            <option></option>
                         <?php foreach ($serviceProvider as $key => $value): ?>
                             <option value="{{$value->id}}" {{ ($curPackage['service_providers']==$value->id)?'selected':'' }}>{{$value->name}}</option>
                         <?php endforeach ?>
                        </select>
                    </div>                      
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">CHANNELS</label>
                         <div class="col-sm-10">
                         <select id="channelSelect" class="js-source-states required" multiple="multiple" name="channels[]" style="width: 100%" required>
                          @foreach($channel as $item)
                              <option value="{{$item->id}}" {{$curPackage->package_channels->where('channel_id', $item->id)->count() > 0 ? 'selected' : ''}}>{{$item->name}}</option>
                          @endforeach
                        </select>
                    </div>                      
                  </div> 

                  <div class="form-group"><label class="col-sm-2 control-label">PRICE INR <sup>*</sup></label>
                      <div class="col-sm-10"><input type="number" class="form-control" id="inr" name="inr" value="{{ $curPackage['inr']}}"></div>
                  </div>                 

                  <div class="form-group"><label class="col-sm-2 control-label">PRICE LKR <sup>*</sup></label>
                      <div class="col-sm-10"><input type="number" class="form-control" id="lkr" name="lkr" value="{{ $curPackage['lkr']}}"></div>
                  </div>
                                                    
                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <div class="col-sm-8 col-sm-offset-2">
                          <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                          <button class="btn btn-primary" type="submit">Done</button>
                      </div>
                  </div>
                </form>
        </div>
    </div>
</div>



@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states1").select2({
      placeholder: "Select service Provider"
  });

    $(".js-source-states").select2({
      placeholder: "Select Chanels"
  });

		
        $('.js-source-states1').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            console.log(valueSelected);
            $('#channelSelect').html('');
                    $.ajax({
                    type: 'GET',
                    url: 'json/sort',
                    data: { q: valueSelected },
                    // resultObj: this.resultObj,
                    dataType:'JSON',
                    success: function (json) {
                        let dataObj = json;

                        if (typeof dataObj.forEach == 'function') {
                            dataObj.forEach(function(element) {
                                console.log(element[0]["id"]+' '+element[0]["name"]);
                                $('#channelSelect')
                                          .append($("<option></option>")
                                          .attr("value",element[0]["id"])
                                          .text(element[0]["name"])); 
                            });
                        }                        
                    }
                });
        });

    $("#myForm").validate({
            rules: {
                name: {
                    required: true                  
                },
                lkr: {
                    required: true                  
                },
                inr: {
                    required: true                  
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });


    $('#inr').on('input', function() {
          console.log($(this).val() );

          $('#lkr').val(($(this).val()*2.51).toFixed(2));
          // $.ajax({
          //           type: 'GET',
          //           url: 'http://openexchangerates.org/api/latest.json?app_id=eb574cbcd9704cd8a6c626a03d20f259',
          //           // data: { q: valueSelected },
          //           // resultObj: this.resultObj,
          //           dataType:'JSON',
          //           success: function (json) {
          //              console.log(json);
                                              
          //           }
          //       });
      });

  });
	
	
</script>
@stop