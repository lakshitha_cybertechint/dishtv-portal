<?php
namespace PackageManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Gallery Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Insaf Zakariya
 * @version    v1.0.0
 */
class PackageChannels extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_package_channels';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['package_id','channel_id'];


	public function channels()
    {
        return $this->belongsTo('ChannelManage\Models\Channel', 'id', 'channel_id');
    }

}
