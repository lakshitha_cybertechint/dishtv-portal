<?php
namespace PackageManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Branch Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class Package extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sa_packages';

	protected $primaryKey = 'id';

	public function package_channels()
		{
			return $this->hasMany('PackageManage\Models\PackageChannels',  'package_id', 'id');
		}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'service_providers','lkr','inr'];


}
