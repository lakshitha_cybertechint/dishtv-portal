<?php
namespace PackageManage\Http\Controllers;

use App\Http\Controllers\Controller;
use PackageManage\Http\Requests\PackageRequest;
use Illuminate\Http\Request;
use PackageManage\Models\Package;
use PackageManage\Models\PackageChannels;
use ServiceProviderManage\Models\ServiceProvider;
use ChannelManage\Models\ChannelSeriveProviders;
use ChannelManage\Models\Channel;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;



class PackageController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Branch Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$serviceProvider = ServiceProvider::get();
		return view( 'PackageManage::package.add')->with([ 
				'serviceProvider' => $serviceProvider
				 ]);
	}


	public function sort(Request $request) {
		$ChannelSeriveProviders = ChannelSeriveProviders::where('service_providers_id','=',$request->q)->get();
		$channel = collect([]);

		foreach ($ChannelSeriveProviders as $ChannelSeriveProvider) {
	        $channel->push(Channel::where('id','=',  $ChannelSeriveProvider['channel_id'])->get());
	    }

        // $channel = Channel::where('id','=',$ChannelSeriveProviders[0]->channel_id)->get();
        return $channel;
    }

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(PackageRequest $request)
	{	
		// dd($request->request);
		$count= Package::where('name', '=',$request->name)->count();
		// echo $count;
		// echo "<br>";
		// echo $request->name;
		// dd($request->request);
		if ($count==0) {
			$package = new Package();
			$package->name = $request->name;
			$package->service_providers = $request->serviceProvider;
			$package->lkr = $request->lkr;
			$package->inr = $request->inr;

			$package->save();	

			if($package){
				if ($request->channels && count($request->channels) > 0) {
		            foreach($request->channels as $key => $channelId){
		              PackageChannels::create([
		              	'package_id' => $package->id,
		                'channel_id' => $channelId
		            ]);
		        }
			}
		}	


		return redirect('package/add')->with([ 'success' => true,
				'success.message'=> 'Service Provider Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('package/add')->with([ 'error' => true,
				'error.message'=> 'Service Provider Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'PackageManage::package.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Package::get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $package) {
				$serviceProvider= ServiceProvider::find($package->service_providers);	
				$dd = array();
				array_push($dd, $i);
				
				array_push($dd, $package->name);
				array_push($dd, $serviceProvider['name']);
				array_push($dd, $package->lkr);
				array_push($dd, $package->inr);
					
				$permissions = Permission::whereIn('name',['package.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('package/edit/'.$package->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['package.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="Package-delete" data-id="'.$package->id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}



	/**
	 * Delete a branch
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$Package = Package::find($id);
			PackageChannels::where('package_id','=',$id)->delete();
			if($Package){
				$Package->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curPackage=Package::find($id);
		$serviceProvider=ServiceProvider::get();
		$channel=Channel::get();

		if($curPackage){
			return view( 'PackageManage::package.edit' )->with([ 
				'curPackage' => $curPackage,
				'serviceProvider'=>$serviceProvider,
				'channel'=>$channel
				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(PackageRequest $request, $id)
	{		
		// dd($request->request);
		$count= Package::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		$Package =  Package::find($id);
		
			if($count==0){
				$PackageOld =  Package::where('id',$id)->take(1)->get();
				$branch=$PackageOld[0];			
				$branch->name = $request->get('name');
				$branch->service_providers = $request->get('serviceProvider');
				$branch->lkr = $request->get('lkr');
				$branch->inr = $request->get('inr');

				PackageChannels::where('package_id', $branch->id)->delete();

		        if ($request->channels && count($request->channels) > 0) {
		          foreach($request->channels as $key => $channelId){
		            PackageChannels::create([
		              'package_id' => $branch->id,
		              'channel_id' => $channelId
		            ]);
		          }
		        }
				

				$branch->save();
				
				return redirect( 'package/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Package updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('package/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Package Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}

	function save_logo_image($image, $path){// sase base64 encoded image,  path with image name

      list($type, $image) = explode(';', $image);
      list(, $image)      = explode(',', $image);
      $image = base64_decode($image);

      file_put_contents($path, $image);

    }

}
