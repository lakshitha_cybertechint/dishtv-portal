<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'package', 'namespace' => 'PackageManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'package.add', 'uses' => 'PackageController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'package.edit', 'uses' => 'PackageController@editView'
      ]);

      Route::get('list', [
        'as' => 'package.list', 'uses' => 'PackageController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'package.list', 'uses' => 'PackageController@jsonList'
      ]);

      Route::get('json/sort', [
        'as' => 'package.list', 'uses' => 'PackageController@sort'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'package.add', 'uses' => 'PackageController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'package.edit', 'uses' => 'PackageController@edit'
      ]);

      Route::post('status', [
        'as' => 'package.status', 'uses' => 'PackageController@status'
      ]);

      Route::post('delete', [
        'as' => 'package.delete', 'uses' => 'PackageController@delete'
      ]);
    });
});