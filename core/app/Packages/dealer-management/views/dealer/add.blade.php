@extends('layouts.back.master') @section('current_title','New Dealer')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<style type="text/css">
    sup {
        color: red;
    }

    .imageBox
    {
      position: relative;
      width: 150px;
      height: 150px;
      border:1px solid #aaa;
      background: #fff;
      overflow: hidden;
      background-repeat: no-repeat;
      cursor:move;
    }

    .imageBox .thumbBox
    {
      position: absolute;
      top: 50%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      width: 100px;
      height: 100px;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: auto;
      margin-right: auto;
      box-sizing: border-box;
      border: 1px solid rgb(102, 102, 102);
      box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
      background: none repeat scroll 0% 0% transparent;
    }

    .imageBox .spinner
    {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      text-align: center;
      line-height: 400px;
      background: rgba(0,0,0,0.7);
    }
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('dealer/list')}}">Dealers Management</a></li>
       
        <li class="active">
            <span>New Dealer</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">                
                <form method="POST" class="form-horizontal" id="myForm" files="true" enctype="multipart/form-data">
                	{!!Form::token()!!}

                	<div class="form-group"><label class="col-sm-2 control-label">BUSINESS NAME <sup>*</sup></label>
                    	<div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                	</div>
                  <div class="form-group"><label class="col-sm-2 control-label">BUSINESS E-MAIL <sup>*</sup></label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="email"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">CONTACT NO <sup>*</sup></label>
                      <div class="col-sm-10"><input type="number" class="form-control" name="tel"></div>
                  </div>
                  <div class="form-group"><label class="col-sm-2 control-label">ADDRESS <sup>*</sup></label>
                      <div class="col-sm-10"><textarea class="form-control" name="address"></textarea></div>
                  </div>
                	<div class="form-group">
                        <input type="hidden" id="logo_img" name="logo_img" value="" required form="myForm">
                            <label class="col-sm-2 control-label required">BUSINESS LOGO </label>
                            <div class="col-sm-10">
                                <div class="col-sm-10">
                                    <div class="col-md-6">
                                        <button type="button" class="btn bg-grey waves-effect" data-toggle="modal" data-target="#crop-modal" >SELECT LOGO</button>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="cropped">

                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">BUSINESS REG NO <sup>*</sup></label>
                      <div class="col-sm-10"><input type="text" class="form-control" name="regNo"></div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 control-label">USER</label>
                         <div class="col-sm-10">
                         <select class="selectSingle" name="user" style="width: 100%" required>
                              <option></option>
                         <?php foreach ($users as $key => $value): ?>
                             <option value="{{$value->id}}">{{$value->  first_name}}</option>
                         <?php endforeach ?>
                            
                            
                        </select>
                    </div>
                      
                  </div>
                	                	
                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Done</button>
	                    </div>
	                </div>
                	
                </form>
        </div>
    </div>
</div>

<!-- crop modal -->
        <div class="modal fade" id="crop-modal" tabindex="-1" role="dialog">
          <div class="modal-dialog " role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" >Crop Logo Image</h4>
              </div>
              <div class="modal-body">
                <div class="container">
                  <div class="imageBox">
                    <div class="thumbBox"></div>
                    {{-- <div class="spinner" style="display: none">Loading...</div> --}}
                  </div>
                  <div class="action">
                    <input type="file" id="cropFile" style="float:left; width: 250px">

                    <input type="button" id="btnZoomIn" value="+" class="btn bg-grey waves-effect">
                    <input type="button" id="btnZoomOut" value="-" class="btn bg-grey waves-effect">

                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn bg-grey waves-effect" id="btnCrop">Crop</button>
                <button type="button" class="btn bg-amber waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>
<!-- crop modal -->

@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{url('assets/back/cropbox/cropbox.js')}}"></script>
<script type="text/javascript">


  var options =
  {
      thumbBox: '.thumbBox',
      spinner: '.spinner',
      imgSrc: 'avatar.png'
  }
  var cropper = $('.imageBox').cropbox(options);
  $('#cropFile').on('change', function(){
      var reader = new FileReader();
      reader.onload = function(e) {
          options.imgSrc = e.target.result;
          cropper = $('.imageBox').cropbox(options);
      }
      reader.readAsDataURL(this.files[0]);
      this.files = [];
  })
  $('#btnCrop').on('click', function(){
    var img = cropper.getDataURL()
    $('#logo_img').val(img);
    document.querySelector('.cropped').innerHTML = '<img style="max-width:100%;" src="'+img+'">';
    $('#crop-modal').modal('hide');
  })
  $('#btnZoomIn').on('click', function(){
      cropper.zoomIn();
  })
  $('#btnZoomOut').on('click', function(){
      cropper.zoomOut();
  })

</script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".selectSingle").select2({
    placeholder: "Select User",
    maximumResultsForSearch:1
});


		$("#myForm").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                  required:true
                },
                tel: {
                  required:true,
                  maxlength: 10,
                  minlength: 9
                },
                address: {
                  required:true
                },
                regNo: {
                  required:true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
	});
	
	
</script>
@stop