<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'dealer', 'namespace' => 'DealerManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'dealer.add', 'uses' => 'DealerController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'dealer.edit', 'uses' => 'DealerController@editView'
      ]);

      Route::get('list', [
        'as' => 'dealer.list', 'uses' => 'DealerController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'dealer.list', 'uses' => 'DealerController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'dealer.add', 'uses' => 'DealerController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'dealer.edit', 'uses' => 'DealerController@edit'
      ]);

      Route::post('status', [
        'as' => 'dealer.status', 'uses' => 'DealerController@status'
      ]);

      Route::post('delete', [
        'as' => 'dealer.delete', 'uses' => 'DealerController@delete'
      ]);
    });
});