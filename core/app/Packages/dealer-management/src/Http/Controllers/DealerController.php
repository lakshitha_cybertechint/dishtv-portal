<?php
namespace DealerManage\Http\Controllers;

use App\Http\Controllers\Controller;
use DealerManage\Http\Requests\DealerRequest;
use Illuminate\Http\Request;
use DealerManage\Models\Dealer;
use UserManage\Models\User;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;



class DealerController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Branch Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$users = User::get();
		return view( 'DealerManage::dealer.add')->with([ 
				'users' => $users
				 ]);
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(DealerRequest $request)
	{
		$count= Dealer::where('name', '=',$request->name)->count();

		if ($count==0) {
			$dealer = new Dealer();
			$dealer->name = $request->name;
			$dealer->email = $request->email;
			$dealer->tel = $request->tel;
			$dealer->address = $request->address;

			if($request->logo_img){

			$logopath = 'uploads/images/logo/dealer';
        	$logoName = 'dealer-business-logo-' . date('YmdHis') . '.png';
        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

        	$dealer->logo_name = $logoName;
        	}
        	$dealer->regNo = $request->regNo;
        	$dealer->user = $request->user;
			$dealer->save();		


		return redirect('dealer/add')->with([ 'success' => true,
				'success.message'=> 'Dealer Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('dealer/add')->with([ 'error' => true,
				'error.message'=> 'Dealer Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'DealerManage::dealer.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Dealer::get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $dealer) {
				
				$dd = array();
				array_push($dd, $i);
				
				array_push($dd, $dealer->name);
				if($dealer->email != ""){
					array_push($dd, $dealer->email);
				}else{
					array_push($dd, "-");
				}
				if($dealer->tel != ""){
					array_push($dd, $dealer->tel);
				}else{
					array_push($dd, "-");
				}	
				$permissions = Permission::whereIn('name',['dealer.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('dealer/edit/'.$dealer->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['dealer.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="dealer-delete" data-id="'.$dealer->id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	/**
	 * Delete a branch
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$dealer = Dealer::find($id);
			if($dealer){
				$dealer->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curDealer=Dealer::find($id);
		$users=User::get();
		if($curDealer){
			return view( 'DealerManage::dealer.edit' )->with([ 
				'curDealer' => $curDealer, 'users' => $users
				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(DealerRequest $request, $id)
	{		
		// dd($request->request);
		$count= Dealer::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		$Dealer =  Dealer::find($id);
		
			if($count==0){
				$DealerOld =  Dealer::where('id',$id)->take(1)->get();
				$branch=$DealerOld[0];			
				$branch->name = $request->get('name');
				$branch->email = $request->get('email');
				$branch->tel = $request->get('tel');
				$branch->address = $request->get('address');
								if ($request->logo_img != '') {
					File::delete(storage_path('core/storage/uploads/images/logo/dealer/' . $Dealer->logo_name));
			        $logopath = 'uploads/images/logo/dealer';
		        	$logoName = 'dealer-business-logo-' . date('YmdHis') . '.png';
		        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

		        	$branch->logo_name = $logoName;
			        }
			    $branch->regNo = $request->get('regNo');
			    $branch->user = $request->get('user');


				$branch->save();
				
				return redirect( 'dealer/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Dealer updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('dealer/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Dealer Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}

	function save_logo_image($image, $path){// sase base64 encoded image,  path with image name

      list($type, $image) = explode(';', $image);
      list(, $image)      = explode(',', $image);
      $image = base64_decode($image);

      file_put_contents($path, $image);

    }

}
