<?php
namespace ServiceProviderManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class ServiceProviderRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){    

	
		$rules = [
			'name' => 'required',			
		];
	
		
		return $rules;
	}

}
