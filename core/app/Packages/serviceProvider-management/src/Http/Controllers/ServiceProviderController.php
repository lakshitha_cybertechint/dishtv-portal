<?php
namespace ServiceProviderManage\Http\Controllers;

use App\Http\Controllers\Controller;
use ServiceProviderManage\Http\Requests\ServiceProviderRequest;
use Illuminate\Http\Request;
use ServiceProviderManage\Models\ServiceProvider;
use Permissions\Models\Permission;
use Sentinel;
use Response;
use File;



class ServiceProviderController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Branch Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		return view( 'ServiceProviderManage::serviceProvider.add');
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function add(ServiceProviderRequest $request)
	{	
		$count= ServiceProvider::where('name', '=',$request->name)->count();
		// echo $count;
		// echo "<br>";
		// echo $request->name;
		// dd($request->request);
		if ($count==0) {
			$ServiceProvider = new ServiceProvider();
			$ServiceProvider->name = $request->name;

			if($request->logo_img){

			$logopath = 'uploads/images/logo';
        	$logoName = 'service-provider-logo-' . date('YmdHis') . '.png';
        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

        	$ServiceProvider->logo = $logoName;
        	}
			$ServiceProvider->save();		


		return redirect('serviceProvider/add')->with([ 'success' => true,
				'success.message'=> 'Service Provider Created successfully!',
				'success.title' => 'Well Done!']);
		}else{
			
				return redirect('serviceProvider/add')->with([ 'error' => true,
				'error.message'=> 'Service Provider Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function listView()
	{		
		return view( 'ServiceProviderManage::serviceProvider.list' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= ServiceProvider::get();			
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $ServiceProvider) {
				
				$dd = array();
				array_push($dd, $i);
				
				if($ServiceProvider->name != ""){
					array_push($dd, $ServiceProvider->name);
				}else{
					array_push($dd, "-");
				}	
				$permissions = Permission::whereIn('name',['branch.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('serviceprovider/edit/'.$ServiceProvider->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['serviceProvider.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="serviceProvider-delete" data-id="'.$ServiceProvider->id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}


	// /**
	//  * Activate or Deactivate user
	//  * @param  Request $request branch id with status to change
	//  * @return json object with status of success or failure
	//  */
	// public function status(Request $request)
	// {
	// 	if($request->ajax()){
	// 		$id = $request->input('id');
	// 		$status = $request->input('status');

	// 		$branch = Branch::find($id);
	// 		if($branch){
	// 			$branch->status = $status;
	// 			$branch->save();
	// 			return response()->json(['status' => 'success']);
	// 		}else{
	// 			return response()->json(['status' => 'invalid_id']);
	// 		}
	// 	}else{
	// 		return response()->json(['status' => 'not_ajax']);
	// 	}
	// }

	/**
	 * Delete a branch
	 * @param  Request $request branch id
	 * @return Json           	json object with status of success or failure
	 */
	public function delete(Request $request)
	{
		if($request->ajax()){
			$id = $request->input('id');

			$serviceProvider = ServiceProvider::find($id);
			if($serviceProvider){
				$serviceProvider->delete();
				return response()->json(['status' => 'success']);
			}else{
				return response()->json(['status' => 'invalid_id']);
			}
		}else{
			return response()->json(['status' => 'not_ajax']);
		}
	}

	/**
	 * Show the branch edit screen to the branch.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$curServiceProvider=ServiceProvider::find($id);
		if($curServiceProvider){
			return view( 'ServiceProviderManage::serviceProvider.edit' )->with([ 
				'curServiceProvider' => $curServiceProvider
				 ]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new user data to database
	 *
	 * @return Redirect to Branch add
	 */
	public function edit(ServiceProviderRequest $request, $id)
	{		
		$count= ServiceProvider::where('id', '!=', $id)->where('name', '=',$request->get('name' ))->count();
		$ServiceProvider =  ServiceProvider::find($id);
		
			if($count==0){
				$serviceProviderOld =  ServiceProvider::where('id',$id)->take(1)->get();
				$branch=$serviceProviderOld[0];			
				$branch->name = $request->get('name');
				if ($request->logo_img != '') {
					File::delete(storage_path('core/storage/uploads/images/logo/' . $ServiceProvider->logo));
			        $logopath = 'uploads/images/logo';
		        	$logoName = 'service-provider-logo-' . date('YmdHis') . '.png';
		        	$this->save_logo_image($request->logo_img, 'core/storage/'.$logopath.'/'.$logoName);

		        	$branch->logo = $logoName;
			        }

				$branch->save();
				
				return redirect( 'serviceProvider/edit/'.$id )->with([ 'success' => true,
					'success.message'=> 'Service Provider updated successfully!',
					'success.title' => 'Good Job!' ]);
			}else{
				return redirect('serviceProvider/edit/'.$id)->with([ 'error' => true,
				'error.message'=> 'Service Provider Already Exsist!',
				'error.title' => 'Duplicate!']);
			}

		
   
		
	}

	function save_logo_image($image, $path){// sase base64 encoded image,  path with image name

      list($type, $image) = explode(';', $image);
      list(, $image)      = explode(',', $image);
      $image = base64_decode($image);

      file_put_contents($path, $image);

    }

}
