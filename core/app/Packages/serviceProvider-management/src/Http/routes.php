<?php
/**
 * BRANCH MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright 2015 Insaf Zakariya
 */

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'serviceprovider', 'namespace' => 'ServiceProviderManage\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'serviceprovider.add', 'uses' => 'ServiceProviderController@addView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'serviceprovider.edit', 'uses' => 'ServiceProviderController@editView'
      ]);

      Route::get('list', [
        'as' => 'serviceprovider.list', 'uses' => 'ServiceProviderController@listView'
      ]);

      Route::get('json/list', [
        'as' => 'serviceprovider.list', 'uses' => 'ServiceProviderController@jsonList'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'serviceprovider.add', 'uses' => 'ServiceProviderController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'serviceprovider.edit', 'uses' => 'ServiceProviderController@edit'
      ]);

      Route::post('status', [
        'as' => 'serviceprovider.status', 'uses' => 'ServiceProviderController@status'
      ]);

      Route::post('delete', [
        'as' => 'serviceprovider.delete', 'uses' => 'ServiceProviderController@delete'
      ]);
    });
});