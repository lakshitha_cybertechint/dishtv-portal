<?php

namespace ServiceProviderManage;

use Illuminate\Support\ServiceProvider;

class ServiceProviderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'ServiceProviderManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ServiceProviderManage', function($app){
            return new ServiceProviderManage;
        });
    }
}
