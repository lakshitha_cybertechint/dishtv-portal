<?php

namespace App\Http\Controllers;


use App\Http\Requests\ReCaptchataTestFormRequest;
use CakeTypeManager\Models\CakeType;
use Illuminate\Http\Request;

use File;
use NewsManage\Models\News;
use PrivateClassManage\Models\PrivateClassCategory;
use PrivateClassManage\Models\PrivateReg;
use GalleryManage\Models\Gallery;
use GalleryManage\Models\GalleryCategory;
use GalleryManage\Models\GalleryImage;
use RecipesManager\Models\Recipes;
use PublicationManager\Models\Publication;
use TastingManage\Models\Tasting;
use TastingManage\Models\TastingPeopleType;
use SliderManager\Models\Slider;
use App\Models\ShoppingCart;
use App\Models\BillingDetail;
use CountryManager\Models\Country;
use App\Models\LastShipping;
use App\Models\ShippingMethod;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\PaypalTransaction;
use App\Models\SortType;
use App\Models\Rating;
use CouponManager\Models\Coupon;
use CouponManager\Models\CouponItem;


use Response;
use Input;
use DB;
use Session;
use Mail;
use Cart;
use PayPal;
use Usps;
use Carbon\Carbon;



use Sentinel;
use PDF;
use View;
use Permissions\Models\Permission;

use Hash;

class WebController extends Controller {

	/*
		|--------------------------------------------------------------------------
		| Web Controller
		|--------------------------------------------------------------------------
		|
		| This controller renders the "marketing page" for the application and
		| is configured to only allow guests. Like most of the other sample
		| controllers, you are free to modify or remove it as you desire.
		|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('guest');
	}



    public function index() {    
       
        return view('front.index');
    }

	
}
