<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */

/*DONT USE THIS ROUTE FOR OTHER USAGE ----ONLY FOR THIS */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]); 
     Route::get('/', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);    
	
});


Route::group([], function()
{	
    // Route::get('/', [
    //   'as' => 'index', 'uses' => 'WebController@index'
    // ]);    

});


/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);



/**
 * USER LOGIN VIA FACEBOOK/GOOGLE/TWITTER/LINKDIN ETC
 */

Route::get('auth/facebook', 'AuthController@redirectToFacebook');
Route::get('auth/facebook/callback', 'AuthController@handleFacebookCallback');

Route::get('auth/google', 'AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'AuthController@handleGoogleCallback');