<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Sweet Delights Cakery</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="pinterest" content="nohover"/>

    <link rel="shortcut icon" href="{{asset('assets/front/images/favicon.ico')}}">

    <!-- CSS LINK -->  
        <!-- CODE HERE -->  
    <!-- ======== -->

    @yield('css')

    <!-- HEADER SECTION -->
            <!-- CODE HERE -->
           <label>HEADER</label></br>
    <!-- ============== -->

    @yield('content')

    <!-- FOOTER SECTION -->
            <!-- CODE HERE -->
            <label>FOOTER</label>
    <!-- ============== -->

    <!-- JS LINK -->
        <!-- CODE HERE -->
    <!-- ======= -->

    @yield('js')

